import java.util.Scanner;
// import java.util.LinkedList;
// import java.util.Queue;
import java.io.File;

public class RedBlackBST {

    private static final boolean RED   = true;
    private static final boolean BLACK = false;

    private Node root; // root of the BST
    private int redNodeCount = 0;

    // BST helper node data type
    private class Node {
        private int key;
        private Node parent, left, right;
        private boolean color;

        private int size;     // subtree count. including self

        public Node(int key) {
            this.key = key;
            this.color = RED;
            this.size = 1;

            redNodeCount++;
        }

        public void setColor(boolean color) {
            if (this.color == RED && color == BLACK) {
                redNodeCount--;
            } else if (this.color == BLACK && color == RED) {
                redNodeCount++;
            }
            this.color = color;

            // this.countRed = countRed(this.left) + countRed(this.right) +
            // (color == RED ? 1 : 0);
        }
    }

    public RedBlackBST() {

    }
   /***************************************************************************
    *  Node helper methods.
    ***************************************************************************/
    // is node x red; false if x is null ?
    private boolean isRed(Node x) {
        if (x == null) return false;
        return x.color == RED;
    }

    // number of node in subtree rooted at x; 0 if x is null
    private int size(Node x) {
        if (x == null) return 0;
        return x.size;
    }

    /**
     * Returns the number of key-value pairs in this symbol table.
     * @return the number of key-value pairs in this symbol table
     */
    public int size() {
        return size(root);
    }

   /**
     * Is this symbol table empty?
     * @return {@code true} if this symbol table is empty and {@code false} otherwise
     */
    public boolean isEmpty() {
        return root == null;
    }

    public int percentRed() {
        return (int)(Math.round((double)redNodeCount / size() * 100));
    }

   /***************************************************************************
    *  Red-black tree insertion.
    ***************************************************************************/

    /**
     * Inserts the specified key-value pair into the symbol table, overwriting the old
     * value with the new value if the symbol table already contains the specified key.
     * Deletes the specified key (and its associated value) from this symbol table
     * if the specified value is {@code null}.
     *
     * @param key the key
     * @param val the value
     * @throws NullPointerException if {@code key} is {@code null}
     */
    public void put(int key) {
        root = put(root, key);
        root.setColor(BLACK);
    }

    // insert the key-value pair in the subtree rooted at h
    private Node put(Node h, int key) {
        if (h == null) {
            // let the parent reference be null
            return new Node(key);
        }

        int cmp = key - h.key;
        if (cmp < 0) {
            h.left = put(h.left, key);
            h.left.parent = h;
        } else if (cmp > 0) {
            h.right = put(h.right, key);
            h.right.parent = h;
        } else {
            h.key = key;
        }

        // fix-up any right-leaning links
        if (isRed(h.right) && !isRed(h.left))      h = rotateLeft(h);
        if (isRed(h.left)  &&  isRed(h.left.left)) h = rotateRight(h);
        if (isRed(h.left)  &&  isRed(h.right))     flipColors(h);
        h.size = size(h.left) + size(h.right) + 1;

        return h;
    }

   /***************************************************************************
    *  Red-black tree helper functions.
    ***************************************************************************/

    // make a left-leaning link lean to the right
    private Node rotateRight(Node h) {
        // assert (h != null) && isRed(h.left);
        Node x = h.left;
        if (x.right != null) {
            x.right.parent = h;
        }
        h.left = x.right;
        // h.right doesn't change:
        if (h.right != null && h.right.parent != h) {
            System.out.println("Error, h.right.parent is not h");
        }
        x.parent = h.parent;
        h.parent = x;
        x.right = h;

        x.setColor(x.right.color);
        x.right.setColor(RED);
        x.size = h.size;
        h.size = size(h.left) + size(h.right) + 1;
        return x;
    }

    // make a right-leaning link lean to the left
    private Node rotateLeft(Node h) {
        // assert (h != null) && isRed(h.right);
        Node x = h.right;
        if (x.left != null) {
            x.left.parent = h;
        }
        h.right = x.left;
        // h.left doesn't change:
        if (h.left != null && h.left.parent != h) {
            System.out.println("Error, h.left.parent is not h");
        }
        x.parent = h.parent;
        h.parent = x;
        x.left = h;

        x.setColor(x.left.color);
        x.left.setColor(RED);
        x.size = h.size;
        h.size = size(h.left) + size(h.right) + 1;
        return x;
    }

    // flip the colors of a node and its two children
    private void flipColors(Node h) {
        // h must have opposite color of its two children
        // assert (h != null) && (h.left != null) && (h.right != null);
        // assert (!isRed(h) &&  isRed(h.left) &&  isRed(h.right))
        //    || (isRed(h)  && !isRed(h.left) && !isRed(h.right));
        h.setColor(!h.color);
        h.left.setColor(!h.left.color);
        h.right.setColor(!h.right.color);
    }


    /**
     * Unit tests the {@code RedBlackBST} data type.
     *
     * @param args the command-line arguments
     */
    public static void main(String[] args) {
        RedBlackBST st = new RedBlackBST();

        if (args.length < 1) {
            int[] sizes = { 10, 100, 1000, 10000 };
            int randomSize = sizes[(int)(Math.random() * sizes.length)];
            System.out.println("No filename given. Generating " +
                randomSize + " random numbers");
            for (int i = 0; i < randomSize; i++) {
                st.put(i);
            }
        } else {
            String filename = args[0];
            Scanner reader;
            try {
                File readData = new File(filename);
                reader = new Scanner(readData);
                System.out.println("Reading input values from " + filename);
            } catch (Exception err) {
                System.out.println("Couldn't open file " + filename);
                System.out.println(err);
                return;
            }
            while (reader.hasNext()) {
                if (reader.hasNextInt()) {
                    st.put(reader.nextInt());
                } else {
                    reader.next();
                }
            }
            reader.close();
        }
        System.out.println("Percent of Red Nodes: " + st.percentRed());

        // Walk the tree to collect colour counts - was used to check answer
/*
        Queue<Node> queue = new LinkedList<RedBlackBST.Node>();
        queue.add(st.root);
        int redCount = 0;
        int blackCount = 0;
        while (!queue.isEmpty()) {
            Node x = queue.remove();
            if (x.left != null) {
                if (x.left.parent != x) {
                    System.out.println("Error, x.left.parent is not x");
                }
                queue.add(x.left);
            }
            if (x.right != null) {
                if (x.right.parent != x) {
                    System.out.println("Error, x.right.parent is not x");
                }
                queue.add(x.right);
            }
            if (x.color) {
                redCount++;
            } else {
                blackCount++;
            }
        }
        System.out.println("Manual count of red nodes " + redCount);
        System.out.println("Manual count of black nodes " + blackCount);
*/
    }
}

/******************************************************************************
 *  Copyright 2002-2016, Robert Sedgewick and Kevin Wayne.
 *
 *  This file is part of algs4.jar, which accompanies the textbook
 *
 *      Algorithms, 4th edition by Robert Sedgewick and Kevin Wayne,
 *      Addison-Wesley Professional, 2011, ISBN 0-321-57351-X.
 *      http://algs4.cs.princeton.edu
 *
 *
 *  algs4.jar is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  algs4.jar is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with algs4.jar.  If not, see http://www.gnu.org/licenses.
 ******************************************************************************/
