/* BaseballElimination.java
   CSC 226 - Spring 2019
   Assignment 4 - Baseball Elimination Program
   
   This template includes some testing code to help verify the implementation.
   To interactively provide test inputs, run the program with
   > java BaseballElimination
  
   To conveniently test the algorithm with a large input, create a text file
   containing one or more test divisions (in the format described below) and run
   the program with
   > java -cp .;algs4.jar BaseballElimination file.txt (Windows)
   or
   > java -cp .:algs4.jar BaseballElimination file.txt (Linux or Mac)
   where file.txt is replaced by the name of the text file.
   
   The input consists of an integer representing the number of teams in the division and then
   for each team, the team name (no whitespace), number of wins, number of losses, and a list
   of integers represnting the number of games remaining against each team (in order from the first
   team to the last). That is, the text file looks like:

   <number of teams in division>
   <team1_name wins losses games_vs_team1 games_vs_team2 ... games_vs_teamn>
   ...
   <teamn_name wins losses games_vs_team1 games_vs_team2 ... games_vs_teamn>

   An input file can contain an unlimited number of divisions but all team names are unique, i.e.
   no team can be in more than one division.

   R. Little - 03/22/2019
*/

import edu.princeton.cs.algs4.*;
import java.util.*;
import java.io.File;

//Do not change the name of the BaseballElimination class
public class BaseballElimination{
  
  // We use an ArrayList to keep track of the eliminated teams.
  public ArrayList<String> eliminated = new ArrayList<String>();
  public String[] teams;
  // Team data uses a 2D array that maps teams[] index to other fields
  public int[] gamesLeft;
  public int[] wins;
  public int[][] against;

  /* BaseballElimination(s)
    Given an input stream connected to a collection of baseball division
    standings we determine for each division which teams have been eliminated 
    from the playoffs. For each team in each division we create a flow network
    and determine the maxflow in that network. If the maxflow exceeds the number
    of inter-divisional games between all other teams in the division, the current
    team is eliminated.
  */
  public BaseballElimination(Scanner s) {
    initDataWithInputFile(s);
    StdOut.print("[");
    for (int i = 0; i < teams.length; i++) {
      StdOut.print(teams[i]);
      if (i != teams.length - 1) StdOut.print(", ");
    }
    StdOut.println("]");
    for (int teamIndex = 0; teamIndex < teams.length; teamIndex++) {
      if (
        // Be efficient by skipping maxflow where possible
        isTriviallyEliminated(teamIndex) ||
        isMaxFlowEliminated(teamIndex)
      ) {
        eliminated.add(teams[teamIndex]);
      }
    }
  }

  public void initDataWithInputFile(Scanner s) {
    int teamCount = s.nextInt();
    teams = new String[teamCount];

    // Initialize the team data
    gamesLeft = new int[teamCount];
    against = new int[teamCount][teamCount];
    wins = new int[teamCount];

    for (int i = 0; i < teamCount; i++) {
      teams[i] = s.next();
      wins[i] = s.nextInt();
      // Skip losses since they're not useful
      s.nextInt();
      gamesLeft[i] = 0;
      for (int j = 0; j < teamCount; j++) {
        int value = s.nextInt();
        gamesLeft[i] += value;
        against[i][j] = value;
      }
    }
  }

  // Not used, but left as an idea...
  public int indexFromTeamString(String team) {
    int teamIndex = -1;
    for (int i = 0; i < teams.length; i++) {
      if (teams[i] == team) {
        teamIndex = i;
        break;
      }
    }
    // if (teamIndex == -1) {
    //   throw new Exception("No such team " + team);
    // }
    return teamIndex;
  }

  public boolean isTriviallyEliminated(int teamIndex) {
    for (int i = 0; i < teams.length; i++) {
      String otherTeam = teams[i];
      if (otherTeam.equals(teams[teamIndex])) {
        continue;
      }
      if (wins[teamIndex] + gamesLeft[teamIndex] < wins[i]) {
        // System.err.println("Trivially eliminated " + teams[teamIndex]);
        return true;
      }
    }
    return false;
  }

  public boolean isMaxFlowEliminated(int teamIndex) {
    // The size of the network is the source, sink, matches, and teams. Could
    // write an O(nlogn) double for loop to add, but it's actually just the
    // arithemic sum.

    // -1 Since the graph doesn't have itself
    int teamCount = teams.length;
    int n = teamCount - 1;
    int matches = n * (n - 1) / 2;
    int vertexCount = matches + teamCount + 2 - 1;
    FlowNetwork net = new FlowNetwork(vertexCount);

    int source = 0;
    int sink = vertexCount - 1;

    int sum = 0;
    int count = 1;
    // Walk the against[][] array and pair up teams in the network
    for (int i = 0, ii = i; i < teamCount; i++, ii++) {
      if (i == teamIndex) {
        ii--;
        continue;
      }
      // Only do the triangle because it's mirrored
      for (int j = i, jj= j; j < teamCount; j++, jj++) {
        // Skip '-' in the table
        if (i == j) continue;
        // Skip own team against other team
        if (j == teamIndex) {
          jj--;
          continue;
        }
        sum += against[i][j];
        // From source to matches
        net.addEdge(new FlowEdge(source, count, against[i][j]));
        // From matches to team
        net.addEdge(new FlowEdge(count, matches + ii + 1, Double.POSITIVE_INFINITY));
        net.addEdge(new FlowEdge(count, matches + jj + 1, Double.POSITIVE_INFINITY));
        count++;
      }
      int weight = wins[teamIndex] + gamesLeft[teamIndex] - wins[i];
      net.addEdge(new FlowEdge(matches + ii + 1, sink, (weight > 0 ? weight : 0)));
    }

    FordFulkerson resultMaxFlow = new FordFulkerson(net, source, sink);
    StdOut.printf("%s: %g. Sum is %d\n", teams[teamIndex], resultMaxFlow.value(), sum);
    StdOut.println("Flow Network for " + teams[teamIndex]);
    StdOut.println(net);

    // Another way of checking elimination is summing all the against[i][j] in
    // the above loop and seeing if it equals resultMaxFlow.value()

    if (sum == (int) resultMaxFlow.value()) return false;
    return true;

    // This way checks capacities from source to matches
    // Adjacent to the source vertex 0

    // for (FlowEdge e : net.adj(0)) {
    //   if (e.flow() != e.capacity()) {
    //     return true;
    //   }
    // }
    // return false;
  }

  /* main()
     Contains code to test the BaseballElimantion function. You may modify the
     testing code if needed, but nothing in this function will be considered
     during marking, and the testing process used for marking will not
     execute any of the code below.
  */
  public static void main(String[] args){
    Scanner s;
    if (args.length > 0){
      try{
        s = new Scanner(new File(args[0]));
      }catch(java.io.FileNotFoundException e){
        System.out.printf("Unable to open %s\n",args[0]);
        return;
      }
      System.out.printf("Reading input values from %s.\n",args[0]);
    }else{
      s = new Scanner(System.in);
      System.out.printf("Reading input values from stdin.\n");
    }
    BaseballElimination be = new BaseballElimination(s);
    if (be.eliminated.size() == 0)
      System.out.println("No teams have been eliminated.");
    else
      System.out.println("Teams eliminated: " + be.eliminated);
  }
}
