# Prim's vs Kruskal's

This was a complicated assignment from lots of miscommunication with the posted
solutions. It seemed we needed to match the output of test files exactly, which
ended up being very hard to do since the algorithm is non-deterministic.

I technically completed the assignment within 30 minutes, but it took another 5
hours of understanding how to manipulate one algorithm to produce the right MST.

By 678d94 I had Kruskal's looking at the Prim tree to always make the right
decision on which of the equally weighted edges to take - turns out, this means
all test cases pass as "True"; which is _too_ good.

It was finally posted that our answers don't need to be the same, clearly.
Instead it's important to use the _textbook implementation_ and not modify
Kruskal's.

Learned a lot about the algorithms though...

Run tests:

```sh
node compare-edges.js
```

There will be a variety of true or falses during the 10 set, but that starts to
become mostly falses by set 50, and entirely false by set 100. This is expected
because of probability and set size. It's just hard to dodge that many bullets
without being hit.

![](./comparison-prim.gif)

![](./comparison-kruskal.gif)

## Visualizations

- https://www.cs.usfca.edu/~galles/visualization/Prim.html
- https://www.cs.usfca.edu/~galles/visualization/Kruskal.html
