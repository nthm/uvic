const fs = require('fs')
const { spawnSync } = require('child_process')

const tests = []

if (process.argv.length > 2) {
  tests.push(...process.argv.slice(2))
} else {
  for (const set of [10, 50, 100]) {
    for (let i = 0; i < 10; i++) {
      tests.push(`ewg_${set}vertices_${i}.txt`)
    }
  }
}

function extractTrees(content) {
  const lines = content.trim().split(/\r?\n/)

  const seperatorIndex = lines.indexOf('')
  // First two lines are text, skip them
  const prim = lines.slice(2, seperatorIndex - 1)
  const krus = lines.slice(seperatorIndex + 2, lines.length - 2)

  const reorder = mstArray =>
    mstArray
      .map(line => {
        const [edges, weight] = line.split(' ')
        const [v, w] = edges.split('-')
        return (
          v < w
            ? `${v}-${w} ${weight}`
            : `${w}-${v} ${weight}`
        )
      })
      .sort()
      .join('\n')
  
  return { prim: reorder(prim), krus: reorder(krus) }
}

for (const filename of tests) {
  const args = `-cp bin:algs4.jar -ea PrimVsKruskal ./tests/${filename}`
  const { stdout } = spawnSync('java', args.split(' '), {
    stdio: ['inherit', 'inherit', 'ignore'],
    encoding: 'utf8',
  })
  // const { prim, krus } = extractTrees(stdout)

  // const solutionFile = filename.replace('.txt', '_output.txt')
  // const content = fs.readFileSync(`./tests/${solutionFile}`, 'utf8')
  // const { prim: primSolution, krus: krusSolution } = extractTrees(content)

  // console.log(`Test of ${filename}`)

  // if (prim !== primSolution) {
  //   console.log('Prim not equal')
  //   console.log(prim, '\n')
  //   console.log('Solution:')
  //   console.log(primSolution, '\n')

  //   const pa = prim.split('\n')
  //   const psa = primSolution.split('\n')
  //   console.log('Prim:', ps.filter(x => !psa.includes(x)))
  //   console.log('Solution:', psa.filter(x => !ps.includes(x)))
  // }

  // if (krus !== krusSolution) {
  //   console.log('Krus not equal')
  //   console.log(krus, '\n')
  //   console.log('Solution:')
  //   console.log(krusSolution, '\n')

  //   const ks = krus.split('\n')
  //   const ksa = krusSolution.split('\n')
  //   console.log('Krus:', ks.filter(x => !ksa.includes(x)))
  //   console.log('Solution:', ksa.filter(x => !ks.includes(x)))
  // }
}
