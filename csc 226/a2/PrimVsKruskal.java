/*
  PrimVsKruskal.java
  CSC 226 - Spring 2019 Assignment 2 - Prim MST versus Kruskal MST Template

  The file includes the "import edu.princeton.cs.algs4.*;" so that yo can use
  any of the code in the algs4.jar file. You should be able to compile your
  program with the command

  javac -cp .;algs4.jar PrimVsKruskal.java

  To conveniently test the algorithm with a large input, create a text file
  containing a test graphs (in the format described below) and run the program
  with

  java -cp .;algs4.jar PrimVsKruskal file.txt

  Where file.txt is replaced by the name of the text file.

  The input consists of a graph (as an adjacency matrix) in the following
  format:

  <number of vertices> <adjacency matrix row 1>
  ...
  <adjacency matrix row n>

  Entry G[i][j] >= 0.0 of the adjacency matrix gives the weight (as type double)
  of the edge from vertex i to vertex j (if G[i][j] is 0.0, then the edge does
  not exist). Note that since the graph is undirected, it is assumed that
  G[i][j] is always equal to G[j][i].

  R. Little - 03/07/2019
*/

import edu.princeton.cs.algs4.Edge;
import edu.princeton.cs.algs4.EdgeWeightedGraph;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.IndexMinPQ;
import edu.princeton.cs.algs4.UF;
import edu.princeton.cs.algs4.MinPQ;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.io.File;

// Do not change the name of the PrimVsKruskal class
public class PrimVsKruskal {

  // This method isn't used. It's for debugging and made it easy to compare
  // against the post test cases since it sorted the output
  static void printMST(Queue<Edge> mst) {
    List<String> mstArray = new ArrayList<String>(mst.size());
    double weight = 0;
    for (Edge e : mst) {
      int v = e.either();
      int w = e.other(v);
      if (v <= w) {
        mstArray.add(e.toString());
      } else {
        mstArray.add(String.format("%d-%d %.5f", w, v, e.weight()));
      }
      weight += e.weight();
    }
    Collections.sort(mstArray);
    for (String s : mstArray) {
      StdOut.println(s);
    }
    StdOut.println(weight);
  }

  // This method isn't used in practice. It was once used for assertions when I
  // needed to compare the algs4 Prims/Kruskals to my adjacency matrix versions

  // This code still has assertions, but they won't be run unless the `-ea`
  // argument is passed to Java
  static boolean sameMST(Queue<Edge> mstA, Queue<Edge> mstB) {
    if (mstA.size() != mstB.size()) {
      return false;
    }
    for (Edge eA : mstA) {
      boolean match = false;
      for (Edge eB : mstB) {
        int v = eB.either();
        int w = eB.other(v);
        if (
          eA.toString().equals(eB.toString()) ||
          eA.toString().equals(String.format("%d-%d %.5f", w, v, eB.weight()))
        ) {
          match = true;
          break;
        }
      }
      if (!match) {
        return false;
      }
    }
    return true;
  }

  // I don't run the optimality checks on either algorithm since that would be
  // more code. I'll assume the given test cases are valid

  static Queue<Edge> PrimMSTAdjMatrix(double[][] G) {
    // Trust that there are G.length vertices (no row or column is all zeros)
    int n = G.length;
    Edge[] edgeTo = new Edge[n];
    double[] distTo = new double[n];
    boolean[] marked = new boolean[n];
    IndexMinPQ<Double> pq = new IndexMinPQ<Double>(n);

    for (int v = 0; v < n; v++) {
      distTo[v] = Double.POSITIVE_INFINITY;
    }

    // Need to run Prim's multiple times to catch forests
    for (int s = 0; s < n; s++) {
      if (marked[s]) {
        // Another iteration of Prim's already got this
        continue;
      }
      // Run Prim's:
      distTo[s] = 0.0;
      pq.insert(s, distTo[s]);
      while (!pq.isEmpty()) {
        int v = pq.delMin();
        marked[v] = true;
        // This is where the symmetry of the adjacency matrix comes in
        for (int w = 0; w < n; w++) {
          if (G[v][w] == 0.0) continue;
          if (marked[w]) continue; // v-w is obsolete edge
          if (G[v][w] < distTo[w]) {
            distTo[w] = G[v][w];
            edgeTo[w] = new Edge(v, w, G[v][w]);
            if (pq.contains(w)) pq.decreaseKey(w, distTo[w]);
            else                pq.insert(w, distTo[w]);
          }
        }
      }
    }

    Queue<Edge> mst = new Queue<Edge>();
    for (int v = 0; v < edgeTo.length; v++) {
      Edge e = edgeTo[v];
      if (e != null) {
        mst.enqueue(e);
      }
    }
    return mst;
  }

  /*
    PrimVsKruskal(G)

    Given an adjacency matrix for connected graph G, with no self-loops or
    parallel edges, determine if the minimum spanning tree of G found by Prim's
    algorithm is equal to the minimum spanning tree of G found by Kruskal's
    algorithm.

    If G[i][j] == 0.0, there is no edge between vertex i and vertex j If G[i][j]
    > 0.0, there is an edge between vertices i and j, and the value of G[i][j]
    gives the weight of the edge. No entries of G will be negative.
  */
  static boolean PrimVsKruskal(double[][] G){
    Queue<Edge> primsMST = PrimMSTAdjMatrix(G);

    // Store the edges outside of the Kruskals loop to avoid searching through
    // the Prim MST on each iteration (saves a few for-loops)
    Set<String> primEdges = new HashSet<String>();
    for (Edge e : primsMST) {
      primEdges.add(e.toString());
      int v = e.either();
      int w = e.other(v);
      String otherPrimsEdge = String.format("%d-%d %.5f", w, v, e.weight());
      primEdges.add(otherPrimsEdge);
    }

    // Uncomment the following lines to have the same output at Rich's solutions

    // System.out.println("Prim Tree:");
    // printMST(primsMST);

    // Important for Node testing
    // StdOut.println();

    // Kruskals algorithm has too many correct answers once edge weights are not
    // distinct.

    // Trust that there are G.length vertices (no row or column is all zeros)
    int n = G.length;
    Queue<Edge> kruskalsMST = new Queue<Edge>();
    MinPQ<Edge> pq = new MinPQ<Edge>();
    for (int i = 0; i < n; i++) {
      // Run through the triangle since it's symmetric and Edge<> is used
      for (int j = 0; j < (i + 1); j++) {
        if (G[i][j] != 0.0) {
          Edge e = new Edge(i, j, G[i][j]);
          pq.insert(e);
        }
      }
    }

    UF uf = new UF(n);
    while (!pq.isEmpty() && kruskalsMST.size() < n - 1) {
      Edge e = pq.delMin();
      int v = e.either();
      int w = e.other(v);
      if (!uf.connected(v, w)) { // v-w does not create a cycle
        uf.union(v, w);
        kruskalsMST.enqueue(e);
        if (!primEdges.contains(e.toString())) {
          // Kruskal's diverged from the Prim MST
          return false;
        }
      }
    }
    
    // System.out.println("Kruskal Tree:");
    // printMST(kruskalsMST);

    assert sameMST(primsMST, kruskalsMST);
    return true;
  }

  /*
    main()

    Contains code to test the PrimVsKruskal function. You may modify the testing
    code if needed, but nothing in this function will be considered during
    marking, and the testing process used for marking will not execute any of
    the code below.
  */
  public static void main(String[] args) {
    Scanner s;
    if (args.length > 0) {
      try {
        s = new Scanner(new File(args[0]));
      } catch(java.io.FileNotFoundException e) {
        System.out.printf("Unable to open %s\n",args[0]);
        return;
      }
      System.out.printf("Reading input values from %s.\n",args[0]);
    } else {
      s = new Scanner(System.in);
      System.out.printf("Reading input values from stdin.\n");
    }

    int n = s.nextInt();
    double[][] G = new double[n][n];
    EdgeWeightedGraph ewg = new EdgeWeightedGraph(n);
    int valuesRead = 0;
    for (int i = 0; i < n && s.hasNextDouble(); i++) {
      for (int j = 0; j < n && s.hasNextDouble(); j++) {
        G[i][j] = s.nextDouble();
        if (G[i][j] != 0.0) {
          ewg.addEdge(new Edge(i, j, G[i][j]));
        }
        if (i == j && G[i][j] != 0.0) {
          System.out.printf("Adjacency matrix contains self-loops.\n");
          return;
        }
        if (G[i][j] < 0.0) {
          System.out.printf("Adjacency matrix contains negative values.\n");
          return;
        }
        if (j < i && G[i][j] != G[j][i]) {
          System.out.printf("Adjacency matrix is not symmetric.\n");
          return;
        }
        valuesRead++;
      }
    }
    s.close();

    if (valuesRead < n*n) {
      System.out.printf("Adjacency matrix for the graph contains too few values.\n");
      return;
    }

    // PrimMST primsReal = new PrimMST(ewg);
    // Queue<Edge> primsRealMST = new Queue<Edge>();
    // for (Edge e : primsReal.edges()) {
    //   primsRealMST.enqueue(e);
    // }

    // KruskalMST kruskalsReal = new KruskalMST(ewg);
    // Queue<Edge> kruskalsRealMST = new Queue<Edge>();
    // for (Edge e : kruskalsReal.edges()) {
    //   kruskalsRealMST.enqueue(e);
    // }

    boolean pvk = PrimVsKruskal(G);
    System.out.printf("Does Prim MST = Kruskal MST? %b\n", pvk);
  }
}
