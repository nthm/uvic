public static int daysUntil(Date upcoming) {
	Date tmp = new Date(this.month, this.day);
	int count = 0;
/*
	The final date will not change, so don't calculate it's day and
	month each time:
*/
	int endMonth = upcoming.getMonth(),
		endDay = upcoming.getDay();

	int monthsPassed = this.month;
	while (endMonth != tmp.getMonth()) {
		count += tmp.daysInMonth();
		tmp = new Date(monthsPassed, this.day);
	}

	// If the dates are in the same month and overlapped then do math.
	if (count == 0 && endDay < this.day) return (365 - (this.day - endDay));

	// We can guarantee the upcoming date will be after this.day()
	while (endDay != tmp.getDay()) {
		tmp.nextDay();
		count += 1;
	}

	return count;
}
