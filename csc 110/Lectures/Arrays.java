import java.util.Arrays;
import java.util.Scanner;

import java.io.File;

public class Test {
	public static void main(String[] args) {
//		int[] a = {1, 2, 3},
//			  b = {4, 5, 6};
//		merge(a, b);
		
		String toRead = "Numbers.txt";
		sumFromFile(toRead);
	}

	public static int[] merge(int[] a, int[] b) {
		int[] result = new int[a.length + b.length];
		int[][] both = {a, b};
		int k = 0;

		for (int[] arr : both) {
			for (int i = 0; i < arr.length; i++) {
				result[i + k] = arr[i];
			}

			k += arr.length;
		}

		System.out.println(Arrays.toString(result));
		return result;
	}
	
	public static int sumFromFile(String filename) {
		int sum = 0;
		
		try {
			File readData = new File(filename);
			Scanner reader = new Scanner(readData);
			
			while ( reader.hasNext() ) {
				if (reader.hasNextInt()) {
					sum += reader.nextInt();
				} else {
					reader.next();
				}
			}
		} catch (Exception err) {
			System.out.println("Couldn't open file:\n" + err);
		}
		
		System.out.println("Sum is " + sum);
		return sum;
	}
}