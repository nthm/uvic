/*
	ID: V00857826 Grant Hames-Morgan
	Encode
	Encrypts a message by shifting each letter forward in the alphabet by a given amount.
	Program takes a message and a key as input and outputs to stdout.
*/

import java.util.*;

public class Encode {

	public static void main(String[] args) {
		testing();
		userInteraction();
	}

	public static void testing() {
		System.out.println("Testing:");

		// Tests are stored as: { encrypt(message, key),  encoded message }
		String[][] tests = {
			{ encrypt("Hello",  2 ), "JGNNQ" },
			{ encrypt("Jgnnq", -2 ), "HELLO" },
			{ encrypt("Test" ,  11), "EPDE"  },
			{ encrypt("XyzA" ,  4 ), "BCDE"  }
		};

		// For-else in Java: http://stackoverflow.com/questions/13069402/
		notOkay : {
			for (String[] line : tests)
			/*
				Variable declaration is not allowed in this block. The encrypted
				result is line[0] and the correct answer is line[1].
			*/
				if (!line[0].equals(line[1])) {
					System.out.println("Failed: Result, " + line[0] + ", was not " + line[1]);
					break notOkay;
				}
			System.out.println("Passed all tests");
		}
	}

	public static String encrypt(String message, int key) {
		System.out.println("Encrypting: " + message + "\tKey: " + key);

		int A = 65,
			Z = 90;

		String encodedMessage = "";
		for (char letter : message.toUpperCase().toCharArray()) {
			int charInt = letter;

			// Skip characters that are not A-Z
			if (charInt < A || charInt > Z) {
				encodedMessage += letter;
				continue;
			}

			int encodedInt = charInt + key;

			if (encodedInt > Z)
				encodedInt = A + (encodedInt % Z) - 1;

			if (encodedInt < A)
				encodedInt = Z - (A % encodedInt) + 1;

			char newLetter = (char) encodedInt;
			encodedMessage += newLetter;
		}

		return encodedMessage;
	}

	public static void userInteraction() {
		Scanner read = new Scanner(System.in);

		System.out.print("Text to encrypt: ");
		String message = read.nextLine();

		System.out.print("Key (Integer): ");
		int key = read.nextInt();

		String result = encrypt(message, key);
		System.out.print("The encrypted message is " + result);
	}
}