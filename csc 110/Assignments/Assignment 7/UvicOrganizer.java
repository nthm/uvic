/*
	ID: V00857826 Grant Hames-Morgan
	UvicOrganizer
	Generates instances of objects to be sorted or searched through. Accepts a 
	list of files through args or prompts the user for them. It then asks what 
	operation they want to perform on the objects.
*/

import java.util.Scanner;
import java.io.File;
import java.util.Arrays;

public class UvicOrganizer {

/*
	The main method initializes two scanners and then calls the other methods 
	in the file. One scanner is set up to read from System.in until it is given 
	a valid filename to link the second scanner (which was initially set to 
	null) to the file.
*/	
	public static void main(String[] args) {
		UvicCourse[] courseList = null;
		Scanner console = new Scanner(System.in),
				reader	= null;
		
		while (true) {
			try {
				System.out.print("What is the name of the input file? ");
				String filename = console.next();
				reader = new Scanner(new File(filename));
				break; // Linked successfully
				
			} catch (Exception e) {
				System.out.println("Unable to access file. Try again.");
			}
		}
		
		courseList = makeArray(reader);
		printArray(courseList);
		
		// Sorting happens before searching for aesthetics and as a dependancy
		sortByNumber(courseList);
		
		System.out.print("What department are you interested in? ");
		String dept = console.next();
		listCoursesInDept(dept, courseList);
		
		System.out.print("What department are you interested in? ");
		dept = console.next();
		System.out.print("What year of courses are you interested in? ");
		int year = console.nextInt();
		listCoursesByDeptAndYear(dept, year, courseList);
	}

/*
	makeArray uses the scanner passed as a parameter (initialized in main) to 
	read a file an build UvicCourse objects. See UvicCourse.java for details on 
	the each object's avaliable methods.
	
	The file read is assumed to follow a syntax where the first integer is the 
	amount of courses in the file, and each line afterward follows:
	
	<Department, one token> <Number, one token (int)> <Title, rest of the line>
*/
	public static UvicCourse[] makeArray(Scanner s) {
		int lines  = s.nextInt();
		UvicCourse[] courseList = new UvicCourse[lines];
		
		for (int i = 0; i < lines; i++) {
			String dept  = s.next();
			int    num   = s.nextInt();
			String title = s.nextLine();
			
			UvicCourse course = new UvicCourse(dept, num, title);
			courseList[i] = course;
		}
		
		return courseList;
	}

/*
	listCoursesInDept searches through the given UvicCourses[] array printing 
	any courses that have the same department (ignoring case) as the department 
	given by targetDept.
*/
	public static void listCoursesInDept(String targetDept, UvicCourse[] arr) {
	/*
		Note on simplicity (unlike listCoursesByDeptYear):
		
		There isn't any optimization to be done here because I only have a 
		method to sort by number not sort by dept. It would be irrelevant to 
		build one too because sorting all entries would involve iterating 
		through the array which is what is done here.
	*/	
		for (UvicCourse course : arr) {
			if (course.getDept().equalsIgnoreCase(targetDept))
				System.out.println(course);
		}
		
		System.out.println();
	}

/*
	yearFromNumber is a helper method used in listCoursesByDeptAndYear which 
	simply takes a courses num field and returns its first digit.
*/
	public static int yearFromNumber(UvicCourse course) {
		return Integer.parseInt(Integer.toString(course.getNum()).substring(0, 1));
	}

/*
	listCoursesByDeptAndYear uses binary search on the sorted array of courses 
	and then linear searching produce a new array that holds all courses in the 
	given year. That array is then passed to listCoursesInDept.
*/
	public static void listCoursesByDeptAndYear(String targetDept, int year, UvicCourse[] arr) {
	/*
		I tried really hard to avoid the obvious solution of a linear search 
		that just prints when year and dept match an object. This method is 
		called explicitly after sortByNumber (which I suppose is then also a 
		dependency) because it takes advantage of the sorted array by using 
		binary search to find the correct year.
	*/	
		// Binary search
		int lo = 0,
			hi = arr.length - 1,
			
		// Initialize these so they're accessible outside the loop
			center = 0,
			courseYear = 0;
		
		while (lo <= hi) {
			center = lo + (hi - lo) / 2;
			courseYear = yearFromNumber(arr[center]);
			if (year < courseYear)
				hi = center - 1;
			else if (year > courseYear)
				lo = center + 1;
			else
				break; // Now inside the range
		}
	/*
		Now reached a target - one of many though, and now need to find a range 
		of values. Exponential searching was suggested online at 
		http://stackoverflow.com/questions/12144802/ but after some 
		attempts in Javascript I found the code too complex. Decided to 
		use a linear search instead despite the performance hit.
	*/
		int lCursor = center,
			rCursor = center;
	/*
		These try...catch blocks are because a cursor may go out of bounds. The 
		exception is not handled because it will simply stop decreasing the 
		cursor when it goes out of bounds - which is perfect.
	*/
		try {
			// Linear search left
			while (yearFromNumber(arr[lCursor - 1]) == courseYear) lCursor--;
		} catch (Exception e) {} // Doesn't matter
		
		try {
			// Linear search right
			while (yearFromNumber(arr[rCursor + 1]) == courseYear) rCursor++;
		} catch (Exception e) {}
	/*
		Used to use System.arraycopy but it was giving me trouble.
		Note: rCursor has + 1 because the last parameter is exclusive.
	*/
		UvicCourse[] coursesWithTargetYear = Arrays.copyOfRange(arr, lCursor, rCursor + 1);
		listCoursesInDept(targetDept, coursesWithTargetYear);
	}

/*
	sortByNumber orders the given array of courses by each item's number field. 
	The operation occurs in place, without generating another array.
*/	
	public static void sortByNumber(UvicCourse[] arr) {
	/*
		The data to be sorted is going to be random (at least in the example 
		lists) so I've decided to use an insertion sort since it will be faster 
		than selection or bubble sorts.
	*/
		for (int i = 1; i < arr.length; i++) {
			UvicCourse course = arr[i];
			
			int num = course.getNum(),
				pos = i; // position copies the index because it will change
			
			while (pos > 0 && arr[pos - 1].getNum() > num) {
				arr[pos] = arr[pos - 1];
				pos--;
			}
			
			arr[pos] = course;
		}
	}

/*
	printArray uses the toString method in UvicCourse.java to print each course 
	in the UvicCourse[] array given.
	
	Wanted to align the data in columns with tabs but thought it could break 
	the automatic marking program...
*/
	public static void printArray(UvicCourse[] arr) {
		for (UvicCourse course : arr) System.out.println(course);
		System.out.println();
	}
}
