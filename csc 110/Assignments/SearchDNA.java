/*
	ID: V00857826 Grant Hames-Morgan
	SearchDNA
	Iterates through an array of DNA sequences to find duplicates and mutations.
	Reads a file and outputs to stdout.
*/

import java.util.Scanner;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class SearchDNA {

	public static void main(String[] args) {
		// Required testing:
		System.out.print("Testing: ");

		String[] testArray = {"AC", "ACCC", "GGTTCC", "GTC", "AC"};
	/*
		This test can't be included with the other tests becuase it would
		be a string in an int[] array.
	*/
		if (findLongest(testArray) != "GGTTCC")
			System.out.println("Test for findLongest failed.");

		// Tests are stored as: { operation(args), correct answer }
		int[][] numericalTests = {
			{ countTotalMutations(testArray)		, 2 },
			{ findFrequency("AC", testArray)		, 2 },
			{ findFreqWithMutations("AC", testArray), 3 }
		};

		// For-else in Java: http://stackoverflow.com/questions/13069402/
		notOkay : {
			for (int[] result : numericalTests)
			/*
				Variable declaration is not allowed in this type of block. The
				computed value is result[0] and the correct answer is result[1].
			*/
				if (result[0] != result[1]) {
					System.out.println("Failed. Aborting.");
					break notOkay;
				}
			System.out.println("Passed.");
		}

		Scanner reader = new Scanner(System.in);
		String[] arr = readArray(reader);
		
		printArray(arr);
		System.out.println("File contains " + arr.length + " sequences.");
		
		String longestSequence = findLongest(arr);
		System.out.println("Longest sequence is " + longestSequence);
		
		int numberOfMutations = countTotalMutations(arr);
		System.out.println("Found " + numberOfMutations + " mutations.");
		
		System.out.print("Sequence to search for: ");
		String toFind = reader.next();
		
		System.out.println("Sequences found: " + findFrequency(toFind, arr));
		System.out.println("Including mutations: " + findFreqWithMutations(toFind, arr));
	}
	
	public static String[] readArray(Scanner reader) {
		Scanner fileReader = null;

		while (true) {
			System.out.print("Enter the name of the file to read: ");
			String file = reader.next();
			try {
			/*
				If the file didn't start with a number this would be the
				easiest way to do it:

				//http://stackoverflow.com/questions/326390
				byte[] encoded = Files.readAllBytes(Paths.get(file));
				return new String(encoded).split(" ");
			*/
				fileReader = new Scanner(new File(file));
				break;
			} catch (Exception err) {
				System.out.println("Couldn't use that file:\n" + err + "\n");
			}
		}

		int size = fileReader.nextInt();
		String[] sequenceArr = new String[size];
		for (int i = 0; i < size; i++) sequenceArr[i] = fileReader.next();

		return sequenceArr;
	}

	public static int countTotalMutations(String[] arr) {
		int count = 0;
		for (String sequence : arr) {
			char lastLetter = ' ';
			
			for (char letter : sequence.toCharArray()) {
				if (letter == lastLetter) {
					count++;
					break; // Move to the next sequence
				} else {
					lastLetter = letter;
				}
			}
		}
		
		return count;
	}
	
	public static int findFrequency(String target, String[] arr) {
		int count = 0;
		for (String sequence : arr)
			if (sequence.equalsIgnoreCase(target)) count++;
		
		return count;
	}
/*
	`repairMutated` is a helper method that avoids duplicate code to
	determine if a sequence is mutated. It returns a non-mutated
	version of a given sequence.
*/
	public static String repairMutated(String sequence) {
	/*
		Java has odd `IndexOutOfBounds` exceptions so to get around them we
		move the first letter to `result` and then iterate over the
		remaining letters as a char array.

		Explanation of method used:

		`String.valueOf( ... )` converts a char to a string so
		`result += letter` will work as expected. Also note the exception
		will not occur for `sequence.charAt(0)` because the given sequence
		will never be empty.
	*/
		String result = String.valueOf(sequence.charAt(0));

		// If only Java had Python-like slices: `for letter in sequence[1:]:`
		for (char letter : sequence.substring(1, sequence.length()).toCharArray())
			if (letter != result.charAt(result.length() - 1)) result += letter;
		
		return result;
	}
	
	public static int findFreqWithMutations(String target, String[] arr) {
		// Rationalize the target: If target is mutated, repair it.
		target = repairMutated(target);
		
		int count = 0;
		for (String sequence : arr) {
			// `||` to avoid calling `repairMutated` if we don't need to.
			if (sequence.equalsIgnoreCase(target) ||
				repairMutated(sequence).equalsIgnoreCase(target)) count++;
		}
		
		return count;
	}
	public static String findLongest(String[] arr) {
		String longest = "";
		int length = 0; // Avoid calculating `longest.length()` multiple times.
		
		for (String sequence : arr) {
			int seqLength = sequence.length();
			if (seqLength > length) {
				length = seqLength;
				longest = sequence;
			}
		}
		
		return longest;
	}
	
	public static void printArray(String[] arr) {
		for (String sequence : arr) System.out.println(sequence);
	}
}
