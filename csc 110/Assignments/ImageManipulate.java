/*
	ID: V00857826 Grant Hames-Morgan
	ImageManipulate
	This program is given a path to an image file and then modifies the image 
	by a given operation. Options including tiling, rotation, inversion of 
	intensity, conversion to ASCII art, and horizontal or vertical mirroring.
*/

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;

import java.util.Map;
import java.util.HashMap;

public class ImageManipulate {
	/**
	 * The main method uses the args array to determine and call
	 * the correct image manipulation method. Index 0 of the 
	 * args array contains the name of the input file, index 1 
	 * the name of the output file, and index 3 the name of the operation.
	 *
	 * @param args The array of Strings 
	 */
	public static void main(String[] args) {
		// Has the user executed the program with enough arguments?
		if (args.length < 3) {
			System.out.println("Invalid program execution, please use:");
			System.out.println("java ImageManipulate <inputFile> <outputFile> <operation>");
			return;
		}
		// Set up variables based on arguments array.
		String inpFile = args[0];
		String outFile = args[1];
		String operation = args[2];
		
		// Convert the image file into a 2D array of integers.
		int[][] data = readGrayscaleImage(inpFile);
		
		// Create an array to hold the resulting values after a manipulation has been called
		int[][] result = null;

		// Check what operation should be run, and call the corresponding method
		switch (operation) {
			case "invert":
				result = invert(data);
				break;

			case "makeAscii":
				makeAscii(data, outFile);
				return; // Don't use writeGrayscaleImage afterwards.

			case "tile":
				try {
					int x = Integer.parseInt(args[3]),
						y = Integer.parseInt(args[4]);
					result = tile(data, x, y);
				} catch (IndexOutOfBoundsException err) {
					System.out.println("Use 'tile <x> <y>'");
					return;
				}

				break;

			case "verticalMirror":
				result = verticalMirror(data);
				break;

			case "horizontalMirror":
				result = horizontalMirror(data);
				break;

			case "rotate":
				result = rotate(data);
				break;

			default:
				System.out.println("Not a valid operation. Exiting.");
				return;
		}
		
		writeGrayscaleImage(outFile, result);
	}

	static int[][] invert(int[][] arr) {
		int[][] result = new int[arr.length][arr[0].length];

		for (int row = 0; row < arr.length; row++) {
			for (int col = 0; col < arr[row].length; col++)
				result[row][col] = 255 - arr[row][col];
		}
		
		return result;
	}
	
	static void	makeAscii(int[][] arr, String outName) {
	/*
		Thought about using two arrays for matching ASCII characters to
		ranges by a shared index but decided to use a map to avoid any
		possibility of mixing up indexes.
	*/
		Map<Character, int[]> ls = new HashMap<Character, int[]>() {{
			put('M', new int[] {  0,  20}); put('L', new int[] { 21,  40}); put('I', new int[] { 41,  60});
			put('o', new int[] { 61,  80}); put('|', new int[] { 81, 100}); put('=', new int[] {101, 120});
			put('*', new int[] {121, 140}); put(':', new int[] {141, 160}); put('-', new int[] {161, 180});
			put(',', new int[] {181, 200}); put('.', new int[] {201, 220}); put(' ', new int[] {221, 255});
		}};
		
		// Decision to use PrintWriter over PrintStream: http://stackoverflow.com/questions/11372546
		PrintWriter fileWriter = null;
		try {
			fileWriter = new PrintWriter(outName, "UTF-8");
		} catch (Exception err) {
			System.out.println("Couldn't write to that text file:\n" + err);
		}
	
		for (int row = 0; row < arr.length; row++) {
			String result = "";
			for (int col = 0; col < arr[0].length; col++) {
				int value = arr[row][col];
				for (Map.Entry<Character, int[]> line : ls.entrySet()) {
					int[] range = line.getValue();
					if (value >= range[0] && value <= range[1]) {
						result += line.getKey();
						break;
					}
				}
			}
			
			// Write the result
			fileWriter.println(result);
		}
		
		fileWriter.close();
	}

	static int[][] tile(int[][] arr, int timesWidth, int timesHeight) {
		int x = arr[0].length,
			y = arr.length;

		// Set up new image's size
		int newWidth  = x * timesWidth,
			newHeight = y * timesHeight;
		
		int[][] result = new int[newHeight][newWidth];
	/*
		The .arraycopy() method expands the row of pixels of the
		original image as many times as needed.
	*/
		for (int row = 0; row < y; row++) {
			for (int i = 0; i < timesWidth; i++) {
				System.arraycopy(arr[row], 0, result[row], x * i, x);
			}
		}
		
		// This copies each line of the horizontally tiled result downwards
		for (int i = y; i < newHeight; i++)
			result[i] = result[i - y];
		
		return result;
	}

	static int[][] verticalMirror(int[][] arr) {
		int x = arr[0].length,
			y = arr.length;

		for (int row = 0; row < y; row++) {
			int[] line = new int[x];
			for (int i = 1; i <= x; i++)
				line[i - 1] = arr[row][x - i];
			arr[row] = line;
		}

		return arr;
	}
	
	static int[][] horizontalMirror(int[][] arr) {
		int x = arr[0].length,
			y = arr.length;
		int [][] result = new int[y][x];

		for (int i = 1; i <= y; i++)
			result[i - 1] = arr[y - i];
		return result;
	}
	
	static int[][] rotate(int[][] arr) {
		int x = arr[0].length,
			y = arr.length;

		// Note that the following array has y and x switched
		int[][] result = new int[x][y];

		for (int col = 0; col < x; col++) {
			for (int row = 0; row < y; row++)
				result[col][row] = arr[row][col];
		}

		return result;
	}

    /** THIS METHOD MAY BE CALLED, BUT MUST NOT BE MODIFIED!
     * This method reads an image file and returns a 2D array
	 * of integers. Each value in the array is a representation
	 * of the corresponding pixel's grayscale value.
	 *
	 * @param filename The name of the image file
	 * @return A 2D array of integers.
	 */
    public static int[][] readGrayscaleImage(String filename) {
        int [][] result = null;
        try {
            File imageFile = new File(filename);
            BufferedImage image = ImageIO.read(imageFile);
            int height = image.getHeight();
            int width  = image.getWidth();
            result = new int[height][width];
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    int rgb = image.getRGB(x, y);
                    result[y][x] = rgb & 0xff;
                }
            }
        }
        catch (IOException ioe) {
            System.err.println("Problems reading file named " + filename);
            System.exit(1);
        }
        return result;
    }


    /** THIS METHOD MAY BE CALLED, BUT MUST NOT BE MODIFIED!
     * This method reads a 2D array of integers and creates
	 * a grayscale image. Each pixel's grayscale value is
	 * based on the corresponding value in the 2D array.
	 *
	 * @param filename The name of the image file to create
	 * @param array The 2D array of integers
	 */
    public static void writeGrayscaleImage(String filename, int[][] array) {
        int width = array[0].length;
        int height = array.length;

        try {
            BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);

            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    int rgb = array[y][x];
                    rgb |= rgb << 8;
                    rgb |= rgb << 16;
                    image.setRGB(x, y, rgb);
                }
            }
            File imageFile = new File(filename);
            ImageIO.write(image, "jpg", imageFile);
        }
        catch (IOException ioe) {
            System.err.println("Problems writing file named " + filename);
            System.exit(1);
        }
    }
}
