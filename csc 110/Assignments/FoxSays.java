/*
 * ID: 	V00857826 Grant Hames-Morgan
 * FoxSays
 * Prints ASCII fox with the entered text above it.
 * Takes in text input through command line and outputs to stdout.
 */

import java.util.Scanner;

public class FoxSays {

	public static void main(String[] args) {
		theMessage();
		printFox();
	}

	public static void printIterable(String[] object) {
		for (String line : object) System.out.println(line);
	}

	public static void theMessage() {
		System.out.print("What does the fox say?: ");
		Scanner read = new Scanner(System.in);

		String input = read.nextLine();
		System.out.println();

		// Just print the fox if there's no message
		if (input.isEmpty()) return;

		input = "| " + input + " |";
		String boxLine = new String(new char[input.length()]).replace("\0", "*");

		String[] list = {boxLine, input, boxLine, "    |"}; // Centered over fox
		printIterable(list);
	}

	public static void printFox() {
		String[] foxDrawing = {
			" /\\   /\\",
			"/   -   \\",
			"\\/ o o \\/     ____ ",
			" \\_\\*/_/     /\\  /",
			"  /   \\_    /  \\/",
			"  \\     \\_ /   / ",
			"   [ [ _/  \\ /",
			"   [ [  \\  // "
		};

		printIterable(foxDrawing);
	}
}