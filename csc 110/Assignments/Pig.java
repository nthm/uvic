/*
	ID: V00857826 Grant Hames-Morgan
	Pig
	Text (CLI) version of the dice game Pig.
	Continously asks the user if they want to roll again (Yes/No) and outputs to stdout.
*/

import java.util.Scanner;
import java.util.Random;

public class Pig {

	public static void main(String[] args) {
	/*	// Tests
		Random rand = new Random();
		for (int i = 0; i < 20; i++) System.out.println("Rolled " + diceRoll(rand));
		printSpace("Dice tests complete.");

		// There's no eval() function in Java so there isn't a simple way to
		// combine these turn tests into one loop.

		for (int i = 0; i < 2; i++) {
			int score = computerTurn(rand);
			printSpace("Score returned: " + score);
		}
		printSpace("Computer tests complete.");

		Scanner input = new Scanner(System.in);
		for (int i = 0; i < 2; i++) {
			int score = playerTurn(input, rand);
			printSpace("Score returned: " + score);
		}
		printSpace("Player tests complete.");
	*/
		printSpace("Welcome to the game of Pig.");
		gameLoop();
	}

	public static void printSpace(String toPrint) {
		/*	This method saves me from typing "\n" for any print statement that
			ends a section in the game such as a round or when scores are printed. */

		System.out.println(toPrint + "\n");
	}

	public static int diceRoll(Random rand) {
		return rand.nextInt(6) + 1;
	}

	public static int playerTurn(Scanner input, Random rand) {
		int turnScore   = 0;
		String response	= "";
		
		do {
			int roll = diceRoll(rand);
			if (roll == 1) {
				printSpace(
					"Uh oh, you rolled a 1!\n" +
					"Your turn is over and your get 0 points this round.");
				return 0; // End the turn early and add 0 to the score.
			}
			
			turnScore += roll;
			System.out.println(
				"You rolled a " + roll + ". Your score this turn is " + turnScore + "\n" +
				"Would you like to roll again?");
			response = input.next();
			
		} while (Character.toUpperCase(response.charAt(0)) == 'Y'); // Anything starting with a Y.
		
		printSpace("Ending your turn with a score of " + turnScore);
		return turnScore;
	}

	public static int computerTurn(Random rand) {
		int turnScore = 0;
		for (int i = 0; i < 4; i++) {
			int roll = diceRoll(rand);
			if (roll == 1) {
				printSpace("Computer rolled a 1, ending its turn with a score of 0 this round.");
				return 0;
			}
			
			turnScore += roll;
			System.out.println("Computer rolled a " + roll + ". Its score this turn is " + turnScore);
		}
		
		printSpace("Ending computer's turn with a score of " + turnScore);
		return turnScore;
	}

	public static void gameLoop() {
		Random rand   = new Random();
		Scanner input = new Scanner(System.in);
		
		int playerScore   = 0,
			computerScore = 0;

		while (playerScore < 100 && computerScore < 100) {
			playerScore   += playerTurn(input, rand);
			computerScore += computerTurn(rand);
			
			System.out.println("The scores at the end of the current round are:");
			printSpace("Player: " + playerScore + "\tComputer: " + computerScore);
		}

		if (playerScore > 100)
			printSpace("You win!");
		else
			printSpace("You lose. Computer wins!");
	}
}