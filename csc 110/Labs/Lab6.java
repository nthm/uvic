import java.util.Arrays;

public class Lab6 {
	public static void main(String[] args) {
		int[] intArgs = new int[args.length];
		
		for (int i = 0; i < args.length; i++)
			intArgs[i] = Integer.parseInt(args[i]);
		
		printIntArray(intArgs);
		
		// Part II
		swapIndexes(intArgs, 1, 2);
		System.out.println("Swapped indices: " + Arrays.toString(intArgs));
		
		// Now we're learning about for (type element : iterable) {} ...
		
		// Part III
		int[] numbers = {2, 1, 3, 5, 1, 2, 6, 7, 2, 9};
		removeRepeatition(numbers);
	}
	
	public static void printIntArray(int[] array) {
		/*
			The instructor talked a lot about how the length of an array is 
			found by array.length, no parentheses. This is because 'length' 
			is a property of the object - the array. It's not a function.
		*/
		for (int item : array) {
			System.out.println(item);		
		}
	}
	
	public static void swapIndexes(int[] array, int a, int b) {
		int hold = array[a];
		
		array[a] = array[b];
		array[b] = hold;
	}
	
	public static void removeRepeatition(int[] array) {
		int currentMax = 0;
		String items = "";
		
		for (int item : array) {
			if (item > currentMax) {
				currentMax = item;
				items += item;
			}
		}
		
		System.out.println("Array no repeatition: " + items);
	}
}