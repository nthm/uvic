import java.util.Scanner;
import java.io.File;

public class Lab8 {
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		// Part I
		echoInput(reader);
		
		// Part II
		System.out.print("Enter a file path to echo:");
		echoFile(reader.nextLine());
	}
	
	public static void echoInput(Scanner reader) {
		System.out.println("Enter text to be copied:");
		while (reader.hasNextLine())
			System.out.println(reader.nextLine() + "\nAgain?");
		// To break from the loop, there's Ctrl-Z (on Windows) which sends an EOF
	}
	
	public static void echoFile(String readfilename) {
		File data = new File(readfilename);
		Scanner reader = null;
		try {
			reader = new Scanner(data);	
		} catch (Exception err) {
			System.out.print("Couldn't use that file:\n" + err);
			return;
		}
		
		while (reader.hasNextLine())
			System.out.println(reader.nextLine());
		
		reader.close();
	}
}