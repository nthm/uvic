import java.util.Scanner;

public class Lab4 {
	public static void main(String[] args) {
		// System.out.println(max(3, 4));
		// average();
		pattern(5);
	}
	
	public static int max(int a, int b) {
		return a > b ? a : b; 
	}

	public static void average() {
		System.out.println("Enter numbers to average. Stop by entering something else:");
		Scanner read = new Scanner(System.in);
		int total 	= 0,
			howMany = 0;
		
		while (read.hasNextInt()) {
			total += read.nextInt();
			howMany++;
		}
		
		float average = (float) total / howMany;
		System.out.println("Average is: " + average);
	}
	
	public static void trees(int lines, int padding) {
		for (int stars = 1; stars < lines; stars++) {
			String line = new String(new char[padding - stars]).replace("\0", " ")
						+ new String(new char[2 * stars - 1]).replace("\0", "*");
			System.out.println(line);
		}
		
		System.out.println(new String(new char[padding - 1]).replace("\0", " ") + "|\n");
	}
	
	public static void pattern(int levels) {
		for (int i = 2; i < levels + 2; i++) {
			trees(i, levels);
		}
	}
}
