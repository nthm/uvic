import java.util.Arrays;
import java.util.Random;

public class Lab7 {
	public static void main(String[] args) {
		int[][] intArray = {
			{ 1, 2, 3, 4 },
			{ 5, 6 },
			{ -2, -10, 5 },
			{ 24, 256 }
		};
		
		// Part I Printing two-dimensional arrays.
		// printArray(intArray);
		
		// Part II
		int[][] intArray2 = new int[5][]; // 5 rows
		for (int i = 0; i < intArray2.length; i++) {
			intArray2[i] = new int[i + 1];
			for (int j = 0; j < intArray2[i].length; j++)
				intArray2[i][j] = -1;
		}
		
		// Part III
		String[] words = { "Hello", "HowAboutNo", "Goodbye" };
		// printArrayLiterally(convertTo2DCharArray(words));
		
		// Part IV
		swapRows(intArray, 3, 2);
		// printArray(intArray);
		
		// Part V
		swapCols(intArray, 0, 1);
		// printArray(intArray);
		
		// Part VI
		minesweepers(4, 4);
	}
	
	public static void printArray(int[][] array) {
		for (int[] line : array) {
			for (int number : line)
				System.out.print(number + " ");
			System.out.println();
		}
	/*
		for (int row = 0; row < intArray.length; row++) {
			for (int col = 0; col < intArray[row].length; col++)
				System.out.print(intArray[row][col] + " ");
			System.out.println();
		}	
	*/
	}
	
	// Methods can have the same name.
	public static void printArrayLiterally(int[][] array) {
		System.out.println(Arrays.toString(array));
	}
	
	public static void printArrayLiterally(char[][] array) {
		System.out.println(Arrays.toString(array));
	}
	
	public static char[][] convertTo2DCharArray(String[] words) {
		int lenWords = words.length; // Avoid calculating this twice.
		char [][] charArray = new char[lenWords][];
		for (int i = 0; i < lenWords; i++)
			charArray[i] = words[i].toCharArray();
		
		return charArray;
	}
	
	public static void swapRows(int[][] arr, int a, int b) {
		int[] tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}
	
	public static void swapCols(int[][] arr, int a, int b) {
		// This method assumes each row has the same length.
		for (int row = 0; row < arr.length; row++) {
			int tmp = arr[row][a];
			arr[row][a] = arr[row][b];
			arr[row][b] = tmp;
		}
	}
	
	public static void minesweepers(int width, int height) {
		int[][] board = new int[width][height];
		
		// 25% of the board will be mines:
		Random rand = new Random();
		for (int i = 0; i < (width * height) / 4; i++)
			board[rand.nextInt(width)][rand.nextInt(height)] = -1;
		
		// Populate board
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < intArray[row].length; col++) {
				if (board[row][col] = -1) surroundPoint(row, col);
			}
		}
		
		// Print completed board 
		printArray(board);
	}
	
	public static void surroundPoint(int row, int col) {
		// TODO. Increase all points surrounding the mine by 1 as long as they are not out of range or mines themselves.
	}
}