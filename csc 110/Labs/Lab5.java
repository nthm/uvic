import java.awt.Point;

public class Lab5 {
	public static void main(String[] args) {
		// This lab will go over the Point class
		int x = 5,
			y = 10;
		
		// Part I
		
		Point p = new Point(x, y);
		System.out.println("Point is " + printPoint(p));
		swapPoint(p);
		System.out.println("Point is now " + printPoint(p));
		
		// Part II
		
		String s = "Hello";
		replaceLetters(s);
		System.out.println("After replaceLetters, variable s is: " + s);	
	/*
		The reason this doesn't work and swapPoint does is that strings are 
		immutable in Java (as with most languages). Data types of all numbers 
		have immutable versions as well:
			
			Mutable | Immutable
		----------- | -------------
			int 	| Integer
			double 	| Double
			... 	| 
	
		There is no version of String that is mutable.
	*/
		String str1 = "This is a string",
			   reversed = reverse(str1);
		
		System.out.println("Final reversed string: " + reversed);
	}
	
	public static String printPoint(Point p) {
		return String.format("(%d, %d)" , p.x, p.y );
	}
	
	public static void swapPoint(Point p) {
		int tmp = p.x;
		p.x = p.y;
		p.y = tmp;
		System.out.println("Swapped x and y");
	}
	
	public static void replaceLetters(String s) {
		s = s.replace('l', 'o');
		System.out.println("Replacing all occurences of 'l' with 'o': " + s);
	}
	
	// Part III (Learning how to make proper comments)
	
	/**
	 * This program takes in a string and reverses it.
	 * 
	 * @param input			String to be reversed
	 * @return				Reversed string
	 */
	
	public static String reverse(String input) {
		// Check input string:
		System.out.println("Input string is: " + input);
		String reversed = "";
		
		for (int i = 0; i < input.length(); i++) {
			reversed = input.charAt(i) + reversed;
			
			// For debugging:
			System.out.println("Reversed string: " + reversed);
		}
		
		return reversed;
	}	
}
