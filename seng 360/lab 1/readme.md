Hash a few thousand common passwords given an IV and find which matches a given
ciphertext.

I didn't include the word list here.

It takes 3-4 seconds on my 2008 laptop to get the password in Node, but 14
minutes to do the same in Fish shell:

```fish
#!/usr/bin/fish

cat 360\ Lab\ 1\ English\ words.txt | while read line
    set KEY (string join '' "$line" (string repeat -n (expr 16 - (string length "$line")) '#') | tr -d '\n' | od -A n -t x1 | sed 's/ *//g')
    set ENC (echo -n 'This is a top secret.' | openssl enc -aes-128-cbc -e -K "$KEY" -iv 'aabbccddeeff00998877665544332211' | od -A n -t x1 | tr -d '\n' | sed 's/ *//g')
    echo "$ENC $line" >> result.txt
end
```
