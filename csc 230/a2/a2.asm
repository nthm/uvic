.equ SPL  = 0x5D            ; stack pointers
.equ SPH  = 0x5E
.equ SREG = 0x5F

.equ DDRB  = 0x24           ; port definitions
.equ PORTB = 0x25
.equ DDRL  = 0x10A
.equ PORTL = 0x10B

.equ ADCSRA = 0x7A          ; control and status registers A/B
.equ ADCSRB = 0x7B
.equ ADMUX  = 0x7C          ; multiplexer
.equ ADCL   = 0x78          ; ADC out (low)
.equ ADCH   = 0x79          ; ADC out (high)

.equ ASSR   = 0xB6          ; timer 2 addresses
.equ OCR2A  = 0xB3
.equ TCCR2A = 0xB0
.equ TCCR2B = 0xB1
.equ TIFR2  = 0x37
.equ TIMSK2 = 0x70

; interrupt addresses, see page 101 of datasheet

.equ ADCCaddr = 0x003A      ; ADC conversion complete
.equ OC2Aaddr = 0x001A      ; timer 2 compare match A

.equ RAMEND = 0x21FF

; button values from the ADC
; A (v 1.1)                 ; B (v 1.0)
.equ ADC_BTN_RIGHT  = 0x032 ; 0x032
.equ ADC_BTN_UP     = 0x0C3 ; 0x0C3 FA
.equ ADC_BTN_DOWN   = 0x17C ; 0x17C C2
.equ ADC_BTN_LEFT   = 0x22B ; 0x22B 8A
.equ ADC_BTN_SELECT = 0x316 ; 0x316 52

; -----------------------------------------------------------------------------
; register definitions - r16:r17 can be overwritten between ISRs

.def rTMP   = r16           ; temporary worker
.def rPORTB = r16           ; holds PORTB while in SHIFT_LEDS
.def rPORTL = r17           ; holds PORTL
.def rLIMIT = r18           ; interrupt counter threshold to call SHIFT_LEDS
.def rCOUNT = r19           ; interrupt counter
.def rSTATE = r20           ; program state, see below

.def XL = r26               ; holds ADCL:ADCH on ADC conversion complete
.def XH = r27

; the high bits of each buttons are stored r21:r25 since there's no compare
; immediate with carry; only compare with carry. see INIT_REGISTERS

; each bit of state register, r20:
; 7   if moving backward
; 6   if inverted
; 5   if button pushed
; 4:0 unused

; -----------------------------------------------------------------------------
; setup

.cseg
.org 0x0000
    rjmp ISR_RESET          ; reset
.org ADCCaddr
    rjmp ISR_BUTTON         ; analog conversion complete
.org OC2Aaddr
    rjmp ISR_COUNT_INT      ; timer 2 compare match

; last interrupt is 0x0070, 0x0072 is the first unreserved location

.org 0x0072
ISR_RESET:
    ldi rTMP, high(RAMEND)  ; stack initialization
    sts SPH, rTMP
    ldi rTMP, low(RAMEND)
    sts SPL, rTMP

    ser rTMP                ; set data direction of all (0xff) pins
    sts DDRB, rTMP          ; to PORTB
    sts DDRL, rTMP          ; to PORTL

    call INIT_REGISTERS     ; setup program state, and high bits of buttons
    call INIT_ADC
    call INIT_TIMERS

    ldi rPORTB, 0x02        ; start at pin 52 (bit 2 PORTB)
    sts PORTB, rPORTB

    sei                     ; enable interupts globally

; -----------------------------------------------------------------------------
; the entire program is empty

loop:
    rjmp loop

; -----------------------------------------------------------------------------
; initialize registers - see "register definitions"

INIT_REGISTERS:
    ldi rLIMIT, 0x40        ; interrupt compare match set to 64
    clr rCOUNT              ; interrupt counter
    clr rSTATE              ; state

    ldi r21, high(ADC_BTN_RIGHT) ; 0x00
    ldi r22, high(ADC_BTN_UP)    ; 0x00
    ldi r23, high(ADC_BTN_DOWN)
    ldi r24, high(ADC_BTN_LEFT)
    ldi r25, high(ADC_BTN_SELECT)
    ret

; -----------------------------------------------------------------------------
; analog to digital conversion

INIT_ADC:
    ; ADCSRA: ADC see page 285
    ; 1   ADEN     enable
    ; 1   ADSC     start conversion
    ; 0   ADATE    auto trigger enable: auto start following a clock (timer)
    ; 0   ADIF     interupt flag (set by ADIE)
    ; 1   ADIE     trigger "ADC Conversion Complete" interupt
    ; 111 ADPS2:0  prescaler from 2 to 128

    ldi rTMP, 0xcf          ; 0xcf is on, 0x4f is off
    sts ADCSRA, rTMP

    ; ADCSRB disabled (all bits 0) - would be used for ADATE in ADCSRA
    ldi rTMP, 0x00
    sts ADCSRB, rTMP

    ; ADMUX (MUX4:MUX0 = 00000, ADLAR = 0, REFS1:REFS0 = 1)
    ldi rTMP, 0x40
    sts ADMUX, rTMP
    ret

; -----------------------------------------------------------------------------
; timer 2 initialization

; timer 2 hits a compare match at 245 which triggers ISR_COUNT_INT incrementing
; r19 until it matches r18 (64, 32, 16, 8, 4, or 2) triggering SHIFT_LEDS

INIT_TIMERS:
    ldi rTMP, 0x00          ; sync
    sts ASSR, rTMP
    ldi rTMP, 0x07          ; clock 16 000 000 Hz (system clock) /1024
    sts TCCR2B, rTMP
    ldi rTMP, 0xf5          ; set output compare value to 245
    sts OCR2A, rTMP
    ldi rTMP, 0x02          ; interrupt on compare match A
    sts TIMSK2, rTMP
    ldi rTMP, 0x07          ; clears timer interupt flag (note sei is not set)
    sts TIFR2, rTMP
    ret

; -----------------------------------------------------------------------------
; shift leds

; pins
; 42 - L, bit 7
; 44 - L, bit 5
; 46 - L, bit 3
; 48 - L, bit 1
; 50 - B, bit 3
; 52 - B, bit 1

SHIFT_LEDS:
    lds rPORTB, PORTB
    lds rPORTL, PORTL
    sbrs rSTATE, 6          ; if inversion bit is set, uninvert for the shift
    rjmp start_shift
    com rPORTB
    com rPORTL
start_shift:
    sbrc rSTATE, 7          ; if direction bit is set, go backward
    rjmp backward
forward:
    sbrc rPORTB, 3          ; skip the port carry if pin 50 is 0
    rjmp port_carry_forward
    lsl rPORTB
    lsl rPORTB
    lsl rPORTL
    lsl rPORTL
    sbrc rPORTL, 7          ; if pin 42 is on, the NEXT isr moves backward
    sbr rSTATE, 0x80        ; set direction to backwards
    rjmp write
port_carry_forward:         ; shift bit 4 of PORTB into bit 2 of PORTL
    clr rPORTB
    ldi rPORTL, 0x02        ; bit 1
    rjmp write
backward:
    sbrc rPORTL, 1
    rjmp port_carry_backward
    lsr rPORTB
    lsr rPORTB
    lsr rPORTL
    lsr rPORTL
    sbrc rPORTB, 1          ; if pin 52 is on, the NEXT isr moves forward
    cbr rSTATE, 0x80        ; set direction to forward
    rjmp write
port_carry_backward:        ; shift bit 2 of PORTL into bit 4 of PORTB
    clr rPORTL
    ldi rPORTB, 0x08        ; bit 3
    rjmp write              ; optimized out by compiler
write:
    call WRITE_LEDS
    ret

; the following is shared with ISR_BUTTON for inversion

WRITE_LEDS:
    sbrs rSTATE, 6          ; if inversion state is on, invert before writing
    rjmp send
    com rPORTB
    com rPORTL
send:
    sts PORTB, rPORTB
    sts PORTL, rPORTL
    ret

; -----------------------------------------------------------------------------
; count timer 2 compare interrupts, call SHIFT_LEDS at threshold (rLIMIT)

ISR_COUNT_INT:
    inc rCOUNT
    cp rCOUNT, rLIMIT
    brsh clear              ; if counter >= (speed changed) or == to rLIMIT
    reti
clear:
    call SHIFT_LEDS
    clr rCOUNT
    reti

; -----------------------------------------------------------------------------
; handle button pushes

; all buttons follow this algorithm and are ordered by their voltages:
; name:
;     compare XL:XH to the button threshold (see .equ directives)
;     if the ADC voltage is too low, move to check the next button
;     do work

ISR_BUTTON:
    lds rTMP, ADCSRA        ; request a new ADC conversion immediately
    ori rTMP, 0x40
    sts ADCSRA, rTMP
    lds XL, ADCL            ; check if a button is being pushed
    lds XH, ADCH
    cpi XL, low(ADC_BTN_SELECT)
    cpc XH, r25
    brlo pushed             ; if it's being pushed, jump, else...
    cbr rSTATE, 0x20        ; save direction/inversion, clear push state, reti
    reti
pushed:
    sbrc rSTATE, 5          ; if a button is being pushed already, reti
    reti
    sbr rSTATE, 0x20        ; set that a button is being pushed
    lds rPORTB, PORTB
    lds rPORTL, PORTL
right:                      ; unused label
    cpi XL, low(ADC_BTN_RIGHT)
    cpc XH, r21
    brsh up
    sbrs rSTATE, 6          ; if inversion bit is already cleared, reti
    reti
    call WRITE_LEDS
    cbr rSTATE, 0x40        ; clear the inversion bit AFTER inverting the LEDs
    reti
up:
    cpi XL, low(ADC_BTN_UP)
    cpc XH, r22
    brsh down
    sbrc rLIMIT, 1          ; don't go faster than bit 1
    reti
    lsr rLIMIT              ; decrease delay by 2 (bitshift)
    reti
down:
    cpi XL, low(ADC_BTN_DOWN)
    cpc XH, r23
    brsh left
    sbrc rLIMIT, 6          ; don't go slower than bit 6
    reti
    lsl rLIMIT              ; increase delay by 2 (bitshift)
    reti
left:
    cpi XL, low(ADC_BTN_LEFT)
    cpc XH, r24
    brsh select
    sbrc rSTATE, 6          ; if inversion bit is already set, reti
    reti
    sbr rSTATE, 0x40        ; set the inversion bit
    call WRITE_LEDS
    reti
select:                     ; no comparison because it was checked at the start
    ldi rTMP, 0x02          ; interrupt bit in TIMSKY
    lds r17, TIMSK2         ; r17 is worker
    eor r17, rTMP           ; toggle
    sts TIMSK2, r17
    reti