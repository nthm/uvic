; mul32.asm
; CSC 230 - Summer 2017
;
; Starter code for assignment 1
;
; B. Bird - 04/30/2017

.cseg
.org 0

	; Initialization code
	; Do not move or change these instructions or the registers they refer to. 
	; You may change the data values being loaded.
	; The default values set A = 0x3412 and B = 0x2010
	ldi r16, 0x14 ; Low byte of operand A
	ldi r17, 0x30 ; High byte of operand A
	ldi r18, 0xF0 ; Low byte of operand B
	ldi r19, 0x01 ; High byte of operand B
	
	; Your task: compute the 32-bit product A*B (using the bytes from registers r16 - r19 above as the values of
	; A and B) and store the result in the locations OUT3:OUT0 in data memory (see below).
	; You are encouraged to use a simple loop with repeated addition, not the MUL instructions, although you are
	; welcome to use MUL instructions if you want a challenge.
	
	;   24  -->  (20 + 4) * (70 + 7)  -->  (2*10^1 + 4*10^0)(7*10^1 + 7*10^0)
	; x 77
	; ---- 

	; (x_h*10^1 + x_l*10^0)(y_h*10^1 + y_l*10^0)
	; (x_hy_h)*10^2 + (x_hy_l + x_ly_h)*10^1 + x_ly_l

	; this would use 7 MUL instructions, 2 cycles/MUL and 3 ADDs, 1 cycle/ADD.
	; total of 16 cycles. however, the bits can be placed in the right 8 bit section instead
	; MOVW is used rather than 2 MOVs because it's only 1 cycle each

	; the MUL instruction can raise the carry flag, so I thought it could overflow. then 
	; there would be 7 instructions which could overflow, so it would be best to store 
	; carries in a register incase two carries need to be applied to the same bit and then 
	; the carry would need to be shifted forward. however (!), the MUL never overflows!?

	; spent a lot of time trying to handle this:

	; after x_hy_h
	; 0000 0000|0000 0000|1111 1111|1111 1111 
	;     0    0    0    0    0    0    0    1
	; after x_ly_l
	; 1111 1111|1111 1111|0000 0000|0000 0000
	;     0    0    0    1    0    0    0    1

	; x_ly_h + x_hy_l
	; 0000 0000|1111 1111|1111 1111|0000 0000
	;     0    0    0    1    0    1    0    1
	;                  twice  1   twice 1 ??

	; but since MUL doesn't overflow, x_hy_h and x_ly_l will never touch eachother, and they 
	; can be placed into OUT2/3 and OUT0/1 respectively without worry

	; x_hy_h which is put into r1 (high), r0 (low)
	mul r17, r19
	movw r23:r22, r1:r0 ; copy r0 to OUT2, r1 to OUT3

	; x_ly_l
	mul r16, r18
	movw r21:r20, r1:r0 ; copy r0 to OUT0, r1 to OUT1

	; x_hy_l + x_ly_h is added one at a time
	
	; x_hy_l
	mul r17, r18
	add r21, r0 ; OUT1 += low bits
	adc r22, r1 ; OUT2 += high bits + carry
	; if that previous adc carries, it needs to be placed into OUT3
	; since it's only the carry just add 0 (r24 is 0)
	adc r23, r24
	
	; x_ly_h - same as above
	mul r16, r19
	add r21, r0
	adc r22, r1
	adc r23, r24 ; if this overflows it's more than 32 bit
	
	sts OUT0, r20
	sts OUT1, r21
	sts OUT2, r22
	sts OUT3, r23
	
	; End of program (do not change the next two lines)

stop:
	rjmp stop

	
; Do not move or modify any code below this line. You may add extra variables if needed.
; The .dseg directive indicates that the following directives should apply to data memory
.dseg 
.org 0x200 ; Start assembling at address 0x200 of data memory (addresses less than 0x200 refer to registers and ports)

OUT0:	.byte 1 ; Bits  7...0 of the output value  LOW
OUT1:	.byte 1 ; Bits 15...8 of the output value  HIGH
OUT2:	.byte 1 ; Bits 23...16 of the output value LOW
OUT3:	.byte 1 ; Bits 31...24 of the output value HIGH
