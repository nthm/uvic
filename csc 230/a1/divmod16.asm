; divmod16.asm
; CSC 230 - Summer 2017
;
; Starter code for assignment 1
;
; B. Bird - 04/30/2017

.cseg
.org 0

	; Initialization code
	; Do not move or change these instructions or the registers they refer to. 
	; You may change the data values being loaded.
	; The default values set A = 0x3412 and B = 0x0003
	ldi r16, 0x00 ; Low byte of operand A
	ldi r17, 0x00 ; High byte of operand A
	ldi r18, 0x02 ; Low byte of operand B
	ldi r19, 0x00 ; High byte of operand B
	
	; Your task: Perform the integer division operation A/B and store the result in data memory. 
	; Store the 2 byte quotient in DIV1:DIV0 and store the 2 byte remainder in MOD1:MOD0.

	; clear the registers which will hold DIV0 and DIV1
	clr r24 ; DIV0
	clr r25 ; DIV1

loop:
	; remainder = A
	; writing to data is only 2 cycles on AVR so using more registers would only 
	; make this harder to understand
	sts MOD0, r16
	sts MOD1, r17

	; backed up r16 and r17 so can use them:
	; remainder - B
	sub r16, r18
	sbc r17, r19
	; if result overflowed (negative), we can't divide anymore - break
	brcs store

	; otherwise, increment to mark that we have successfully divided
	; inc does not handle carry. adiw is most efficient 
	adiw r25:r24, 1

	; loop, overwriting the remainder with the new result
	rjmp loop
	
store:
	sts DIV0, r24
	sts DIV1, r25
	; MOD0/1 have been written. don't rjmp, just fall through to stop

	; End of program (do not change the next two lines)
stop:
	rjmp stop
	
; Do not move or modify any code below this line. You may add extra variables if needed.
; The .dseg directive indicates that the following directives should apply to data memory
.dseg 
.org 0x200 ; Start assembling at address 0x200 of data memory (addresses less than 0x200 refer to registers and ports)

DIV0:	.byte 1 ; Bits  7...0 of the quotient
DIV1:	.byte 1 ; Bits 15...8 of the quotient
MOD0:	.byte 1 ; Bits  7...0 of the remainder
MOD1:	.byte 1 ; Bits 15...8 of the remainder
