#include "CSC230.h"

#define ADC_BTN_RIGHT  0x032
#define ADC_BTN_UP     0x0C3
#define ADC_BTN_DOWN   0x17C
#define ADC_BTN_LEFT   0x22B
#define ADC_BTN_SELECT 0x316

unsigned int interrupts = 0;
unsigned int paused = 0;
unsigned int pushed = 0;

// times are stored little endian: tenths first
char time[5];
char lap_right[5];
char lap_left[5];

void zero_all() {
    for (int i = 0; i < 5; i++) {
        time[i]      = '0';
        lap_right[i] = '0';
        lap_left[i]  = '0';
    }
}

void display(char to_display[5]) {
    char seperator = ':';
    // move backwards through time from 5..1: minutes to tenths
    for (int i = 5; i > 0; i--) {
        lcd_putchar(to_display[i - 1]);
        if (i % 2 == 0) {
            lcd_putchar(seperator);
            seperator = '.';
        }
    }
}

int main() {
    // init ADC
    ADCSRA = 0x8F;         // conversion complete interrupt; prescaler of 128
    ADMUX  = 0x40;         // ADMUX: MUX4:MUX0 = 0, ADLAR = 0, REFS1:REFS0 = 1

    // init timer 0
    TIMSK0 = 0x02;         // interrupt on compare match A
    TCCR0A = 0x02;         // clear on compare match (WGM01)
    TCCR0B = 0x04;         // prescaler of 256
    OCR0A  = 0x7C;         // set output compare value to 125 (0...124)
    TCNT0  = 0x00;
    TIFR0  = 0x02;         // clear comapre match flag

    lcd_init();
    lcd_puts("Time: ");

    zero_all();
    display(time);

    paused = 1;
    sei();
    while (1) {}
}

ISR(TIMER0_COMPA_vect) {
    ADCSRA |= 1<<ADSC;     // request a new ADC conversion

    if ((paused == 0) && (interrupts++ >= 50)) {
        interrupts = 0;
        for (int i = 0; i < 5; i++) {
            if (time[i] == (i == 3 ? '5' : '9')) {
                time[i] = '0';
            } else {
                time[i]++;
                break;
            }
        }
        lcd_xy(6,0);
        display(time);
    }
}

ISR(ADC_vect) {
    // merge ADCL:ADCH
    unsigned short adc_low = ADCL;
    unsigned short adc_high = ADCH;
    unsigned short adc = (adc_high << 8) | adc_low;

    if (adc > ADC_BTN_SELECT) {
        pushed = 0;
        reti();
    }
    if (pushed) {
        reti();
    }
    pushed = 1;
    if (adc < ADC_BTN_RIGHT) {
        // do nothing
    } else if (adc < ADC_BTN_UP) {
        for (int i = 0; i < 5; i++) {
            lap_left[i] = lap_right[i];
            lap_right[i] = time[i];
        }
        lcd_xy(0,1);
        display(lap_left);
        lcd_xy(9,1);
        display(lap_right);
    } else if (adc < ADC_BTN_DOWN) {
        lcd_xy(0,1);
        lcd_puts("                ");
        for (int i = 0; i < 5; i++) {
            lap_right[i] = '0';
        }
    } else if (adc < ADC_BTN_LEFT) {
        paused = 1;
        zero_all();
        lcd_xy(6,0);
        display(time);
    } else {
        // select button
        paused = 1 - paused;
    }
}
