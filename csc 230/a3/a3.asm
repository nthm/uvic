; these include files are untouched
.include "m2560def.inc"
.include "lcd_function_defs.inc"

.equ SPL_DATASPACE = 0x5D
.equ SPH_DATASPACE = 0x5E

.equ LAP_R = (TIME + 5)     ; offsets from TIME in dseg
.equ LAP_L = (LAP_R + 5)

; ADC button values           v1.1  v1.0
.equ ADC_BTN_RIGHT  = 0x032 ; 032   032
.equ ADC_BTN_UP     = 0x0C3 ; 0C3   0FA
.equ ADC_BTN_DOWN   = 0x17C ; 17C   1C2
.equ ADC_BTN_LEFT   = 0x22B ; 22B   28A
.equ ADC_BTN_SELECT = 0x316 ; 316   352

; -----------------------------------------------------------------------------
; register definitions - r16:r17 can be overwritten between ISRs

.def rTMP   = r16           ; temporary worker
.def rX     = r16           ; indicates a popped value is not used
.def rLOOP  = r17           ; used in for-loops
.def rTIME  = r18           ; holds a digit of time
.def rLIMIT = r19           ; interrupt counter threshold
.def rCOUNT = r20           ; interrupt counter
.def rSTATE = r21

.equ sPAUSED = 0            ; bits for rSTATE
.equ sPUSHED = 1

; XL:XH holds ADCL:ADCH on ADC conversion complete

; -----------------------------------------------------------------------------
; setup

.cseg
.org 0x0000
    rjmp ISR_RESET          ; reset
.org ADCCaddr
    rjmp ISR_BUTTON         ; analog conversion complete
.org OC2Aaddr
    rjmp ISR_COUNT_INT      ; timer 2 compare match

; last interrupt is 0x0070, 0x0072 is the first unreserved location

.org 0x0072
ISR_RESET:
    ldi rTMP, high(RAMEND)
    sts SPH_DATASPACE, rTMP
    ldi rTMP, low(RAMEND)
    sts SPL_DATASPACE, rTMP

    call INIT_ADC
    call INIT_TIMER

    call lcd_init           ; lowercase calls are library. cursor to (0,0)
    MSG: .db "Time: ", 0x00 ; overwrites rSTATE
    ldi rTMP, high(TEXT)
    push rTMP
    ldi rTMP, low(TEXT)
    push rTMP
    ldi rTMP, high(MSG << 1)
    push rTMP
    ldi rTMP, low(MSG << 1)
    push rTMP
    call str_init
    pop rX
    pop rX
    call lcd_puts           ; display "Time: " (from stack). cursor to (6,0)
    pop rX
    pop rX

    ldi YL, low(TIME)
    ldi YH, high(TIME)
    call ZERO_Y
    call PRINT_Y            ; update the time. YH:YL are set by ZERO_Y
    call SHIFT_TIMES        ; shift the zeroed time to LAP_R

    ldi rLIMIT, 50          ; interrupt counter threshold (alternates with 13)
    ldi rSTATE, (1 << sPAUSED)

    sei                     ; enable interrupts (already enabled by lcd_init?)
loop:
    rjmp loop

; -----------------------------------------------------------------------------
; analog to digital conversion

INIT_ADC:
    ; ADCSRA: ADC see page 285
    ; 1   ADEN     enable
    ; 1   ADSC     start conversion
    ; 0   ADATE    auto trigger enable: auto start following a clock (timer)
    ; 0   ADIF     interupt flag (set by ADIE)
    ; 1   ADIE     trigger "ADC Conversion Complete" interupt
    ; 111 ADPS2:0  prescaler from 2 to 128

    ldi rTMP, 0xCF          ; 0xCF is on, 0x4F is off
    sts ADCSRA, rTMP
    ldi rTMP, 0x00          ; disable ADCSRB: only used for ADATE in ADCSRA
    sts ADCSRB, rTMP
    ldi rTMP, 0x40          ; ADMUX: MUX4:MUX0 = 0, ADLAR = 0, REFS1:REFS0 = 1
    sts ADMUX, rTMP
    ret

; -----------------------------------------------------------------------------
; timer 2 initialization

; timer 2 hits a compare match at 125 which triggers ISR_COUNT_INT incrementing
; rCOUNT until it matches 50

INIT_TIMER:
    ldi rTMP, 0x00          ; sync
    sts ASSR, rTMP
    ldi rTMP, 0x02          ; clear on compare match (WGM21)
    sts TCCR2A, rTMP
    ldi rTMP, 0x06          ; clock 16 000 000 Hz (system clock) / 256
    sts TCCR2B, rTMP
    ldi rTMP, 0x7C          ; set output compare value to 125 (0...124)
    sts OCR2A, rTMP
    ldi rTMP, 0x02          ; interrupt on compare match A
    sts TIMSK2, rTMP
    ret

; -----------------------------------------------------------------------------
; zero out a time block given by YL:YH

ZERO_Y:
    ldi rTMP, '0'
    clr rLOOP
overwrite:
    st Y+, rTMP
    inc rLOOP
    cpi rLOOP, 5            ; 5 bytes
    brne overwrite
    ret

; -----------------------------------------------------------------------------
; count timer 2 compare interrupts. update display after counting to 50
; 1/10 / (1/(16 000 000/(256 * 125))) = 50 exactly

ISR_COUNT_INT:
    lds rTMP, ADCSRA        ; request a new ADC conversion
    ori rTMP, 0x40
    sts ADCSRA, rTMP

    sbrc rSTATE, sPAUSED    ; if paused just leave
    reti
    inc rCOUNT
    cp rCOUNT, rLIMIT
    breq update             ; if counter reaches 1/10 of a second
    reti
update:
    clr rCOUNT
    ldi YL, low(TIME)       ; load the address of tenth into YH:YL
    ldi YH, high(TIME)
    clr rLOOP               ; reuse for tracking digit in time below
carry_loop:
    cpi rLOOP, 5            ; only test up to 5 digits
    breq done
    ld rTIME, Y
    cpi rLOOP, 2            ; high digit of seconds (TIME + 2) overflows at 5
    breq five
    ldi rTMP, '9'           ; overflow at 9
    rjmp test
five:
    ldi rTMP, '5'           ; overflow at 5
test:
    cp rTIME, rTMP
    brne done               ; if rTIME is less than or equal to its max value
    ldi rTIME, '0'
    st Y+, rTIME            ; move to next digit
    inc rLOOP
    rjmp carry_loop
done:
    inc rTIME
    st Y, rTIME
    call PRINT_TIME
    reti

; -----------------------------------------------------------------------------
; print time at YH:YL to current cursor position
; this reads from the end of a time, minutes first: i.e high/low(TIME + 5)

PRINT_Y:
    ldi rTMP, 0x01          ; character of time. bit 0M 1M 2: 3S 4S 5. 6T
char_loop:
    sbrc rTMP, 2            ; between minute and second
    rjmp m2s
    sbrc rTMP, 5            ; between second and tenth
    rjmp s2t
    ld rTIME, -Y
    rjmp write
m2s:
    ldi rTIME, ':'
    rjmp write
s2t:
    ldi rTIME, '.'
write:
    push rTIME
    call lcd_putchar        ; moves cursor as well
    pop rTIME               ; if cannot be rX... no idea why

    sbrc rTMP, 6            ; if character 6 was printed, leave
    ret
    lsl rTMP                ; move to next digit
    rjmp char_loop

; -----------------------------------------------------------------------------
; print the primary time at cursor (6,0) by calling PRINT_Y

PRINT_TIME:
    ldi rTMP, 0
    push rTMP
    ldi rTMP, 6
    push rTMP
    call lcd_gotoxy
    pop rX
    pop rX
    ldi YL, low(TIME + 5)
    ldi YH, high(TIME + 5)
    call PRINT_Y
    ret

; -----------------------------------------------------------------------------
; print both lap times by calling PRINT_Y

PRINT_LAPS:
    ldi rTMP, 1
    push rTMP
    ldi rTMP, 0             ; start at 0th column
    push rTMP
    call lcd_gotoxy
    pop rX
    ldi YL, low(LAP_L + 5)
    ldi YH, high(LAP_L + 5)
    call PRINT_Y

    ldi rTMP, 9             ; start at 9th column
    push rTMP
    call lcd_gotoxy
    pop rX
    pop rX                  ; pop the row
    ldi YL, low(LAP_R + 5)
    ldi YH, high(LAP_R + 5)
    call PRINT_Y
    ret

; -----------------------------------------------------------------------------
; copy LAP_R to LAP_L, then TIME to LAP_R
; uses the T flag to loop twice

SHIFT_TIMES:
    ldi XL, low(LAP_R)
    ldi XH, high(LAP_R)
    ldi YL, low(LAP_L)
    ldi YH, high(LAP_L)
    clr rLOOP
copy:
    ld rTIME, X+
    st Y+, rTIME
    inc rLOOP
    cpi rLOOP, 5            ; copy 5 bytes
    brne copy
    brts copy_done
    sbiw XH:XL, 10
    sbiw YH:YL, 10
    clr rLOOP
    set                     ; both have been copied
    rjmp copy
copy_done:
    clt
    ret

; -----------------------------------------------------------------------------
; write spaces to the bottom lcd row

CLEAR_LAPS:
    ldi rTMP, 1             ; set cursor to (0,1)
    push rTMP
    ldi rTMP, 0
    push rTMP
    call lcd_gotoxy
    pop rX
    pop rX
    ldi rTMP, ' '
    push rTMP               ; for the repeated calls to lcd_putchar
    clr rLOOP
clear:
    call lcd_putchar
    inc rLOOP
    cpi rLOOP, 16           ; print from 0..15
    brne clear

    pop rX
    ldi YL, low(LAP_R)      ; zero LAP_R for SHIFT_TIMES in PRINT_LAPS
    ldi YH, high(LAP_R)
    call ZERO_Y
    ret

; -----------------------------------------------------------------------------
; handle button pushes

ISR_BUTTON:
    lds XL, ADCL            ; check if a button is being pushed
    lds XH, ADCH
    cpi XL, low(ADC_BTN_SELECT)
    ldi rTMP, high(ADC_BTN_SELECT)
    cpc XH, rTMP
    brlo pushed             ; if a button is pushed. else clear push state
    cbr rSTATE, (1 << sPUSHED)
    reti
pushed:
    sbrc rSTATE, sPUSHED    ; if a push state has already been set, reti
    reti
    sbr rSTATE, (1 << sPUSHED)
right:                      ; unused label
    cpi XL, low(ADC_BTN_RIGHT)
    ldi rTMP, high(ADC_BTN_RIGHT)
    cpc XH, rTMP
    brsh up
; ------                    ; do nothing
    reti
up:
    cpi XL, low(ADC_BTN_UP)
    ldi rTMP, high(ADC_BTN_UP)
    cpc XH, rTMP
    brsh down
; ------
    call SHIFT_TIMES
    call PRINT_LAPS         ; update
    reti
down:
    cpi XL, low(ADC_BTN_DOWN)
    ldi rTMP, high(ADC_BTN_DOWN)
    cpc XH, rTMP
    brsh left
; ------
    call CLEAR_LAPS
    reti
left:
    cpi XL, low(ADC_BTN_LEFT)
    ldi rTMP, high(ADC_BTN_LEFT)
    cpc XH, rTMP
    brsh select
; ------
    ldi rSTATE, (1 << sPAUSED)
    ldi YL, low(TIME)
    ldi YH, high(TIME)
    call ZERO_Y
    call PRINT_TIME         ; can't use PRINT_Y because cursor isn't at (6,0)
    reti
select:                     ; no comparison: was checked at the start
    ldi rTMP, (1 << sPAUSED)
    eor rSTATE, rTMP
    reti

; -----------------------------------------------------------------------------
; lcd driver

.include "lcd_function_code.asm"

; -----------------------------------------------------------------------------
; data

; times are saved into 5 bytes, in litte-endian format: [T, S, S, M, M]
; order here matters. because of SHIFT_TIMES

.dseg
    TEXT: .byte 10
    TIME: .byte 15          ; has TIME, LAP_R, LAP_L all together
