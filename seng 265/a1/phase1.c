/*
 * Phase 1 Assignment
 * SENG 265
 * June 2nd, 2017
 * Grant Hames-Morgan, V00857826
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h> /* istty() support */

/* using define instead of const int (see #1) */
#define MAX_BLOCK_SIZE 20
#define MAX_BUFFER MAX_BLOCK_SIZE + 2 /* +2 for \x03\0 */
/* #define DEBUG */

const char *PREFIX = "\xab\xba\xbe\xef\x14\x00\x00\x00";
const char BLOCK_END_SYMBOL = '\x03';

FILE *input;
FILE *output;

char buffer[MAX_BUFFER];
char grid[MAX_BUFFER - 1][MAX_BUFFER] = {'\0'};
/* this is not grid[MAX_BUFFER][MAX_BUFFER] since only rows end in \0 */

int block_size;
/* number of chars read by fread (max: MAX_BUFFER) */

#ifdef DEBUG
    void print_buffer(char *array) {
        int i;
        printf("[  ");
        /* this prints up to strlen() + 1 to include the \0 */
        for (i = 0; i < sizeof(char) * (strlen(array) + 1); i++) {
            switch (array[i]) {
                case '\x03': printf("\\3 "); break;
                case '\x0a': printf("\\n "); break;
                case '\x00': printf("\\0 "); break;
                default:     printf("%c  ", array[i]);
            }
        }
        printf("]\n");
    }

    void print_grid(char grid[MAX_BUFFER - 1][MAX_BUFFER]) {
        int row, i;
        printf("\t   ");
        for (i = 0; i < block_size; i++)
            printf("%d  ", i % 10);
        printf("\n");

        for (row = 0; row < block_size - 1; row++) {
            printf("%d\t", row);
            print_buffer(grid[row]);
        }
        printf("\n");
    }
#endif

void null_grid(char grid[MAX_BUFFER - 1][MAX_BUFFER]) {
    int row, col;
    for (row = 0; row < MAX_BUFFER - 1; row++) {
        for (col = 0; col < MAX_BUFFER; col++)
            grid[row][col] = '\0';
    }
}

void col_to_buffer(char grid[MAX_BUFFER - 1][MAX_BUFFER], int col) {
    int i;
    for (i = 0; i < block_size - 1; i++)
        buffer[i] = grid[i][col];

    /* terminate the string */
    buffer[i + 1] = '\0';
}

int compare_rows(const void *a, const void *b) {
    return strcmp((char *)a, (char *)b);
}

void sort(char grid[MAX_BUFFER - 1][MAX_BUFFER]) {
    qsort(grid, block_size - 1, sizeof(char) * MAX_BUFFER, compare_rows);
}

void forward_transform() {
    char head;
    int row, i;
    for (row = 0; row < block_size - 1; row++) {
        strncpy(grid[row], buffer, block_size);

        /* rotate buffer left by 1 */
        head = buffer[0];
        for (i = 0; i < block_size - 1; i++) /* -1 skips \0 rotation */
            buffer[i] = buffer[i + 1];
        buffer[i - 1] = head;
    }
#ifdef DEBUG
    printf("final grid:\n");
    print_grid(grid);
#endif
    sort(grid);
#ifdef DEBUG
    printf("after sorting:\n");
    print_grid(grid);
#endif
    /* copy the last column into buffer:
     * grid rows end with \0. block_size - 1 is the last valid index, so
     * block_size - 2 is the last not \0 column */
    col_to_buffer(grid, block_size - 2);
}

void backward_transform() {
    char sorting_grid[MAX_BUFFER - 1][MAX_BUFFER];
    null_grid(sorting_grid);

    int to_col, row, col, i;
    for (col = 0; col < block_size - 2; col++) {
        /* write buffer 'vertically' to latest column */
        for (i = 0; i < block_size - 1; i++)
            grid[i][col] = buffer[i];
#ifdef DEBUG
        printf("current grid:\n");
        print_grid(grid);
#endif
        /* to_col: used to copy written cols of grid into sorting_grid */
        for (to_col = 0; to_col <= col; to_col++) {
            for (row = 0; row < block_size - 1; row++)
                sorting_grid[row][to_col] = grid[row][to_col];
        }

        sort(sorting_grid);
        /* write the result to buffer and loop back up */
        col_to_buffer(sorting_grid, col);
    }
    /* instead of writing to grid from sorting_grid just use it outright
     * since removing the BLOCK_END_SYMBOL is already done */

    /* index of BLOCK_END_SYMBOL (see #3) */
    char *pos = strchr(buffer, BLOCK_END_SYMBOL);
    if (pos == NULL) {
        printf("backward transform failed: no BLOCK_END_SYMBOL\n");
        exit(EXIT_FAILURE);
    }
    int index = pos - buffer;
#ifdef DEBUG
    printf("found end symbol on index: %d. grid[index] is\n", index);
    print_buffer(grid[index]);
#endif
    strncpy(buffer, grid[index], block_size - 1);
}

void usage(char *name) {
    printf(
         "perform phase 1 data tranformation:"
    "\n" "usage: %s [options]"
    "\n"
    "\n" "options:"
    "\n" "  optional:"
    "\n" "    -i, --infile <file>    file to read from; defaults to stdin"
    "\n" "    -o, --outfile <file>   file to write to; defaults to stdout"
    "\n" "    -h, --help             display this help and exit"
    "\n" "  required: one of:"
    "\n" "    -f, --forward          transform data into the phase 1 format"
    "\n" "    -b, --backward         undo the phase 1 transformation"
    "\n"
    "\n" "when stdin is a TTY (not a pipe), it listens for ^D (ctrl-D) to"
    "\n" "signal end of input."
    "\n"
    "\n" "this program respects pipes, such as:"
    "\n" "./phase1 -f < tests/t01.txt | diff - tests/t01.ph1"
    "\n", name);
}

void test_file(FILE **file) {
    if (file == NULL) {
        printf("unable to open the given file\n");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char *argv[]) {
    struct option opts[] = {
        { "infile"  , required_argument, NULL, 'i' },
        { "outfile" , required_argument, NULL, 'o' },
        { "forward" , no_argument      , NULL, 'f' },
        { "backward", no_argument      , NULL, 'b' },
        { "help"    , no_argument      , NULL, 'h' },
        { NULL, 0, NULL, 0 }
    };

    /* default in/out streams */
    input = stdin;
    output = stdout;

    /* transformation direction for main (see #2) */
    enum tdir { NONE, FORWARD, BACKWARD };
    enum tdir direction = NONE;

    char used[6] = {'\0'}; /* number of elements in opts[] */
    int count = 0;
    int arg, merged;
    while ((arg = getopt_long(argc, argv, "i:o:fbh", opts, NULL)) != -1) {
        /* mark either direction as 'd' */
        merged = (arg == 'f' || arg == 'b') ? 'd' : arg;
        if (strchr(used, merged) != NULL) {
            printf("options can only be specified once\n");
            exit(EXIT_FAILURE);
        }
        used[count++] = merged;
        switch (arg) {
            case 'i':
                input = fopen(optarg, "r");
                test_file(&input);
                break;
            case 'o':
                output = fopen(optarg, "w");
                test_file(&output);
                break;
            case 'f':
                direction = FORWARD;
                break;
            case 'b':
                direction = BACKWARD;
                break;
            case 'h':
                usage(argv[0]);
                exit(EXIT_SUCCESS);
            default:
                printf("use --help to see usage and options\n");
                exit(EXIT_FAILURE);
        }
    }

    /* read or write PREFIX */
    char file_prefix[sizeof(PREFIX)];
    int MAX_READ = MAX_BLOCK_SIZE;
    switch (direction) {
        case FORWARD:
            fwrite(PREFIX, sizeof(PREFIX), sizeof(char), output);
            break;
        case BACKWARD:
            fread(file_prefix, sizeof(PREFIX), sizeof(char), input);
            if (strcmp(file_prefix, PREFIX) != 0) {
                printf("input file has incorrect header\n");
                exit(EXIT_FAILURE);
            }
            MAX_READ++;
            break;
        default:
            printf("transform direction required: --forward or --backward\n");
            exit(EXIT_FAILURE);
    }

    /* no c99 support, can't use fileno() (see #4) */
    if (input == stdin && isatty(STDIN_FILENO))
        printf("\nreading stdin, use ^D (ctrl-D) as end of input:\n");

    do {
        block_size = fread(buffer, 1, MAX_READ, input);
        if (block_size == 0) break;
#ifdef DEBUG
        printf("new block: %d bytes. adding end character(s)\n", block_size);
#endif
        if (direction == FORWARD) buffer[block_size++] = BLOCK_END_SYMBOL;
        buffer[block_size++] = '\0';
#ifdef DEBUG
        printf("block size is now %d\n", block_size);
        print_buffer(buffer);
#endif
        if (direction == FORWARD)  forward_transform();
        if (direction == BACKWARD) backward_transform();
#ifdef DEBUG
        printf("result:\n");
        print_buffer(buffer);
#endif
        fwrite(buffer, sizeof(char), strlen(buffer), output);
        null_grid(grid);
    } while (feof(input) == 0);
#ifdef DEBUG
    printf("EOF. bye!\n");
#endif
    fflush(output);

    if (input != stdin)   fclose(input);
    if (output != stdout) fclose(output);
    exit(EXIT_SUCCESS);
}

/* Links:
 * #1 stackoverflow.com/q/1712592/variably-modified-array-at-file-scope
 * #2 stackoverflow.com/q/12593061/define-an-unknown-or-null-value-in-an-enum
 * #3 stackoverflow.com/q/4824/string-indexof-function-in-c
 * #4 stackoverflow.com/q/15749184/fileno-not-available
 */
