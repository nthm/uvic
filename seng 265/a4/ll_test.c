#include <stdlib.h>
#include <stdio.h>
#include "linked_list.h"

int main (int argc, char* argv[]) {
    List* list = ll();

    printf("appending three characters and a number\n");
    ll_append(list, ll_node('a'));
    ll_append(list, ll_node('b'));
    ll_append(list, ll_node('c'));
    ll_append(list, ll_node(1+128));
    ll_print(list);

    printf("setting the cursor to head and nexting it\n");
    list->cursor = list->head;
    ll_next(list);

    printf("prepending 'z'\n");
    ll_prepend(list, ll_node('z'));
    ll_print(list);

    printf("unlinking and freeing the item at the cursor\n");
    free(ll_unlink(list));
    ll_print(list);

    printf("searching for 'A'. won't exist. cursor runs off the list\n");
    for (list->cursor = list->head;
         list->cursor && (list->cursor->hex != 3+128);
         ll_next(list)) {}
    printf("trying to unlink that ''search result''\n");
    Node* empty_unlink = ll_unlink(list);
    if (empty_unlink != NULL) free(empty_unlink);
    ll_print(list);

    printf("searching for 'a' and unlinking it\n");
    for (list->cursor = list->head;
         list->cursor && (list->cursor->hex != 'a');
         ll_next(list)) {}
    Node* unlinked = ll_unlink(list);
    ll_print(list);

    printf("setting the cursor to head\n");
    list->cursor = list->head;

    printf("prepending the unlinked 'a' node to cursor\n");
    ll_cursor_prepend(list, unlinked);
    ll_print(list);

    printf("appending '25' to cursor\n");
    ll_cursor_append(list, ll_node(25+128));
    ll_print(list);

    printf("setting cursor to null. trying to append and prepend to it\n");
    list->cursor = NULL;
    ll_cursor_append(list, ll_node(90+128));
    ll_cursor_prepend(list, ll_node(90+128));
    ll_print(list);

    printf("emptying the list\n");
    ll_empty(list);
    ll_print(list);

    printf("freeing the list, and nulling its local variable\n");
    free(list);
    list = NULL;
    ll_print(list);

    exit(EXIT_SUCCESS);
}
