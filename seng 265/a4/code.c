#include "linked_list.h"

void encode(List* data) {
    /* stack cursor's index and one node count for runlength encoding */
    unsigned int index, ones = 0;
    List* stack = ll();
    Node* one;

    /* reset cursors */
    data->cursor = data->head;
    data->prev = NULL;
    for ( ; data->cursor; ll_next(data)) {
        /* search through the stack */
        for (stack->cursor = stack->head, index = 1;
             stack->cursor && (stack->cursor->hex != data->cursor->hex);
             ll_next(stack), index++) {}

        /*  char not in stack */
        if (stack->cursor == NULL) {
            ll_append(stack, ll_node(data->cursor->hex));
            ll_cursor_prepend(data, ll_node(stack->length + 128));
        }
        /* char is in stack */
        else {
            /* replace char with stack index */
            data->cursor->hex = 128 + index;

            /* runlength encoding */
            if (index == 1) {
                if (ones++ == 0)
                    one = data->cursor;
                if (data->cursor->next != NULL)
                    continue;
            } else {
                Node* unlinked = ll_unlink(stack);
                ll_prepend(stack, unlinked);
            }
        }
        if (ones < 3) {
            ones = 0;
            continue;
        }
        /* overwrite 2 one nodes */
        one->hex = 128 + 0;
        one = one->next;
        one->hex = 128 + ones;
        /* delete the remaining ones */
        Node* saved_cursor = data->cursor;
        for (ones -= 2; ones > 0; ones--) {
            data->cursor = one;
            ll_next(data);
            free(ll_unlink(data));
        }
        data->cursor = saved_cursor;
    }
    ll_empty(stack);
    free(stack);
}

void decode(List* data) {
    /* count ones for runlength decoding */
    unsigned int ones, i;
    unsigned char hex;
    List* stack = ll();
    Node* after;

    /* reset cursors */
    data->cursor = data->head;
    data->prev = NULL;
    for ( ; data->cursor; ll_next(data)) {
        hex = data->cursor->hex;
        /* if hex is 0 then expand the ones */
        if (hex == 128 + 0) {
            ones = data->cursor->next->hex - 128;
            hex++;
            /* this doesn't move the data->cursor. hex is now 128 + 1 */
            data->cursor->hex = hex;
            data->cursor->next->hex = hex;
            for (ones -= 2; ones > 0; ones--)
                ll_cursor_append(data, ll_node(hex));
        }
        /* if hex is a number not yet seen */
        if (hex > 128 && hex - 128 > stack->length) {
            after = data->cursor->next;
            free(ll_unlink(data));
            data->cursor = after;
            hex = data->cursor->hex;
        }
        /* if hex is a character */
        if (hex < 128) {
            ll_append(stack, ll_node(data->cursor->hex));
            continue;
        }
        /* if hex is a number we've seen before */
        stack->cursor = stack->head;
        /* recall stack starts at index 1 not 0 */
        for (i = 1; i < hex - 128; ll_next(stack), i++) {}
        data->cursor->hex = stack->cursor->hex;
        ll_prepend(stack, ll_unlink(stack));
    }
    ll_empty(stack);
    free(stack);
}
