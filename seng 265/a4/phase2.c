/*
 * Phase 2
 * SENG 265
 * July 19, 2017
 * Grant Hames-Morgan, V00857826
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <getopt.h>
#include <unistd.h> /* isatty() support */

#define MAX_READ 1000 /* read limit for the buffer */

#include "linked_list.h"
#include "code.c"

/* phase 1 and phase 2 prefixes, in order. not const - see #1 */
char* MAGICS[] = {"\xab\xba\xbe\xef", "\xda\xaa\xaa\xad"};

void usage(char* name) {
    printf(
         "perform phase 2 encode and decode:"
    "\n" "usage: %s [options]"
    "\n"
    "\n" "options:"
    "\n" "  optional:"
    "\n" "    -i, --infile <file>    file to read from; defaults to stdin"
    "\n" "    -o, --outfile <file>   file to write to; defaults to stdout"
    "\n" "    -h, --help             display this help and exit"
    "\n" "  required: one of:"
    "\n" "    -e, --encode           perform encoding; input must be a .ph1"
    "\n" "    -d, --decode           undo encoding; input must be a .ph2"
    "\n"
    "\n" "when stdin is a TTY (not a pipe), it listens for ^D (ctrl-D) to"
    "\n" "signal end of input."
    "\n"
    "\n" "this program respects pipes, such as:"
    "\n" "./phase2 -e < tests/t01.ph1 | diff - tests/t01.ph2"
    "\n", name);
}

void quit(char* reason) {
     printf("%s", reason);
     exit(EXIT_FAILURE);
}

void test_file(FILE** file) {
    if (file == NULL)
        quit("unable to open the given file\n");
}

int main(int argc, char* argv[]) {
    struct option opts[] = {
        { "infile" , required_argument, NULL, 'i' },
        { "outfile", required_argument, NULL, 'o' },
        { "encode" , no_argument      , NULL, 'e' },
        { "decode" , no_argument      , NULL, 'd' },
        { "help"   , no_argument      , NULL, 'h' },
        { NULL, 0, NULL, 0 }
    };

    /* default in/out streams */
    /* TODO this would segfault phase1.c unless they were globals */
    FILE* input = stdin;
    FILE* output = stdout;

    /* coding (see #2) */
    enum code { NONE, ENCODE, DECODE };
    enum code coding = NONE;

    char used[sizeof(opts)] = {'\0'};
    int count = 0;
    int arg, merged;
    while ((arg = getopt_long(argc, argv, "i:o:edh", opts, NULL)) != -1) {
        /* mark either coding as 'c' */
        merged = (arg == 'e' || arg == 'd') ? 'c' : arg;
        if (strchr(used, merged) != NULL)
            quit("options can only be specified once\n");
        used[count++] = merged;
        switch (arg) {
            case 'i':
                input = fopen(optarg, "r");
                test_file(&input);
                break;
            case 'o':
                output = fopen(optarg, "w");
                test_file(&output);
                break;
            case 'e':
                coding = ENCODE;
                break;
            case 'd':
                coding = DECODE;
                break;
            case 'h':
                usage(argv[0]);
                exit(EXIT_SUCCESS);
            default:
                printf("use --help to see usage and options\n");
                exit(EXIT_FAILURE);
        }
    }

    if (coding == NONE)
        quit("coding required: --encode or --decode\n");

    /* no c99 support, can't use fileno() (see #3) */
    if (input == stdin && isatty(STDIN_FILENO))
        printf("\nreading stdin, use ^D (ctrl-D) as end of input:\n");

    /* check magic number */
    int index = (coding == ENCODE) ? 0 : 1; /* swap on decode */
    char* to_read = MAGICS[index];
    char* to_write = MAGICS[!index];

    char read_magic[4];
    fread(read_magic, sizeof(char), 4, input);
    if (strncmp(read_magic, to_read, 4) != 0)
        quit("input file has incorrect header. see --help\n");

    fwrite(to_write, sizeof(char), 4, output);

    /* copy blocksize from input to output */
    char blocksize[4];
    fread(blocksize, sizeof(char), 4, input);
    fwrite(blocksize, sizeof(char), 4, output);

    /* load the file into memory - dynamically. have an array that stores up
     * to MAX_READ characters and then moves them to a linked list */
    int allocated = MAX_READ * sizeof(char);
    char* buffer = malloc(allocated);
    if (buffer == NULL)
        quit("could not allocate buffer array\n");

    List* data = ll();
    int read, i;
    while ((read = fread(buffer, sizeof(char), MAX_READ, input)) != 0) {
        for (i = 0; i < read; i++)
            ll_append(data, ll_node(buffer[i]));
    }
#ifdef DEBUG
    ll_print(data);
#endif
    /* functions included from code.c */
    coding == ENCODE ? encode(data) : decode(data);

    i = 0;
    for (data->cursor = data->head; data->cursor; ll_next(data), i++) {
        if (i == MAX_READ) {
#ifdef DEBUG
            printf("buffer hit limit. writing\n");
#endif
            fwrite(buffer, sizeof(char), MAX_READ, output);
            i = 0;
        }
        buffer[i] = data->cursor->hex;
    }
#ifdef DEBUG
    printf("writing remaining %d chars\n", i);
#endif
    fwrite(buffer, sizeof(char), i, output);

    fflush(output);
    free(buffer);

    ll_empty(data);
    free(data);

    if (input  != stdin ) fclose(input );
    if (output != stdout) fclose(output);
    exit(EXIT_SUCCESS);
}

/* Links:
 * #1 stackoverflow.com/q/2316387/initialization-discards-qualifiers-from-pointer-target-type
 * #2 stackoverflow.com/q/12593061/define-an-unknown-or-null-value-in-an-enum
 * #3 stackoverflow.com/q/15749184/fileno-not-available
 */
