#!/usr/bin/fish
for i in (seq -w 1 20)
    ./phase2 --encode --infile tests/t$i.ph1 --outfile out/t$i.ph2
    #./phase2 --decode --infile tests/t$i.ph2 --outfile out/t$i.ph1
    echo out:
    hexdump -CL out/t$i.ph2 | head -n 1
    echo expected:
    hexdump -CL tests/t$i.ph2 | head -n 1
    diff {out,tests}/t$i.ph2
    echo -n test $i': ' 
    if test $status -gt 0
        echo failed
        exit 1
    else
        echo passed
    end
    sleep 0.25
end
