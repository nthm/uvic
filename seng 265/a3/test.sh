#!/usr/bin/fish
for i in (seq -w 1 20)
    # python phase2.py --encode --infile tests/t$i.ph1 --outfile out/t$i.ph2
    python phase2.py --decode --infile tests/t$i.ph2 --outfile out/t$i.ph1
    echo out:
    hexdump -CL out/t$i.ph1 | head -n 1
    echo expected:
    hexdump -CL tests/t$i.ph1 | head -n 1
    diff {out,tests}/t$i.ph1
    echo -n test $i': ' 
    if test $status -gt 0
        echo failed
        exit 1
    else
        echo passed
    end
    sleep 1
end
