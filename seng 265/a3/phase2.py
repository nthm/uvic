#!/usr/bin/python3

"""
Phase 2
SENG 265
July 4, 2017
Grant Hames-Morgan, V00857826
"""

import sys, getopt

PH1 = '\xab\xba\xbe\xef'
PH2 = '\xda\xaa\xaa\xad'
ENC = 'latin-1'

def usage():
    print("""
perform phase 2 encode and decode:
usage {} [options]

options:
  optional:
    --infile <file>    file to read from
    --outfile <file>   file to write to
    --help
  required:
    --encode           perform encoding; --infile must be a .ph1
    --decode           undo the phase 2 encoding; --infile must be a .ph2
    """.format(sys.argv[0]))

def main():
    opt = ['infile=', 'outfile=', 'encode', 'decode', 'help']
    try:
        parsed, _ = getopt.getopt(sys.argv[1:], '', opt)
    except getopt.GetoptError as err:
        print(err + 'use --help for usage') # option -x not recognized
        sys.exit(2)
    io = {}
    method = None
    used = []
    for key, value in parsed:
        if key in used:
            sys.exit('options can only be specified once')
        elif key in ('--infile', '--outfile'):
            io[key[2:-4]] = value
        elif key in ('--encode', '--decode'):
            method = key[2:]
        elif key == '--help':
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"
        used.append(key if method is not None else '--method')

    # error handling
    if method is None:
        sys.exit('method required: use --encode or --decode')

    for key, access in [('in', 'r'), ('out', 'w')]:
        if key not in io:
            io[key] = input('no {0}put: enter {0}put file: '.format(key))
        try:
            open(io[key], access).close()
        except IOError:
            sys.exit('cannot open {0}put file'.format(key))

    # magic numbers
    toread, towrite = (PH1, PH2) if method == 'encode' else (PH2, PH1)

    infile = open(io['in'], 'r', encoding = ENC)
    if infile.read(4) != toread:
        sys.exit('input file has incorrect header. see --help')

    blocksize = infile.read(4)
    data = infile.read()
    infile.close()

    outfile = open(io['out'], 'w', encoding = ENC)
    outfile.write(towrite + blocksize)

    if method == 'encode':
        data = into_ascii(run_length_encode(mtf_encode(data)))
    else:
        data = mtf_decode(run_length_decode(from_ascii(data)))

    outfile.write(''.join(data))
    outfile.close()

def mtf_encode(data):
    # the +1's are because stack indexing starts at 1 not 0
    result, stack = [], []
    for char in data:
        if char not in stack: # look through stack because it's smaller
            result.extend([(len(stack) + 1), char])
            stack.append(char)
        else:
            index = stack.index(char)
            result.append(index + 1)
            stack.pop(index)
            stack.insert(0, char) # prepend
    return result

def run_length_encode(data):
    # keeps last known 1's index: data starts with 1
    index = 0
    while True:
        try:
            # look for 1's 1 index after the last known 1
            index += data[index + 1:].index(1) + 1
        except ValueError:
            return data # no more 1's
        count = 0
        try:
            while data[index + count] is 1: count += 1
        except IndexError:
            pass
        if count < 3:
            continue
        del data[index:index + count]
        data[index:index] = [0, count]

def into_ascii(data):
    return [chr(128 + x) if isinstance(x, int) else x for x in data]
    # if x is > 127 for chr() we got problems

def mtf_decode(data):
    original, stack = [], []
    for item in data:
        if isinstance(item, str):
            stack.append(item)
            original.append(item)
        # if the number is beyond the stack, skip it
        elif item <= len(stack):
            original.append(stack[item - 1])
            stack.insert(0, stack.pop(item - 1)) # prepend
    return original

def run_length_decode(data):
    # while 0 in data: index = data.index(0); data[ind...]...
    index = None
    while True:
        try:
            index = data.index(0)
        except ValueError:
            return data
        data[index:index + 2] = [1 for x in range(data[index + 1])]

def from_ascii(data):
    return [ord(x) - 128 if ord(x) >= 128 else x for x in data]

if __name__ == '__main__': main()

"""
Links:
1. stackoverflow.com/q/7376019/list-extend-to-index-inserting-list-elements-not-only-to-the-end
"""
