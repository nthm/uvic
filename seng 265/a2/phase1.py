#!/usr/bin/python3

"""
Phase 1 Assignment
SENG 265
June 14, 2017
Grant Hames-Morgan, V00857826
"""

import sys, getopt
from struct import pack, unpack

PREFIX = '\xab\xba\xbe\xef'
BLOCK_END = '\x03'

def usage():
    print("""
perform phase 1 transform:
usage {} [options]

options:
  optional:
    --infile <file>    file to read from
    --outfile <file>   file to write to
    --help
  required:
    --forward          transform data into phase 1 format
    --blocksize <int>  characters read at once. required for --forward
    --backward         undo the phase 1 transform
    """.format(sys.argv[0]))

def main():
    opt = ['infile=', 'outfile=', 'forward', 'backward', 'blocksize=', 'help']
    try:
        parsed, _ = getopt.getopt(sys.argv[1:], '', opt)
    except getopt.GetoptError as err:
        print(err + 'use --help for usage') # option -x not recognized
        sys.exit(2)
    io = {}
    direction = None
    blocksize = 0
    used = []
    for key, value in parsed:
        if key in used:
            sys.exit('options can only be specified once')
        elif key in ('--infile', '--outfile'):
            io[key[2:-4]] = value
        elif key in ('--forward', '--backward'):
            direction = key[2:]
        elif key == '--blocksize':
            try:
                blocksize = int(value)
            except ValueError:
                sys.exit('blocksize is not a valid integer')
        elif key == '--help':
            usage()
            sys.exit()
        else:
            assert False, "unhandled option"
        used.append(key if direction is not None else '--direction')

    # error handling
    if direction is None:
        sys.exit('transform direction required: use --forward or --backward')
    if direction == 'forward' and blocksize <= 1:
        sys.exit('blocksize must be set and be >1 for forward transforms')
    if direction == 'backward' and blocksize != 0:
        sys.exit('blocksize can only be used with forward transforms')

    for key in filter(lambda x: x not in io, ['in', 'out']):
        io[key] = input('no {0}put given: enter an {0}put file: '.format(key))
    enc = 'latin-1'
    with open(io['in'], 'r', encoding = enc) as infile, \
            open(io['out'], 'w', encoding = enc) as outfile:

        def transform(function, blocksize):
            for block in iter(lambda: infile.read(blocksize), ''):
                outfile.write(function(block))

        if direction == 'forward':
            outfile.write(PREFIX + ''.join(map(chr, pack("I", blocksize))))
            transform(forward_transform, blocksize)

        if direction == 'backward':
            if infile.read(len(PREFIX)) != PREFIX:
                sys.exit('input file has incorrect header')
            raw_blocksize = bytearray(map(ord, list(infile.read(4))))
            blocksize = unpack("I", raw_blocksize)[0] + 1 # BLOCK_END
            transform(backward_transform, blocksize)

def forward_transform(block):
    block += BLOCK_END
    grid = [block[x:] + block[:x] for x in range(0, len(block))]
    return ''.join(line[-1] for line in sorted(grid)) 

def backward_transform(block):
    grid = list(block)
    while len(grid[0]) < len(grid):
        grid = [row + sort[-1] for row, sort in zip(grid, sorted(grid))]
    return ''.join(grid[[line[-1] for line in grid].index(BLOCK_END)][:-1])

if __name__ == '__main__': main()

"""
Links
1. stackoverflow.com/q/10140281/how-to-find-out-whether-a-file-is-at-its-eof
2. stackoverflow.com/q/1919044/is-there-a-better-way-to-iterate-over-two-lists-getting-one-element-from-each-l
"""
