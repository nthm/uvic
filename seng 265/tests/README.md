# Test files SENG 265 Summer 2017 (UVic)

The files in this directory consist of pairs. For example:
* t01.txt and t01.ph1 are a pair
* t02.txt and t02.ph1 are a pair
* etc.

Blocks sizes are as follows:
* t01 through to t11: 20
* t12: 50
* t13: 100
* t14: 200
* t15: 500
* t16: 20
* t17: 50
* t18: 100
* t19: 200
* t20: 500
