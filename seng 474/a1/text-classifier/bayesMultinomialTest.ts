import { test, train, categories } from './bayesMultinomial';
import * as fs from 'fs';

const readData = (filename: string) =>
  fs.readFileSync(`./data/${filename}`, 'utf-8').split(/\r?\n/);

const trainData = readData('traindata.txt');
const trainLabels = readData('trainlabels.txt');

if (trainData.length !== trainLabels.length) {
  throw new Error(`Wrong lengths ${trainData.length} and ${trainLabels.length}`);
}

// Train it
for (let i = 0; i < trainData.length; i++) {
  train(trainLabels[i], trainData[i]);
}

function runTest(data: string[], labels: string[]) {
  const labelTags = ['FUTURE', 'WISE'];
  let correct = 0;
  for (let i = 0; i < data.length; i++) {
    const sentence = data[i];
    const labelIndex = parseInt(labels[i]);
    if (test(sentence) === labels[i]) {
      correct++;
      console.log(`✔️ ${labelTags[labelIndex]} ${sentence}`);
    } else {
      console.log(`❌ Expected ${labelTags[labelIndex]} but was ${labelTags[(labelIndex + 1) % 2]} - ${sentence}`);
    }
  }
  const percent = ((correct / data.length) * 100).toFixed(3);
  console.log(`${correct}/${data.length} = ${percent}%\n`);
}

// Try it on itself all over again...
console.log('TRAINING SET:');
runTest(trainData, trainLabels);

// Run the real data set
const testData = readData('testdata.txt');
const testLabels = readData('testlabels.txt');

if (testData.length !== testLabels.length) {
  throw new Error(`Wrong lengths ${testData.length} and ${testLabels.length}`);
}

console.log('TEST SET:');
runTest(testData, testLabels);
