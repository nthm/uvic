// There is likely a better way to do this in a spreadsheet but yknow...
const fs = require('fs')

// If this dataset was any bigger I'd want to stream it instead of holding it in
// memory but yknow this is a one off calculation...
const raw = fs.readFileSync(__dirname + '/titanic.tsv', 'utf-8')
const split = raw.split(/\r?\n/)
const headers = split.shift().split('\t')
const data = split.map(row =>
  row.split('\t').reduce((obj, value, index) => {
    obj[headers[index]] = value
    return obj
  }, {}))

// Into object[]
// [{ pclass: '2nd', age: 'adult', ... }, ... ]

// Switch between these:
const [pclassWant, ageWant, sexWant] = ['2nd', 'child', 'male']
// const [pclassWant, ageWant, sexWant] = ['2nd', 'adult', 'female']
const result = {}
for (const fit of ['yes', 'no']) {
  let surviveFit = 0

  let pclassFit = 0
  let ageFit = 0
  let sexFit = 0

  for (const x of data) {
    if (x.pclass === pclassWant) {
      if (x.survived === fit) {
        pclassFit++
      }
    }
    if (x.age === ageWant) {
      if (x.survived === fit) {
        ageFit++
      }
    }
    if (x.sex === sexWant) {
      if (x.survived === fit) {
        sexFit++
      }
    }
    if (x.survived === fit) {
      surviveFit++
    }
  }
  result[fit] =
    (pclassFit / surviveFit) *
    (ageFit / surviveFit) *
    (sexFit / surviveFit) *
    (surviveFit / data.length)

  console.log(`P(survive = ${fit} | E) =
\tP(pclass = ${pclassWant} | survive = ${fit}) *
\tP(age = ${ageWant} | survive = ${fit}) *
\tP(sex = ${sexWant} | survive = ${fit}) *
\tP(survive = ${fit}) / P(E) =
= (${pclassFit}/${surviveFit}) *
\t(${ageFit}/${surviveFit}) *
\t(${sexFit}/${surviveFit}) *
\t(${surviveFit}/${data.length}) / P(E) = ${result[fit].toFixed(4)} / P(E)
`)
}

const { yes, no } = result
const yesF = yes.toFixed(4)
const noF = no.toFixed(4)

console.log(`P(survive = yes | E) + P(survive = no | E) = 1
${yesF} / P(E) + ${noF} / P(E) = 1
${yesF} + ${noF} = P(E)

P(survive = yes | E) = ${yesF} / (${yesF} + ${noF}) = ${((yes / (yes + no)) * 100).toFixed(2)}%
P(survive = no | E) = ${noF} / (${yesF} + ${noF}) = ${((no / (yes + no)) * 100).toFixed(2)}%
`)
