// This is homework from a data mining course. The dataset is Titanic2.txt and
// comes with Weka when you install it.

const log = Math.log2
const entropy = (...p) => p.reduce((ans, px) => ans - px * log(px), 0)
const info = ([yes, no]) => entropy(yes / (yes + no), no / (yes + no))

function work(tsv) {
  const results = []
  const [headers, ...parsed] = tsv.trim().split('\n').map(row => row.split('\t'))
  const [type] = headers
  console.log(`${type}:`)
  for (let [name, no, yes] of parsed) {
    yes = Number(yes)
    no = Number(no)
    const ans = info([yes, no])
    results.push([yes, no, ans])
    const all = yes + no
    console.log(`
      ${type} = ${name}:
          info([${yes},${no}]) = entropy(${yes}/${all},${no}/${all}) =
          - ${yes}/${all}*log(${yes}/${all}) - ${no}/${all}*log(${no}/${all}) = ${ans.toFixed(3)}
    `)
  }
  const total = results.reduce((sum, [yes, no, ans]) => sum + yes + no, 0)
  const totalAns = results.reduce((sum, [yes, no, ans]) => sum + (ans * ((yes + no) / total)), 0)
  console.log(`
    info([${results.map(([yes, no, ans]) => `${yes}/${yes + no}`)}]) =
    ${results.map(([yes, no, ans]) => `${ans.toFixed(3)}*${yes + no}/${total}`).join(' + ')} = ${totalAns.toFixed(3)}
  `)
}

const splitTSVs = collection => collection.trim().split('\n\n')

// Paste the TSV from a pivot table in Google Sheets or Excel here:
const rootSplitSet = `
pclass	no	yes
1st	122	203
2nd	167	118
3rd	528	178
crew	673	212

age	no	yes
adult	1438	654
child	52	57

sex	no	yes
female	126	344
male	1364	367
`

// Then sex was chosen as the root, so female then male:
const sexSplitSet = `
pclass (for sex = female)	no	yes
1st	4	141
2nd	13	93
3rd	106	90
crew	3	20

age (for sex = female)	no	yes
adult	109	316
child	17	28

pclass (for sex = male)	no	yes
1st	118	62
2nd	154	25
3rd	422	88
crew	670	192

age (for sex = male)	no	yes
adult	1329	338
child	35	29
`

splitTSVs(sexSplitSet).forEach(work)
