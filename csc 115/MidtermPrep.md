## Midterm prep

1. Write a `.equals()` method:
```java
public boolean equals(bicycle other) {
	return this.colour.equals(other.getColour())
	    && this.hasBell == other.getHasBell()
	    && Double.compare(this.frameSize, other.getFrameSize()) == 0;
}
```

2. Write a recursive method to determine if a string is a palindrome
```java
public boolean isPalindrome(String word) {
	int len = word.length();
	if (len <= 1)
		return true;
	if (word.charAt(0) == word.charAt(len - 1))
		return isPalindrome(word.substring(1, len));
	return false;
}
```
Forgot `static`


3.

| Types     | Order
| - - - - - - - - - - - - - - - - - - - - - - -
| Inorder   | 1, 3, 4, 6, 7, 8, 10, 13, 14
| Preorder  | 8, 3, 1, 6, 4, 7, 10, 14, 13
| Postorder | 1, 4, 7, 6, 3, 8?, 13, 14, 10, 8?
	
-1 Didn't know where to put the root in post order. Root comes last.

4.

Rel between Goose and Bird: Goose extends Bird. Goose is a child of Bird, and 
Bird is a parent of Goose. Bird has a Goose.

Rel between Bird and Penguin: Same as Goose and Bird. Goose and Penguin are 
siblings.

Rel between Bird and BirdController: Bird extends BirdController

5.
```java
public static boolean isMatching(String input) {
	Stack<Character> stack = new Stack<>();
	
	for (char letter : input.toCharArray()) {
		if (letter == '{') {
			stack.push(letter);
		} else if (letter == '}') {
			if (!stack.empty() && stack.pop() == '{')
				return true;
			return false;
		} else {
			return false;
		}
	}
	
	if (stack.empty()) return true;
	return false;
}
```