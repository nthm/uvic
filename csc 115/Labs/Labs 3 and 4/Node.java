class Node {
	int  data;
	Node next;
	
	Node() {
		this(0);
	}
	
	Node(int n) {
		data = n;
		next = null;
	}
}