/*
	This lab file explains pre-conditions and why they're important - you use 
	them when you write bad code and need to cover your ass. "Or else you'll 
	get sued" is the instructor's reasoning.
*/

public class Vector {

	private int[] vectorArray;
	private int size;
	public static final int INIT_SIZE = 3;

	// Pre-condition: int[] incoming must be full (apparently)
	public Vector(int[] incoming) {
		vectorArray = incoming;
		size = incoming.length;
	}
	
	public Vector() {
		vectorArray = new int[INIT_SIZE];
	}
	
	// Pre-condition: vectorArray must not be full (or else there's overflow)
	public void add(int newInt) {
		vectorArray[size++] = newInt;
	}

	// Pre-condition: 0 <= index < size
	public void update(int index, int newNum) {
		vectorArray[index] = newNum;
	}

	public Vector copy() {
	/*
		This references vectorArray so if any connected Vector objects modify 
		it then all of them will 'change'.
	*/	
		int[] b = vectorArray;
		return new Vector(b);
	}
	
	public Vector deepCopy() {
		int size = vectorArray.length;
		int[] copy = new int[size];
		
		System.arraycopy(vectorArray, 0, copy, 0, size);
		return new Vector(copy);
	}

	public String toString() {
		StringBuffer s = new StringBuffer(size * 5 + 2);
		s.append("{");
		for (int i = 0; i < size - 1; i++) {
			s.append(vectorArray[i] + ",");
		}
		if (size > 0) {
			s.append(vectorArray[size - 1] + "}");
		}
		return s.toString();
	}
	
	public static void main(String[] args) {
		Vector v1 = new Vector();
		v1.add(4);
		v1.add(16);
		v1.add(12);
		System.out.println("v1 = " + v1);
		Vector v2 = v1.copy();
	/*
		We went into a lot of detail here talking about how variables/objects 
		are initialized. When `Vector v2` is written memory is allocated for an 
		empty array - before any data is put into the new space.
	*/	
		System.out.println("v2 = " + v2);
		v2.update(0,25);
		System.out.println("After changing v2:");
		System.out.println("v1 = " + v1);
		System.out.println("v2 = " + v2);
	}
}
