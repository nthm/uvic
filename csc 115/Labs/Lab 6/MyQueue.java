/*
	Notes:
	Difference between
		- Interface
			Ex: List
			List l = new List() is not possible as its only an interface. We 
			create an implementation of the interface.
			
			A.java: A implements List<E> {}
			List l = new A()
		- Abstract Class
			Ex: AbstractSequentialList
			Can't create an object with this either, it's abstract meaning it 
			doesn't implement all methods defined in the interface.
			
			Example: AbstractShape might implement some methods but not 
			something like Area() because we don't know the shape. That's up 
			to the children that implement it.
		- Class
			Ex: Skier (from first assignment)
			Can make an object with.
	
	This lab: Implementing a queue
*/

import java.util.LinkedList;

public class MyQueue {
	private LinkedList<String> list;
	
	public MyQueue() {
		list = new LinkedList<String>();
	}
	
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	public void enqueue(String item) {
		list.addFirst(item);
	}
	
	public String dequeue() {
		return list.removeFirst();
	}
	
	public String peek() {
		return list.peek();
	}
	
	public void dequeueAll() {
		list.clear();
	}
	
	public String toString() {
		return list.toString();
	}
	
	public static void main(String[] args) {
		MyQueue list = new MyQueue();
		for (int i = 0; i < 5; i++) list.enqueue("Uno" + i);
		System.out.println(list);
		
		list.dequeue();
		list.dequeue();
		System.out.println(list);
	}
}