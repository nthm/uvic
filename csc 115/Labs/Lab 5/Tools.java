import java.util.Stack;

public static boolean isBalanced(String str) {
	Stack<Character> stack = new Stack<Character>();
	for (char item : str.toCharArray()) {
		switch(item) {
			case '(':
				stack.push(item);
				break;
			case ')':
				if (stack.empty() || stack.pop() != '(')
					return false;
				break;
		}
	}

	if (stack.empty())
		return true;

	return false;
}