/*
	Notes in Lab 1:
	- Integers have 4 bytes and doubles have 8
	- An object is the same as a variable but for abstract data types

	Planning a new class: Time

	Time
	Attributes:
		- hour
		- minute
	Behaviours:
		- {get,set}{Minute,Hour}
		- equals
		- compareTo
		- output
*/

public class Time {
	private int hour,
				minute;

	public Time() {
		this.hour 	= 0;
		this.minute = 0;
	}

	public Time(int hours, int minutes) {
	/*	Avoid repeating code
	
		this.hour 	= hours % 24;
		this.minute = minutes % 60;
	*/
		setHour(hours);
		setMinute(minutes);
	}
	
	// Get methods
	public int getHour() {
		return this.hour;
	}
	
	public int getMinute() {
		return this.minute;
	}
	
	// Set methods
	public void setHour(int hour) {
		this.hour = hour % 24;
	}
	
	public void setMinute(int minute) {
		this.minute = minute % 60;
	}
	
	public boolean equals(Time other) {
		return this.hour 	== other.hour
			&& this.minute 	== other.minute;
	}
	
	// Pretty printing
	public String toString() {
		return this.hour + ":" + (this.minute < 9 ? "0" + this.minute : this.minute);
	}
	
	public static void main(String[] args) {
		Time one = new Time();
		System.out.println(one.hour + ":" + one.minute);
    /*
    	In the println, those variables are able to be accessed despite their 
    	`private` status because it's being called in the same file.
    	
    	Introduction to hexidecimal
    	0..9 becomes 0..9ABCDEF
    	
    	Decimal		Hexidecimal
    	14			E
    	15			F
    	16			10
    	17			11
    	
    	Hex is used for memory addresses. Such as 0xFA
    */
        Random rand = new Random();
        
        Time[] arrayTime = new Time[4];
        for (int i = 0; i < arrayTime.length; i++) {
            // Fill with random Time objects
            arrayTime[i] = new Time(rand.nextInt(24), rand.nextInt(60));
            System.out.println(i + ". " + arrayTime[i].toString());
        }
    }
}   
