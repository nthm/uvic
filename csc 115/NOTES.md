## Lecture 1

Email: vanrooji@uvic.ca

Midterm dates:

- February 4
- March 17

Java: Walls & Mirrors, 3rd edition (Recommended)
Labs in ECS 258

The course will be about efficiency with algorithms and data structures

#### Data types and variables:
- Reference: a variable is used to locate an object in memory (the heap)
- Java doesn't allow arthmitic on references

---

## Lecture 2

Small note on `printf()`
Syntax: `% [flags] [width] [.precision]`

Example used in the previous lecture: `("% 3.2f\n", data)`

#### Classes:

- Must have a keyword class followed by name of the class
- Must have a body
- Optional:
	- subclassing modifier (ie. `extend`)
	- TODO
	
The constructor is called only once when creating a `new` object.

#### Packages

Use `import` directive:

- `java` package does not need to be linked to your directory
- `java.lang` package is imported by default

#### Type: `static`

When saying `public static final double` static means it only makes one copy 
in memory.

Note: parseInt on a double will __not__ truncate it to an integer. It will 
throw an error

---

## Lecture 3

#### Copying Objects

As we know, referencing two variables to one array in memory will "change" 
either variable when the array is updated.

```
int[] a = {1, 2, 3, 4, 5},
	  b = a;
```

Any change such as `a[1] = 7` or `b[4] = 2` effects both.

#### Encapsulation

_"You need to boil a kettle of water. You use a power outlet. Does it matter 
how the power is generated?"_

No, it doesn't. That's encapsulation: it's the idea of a black box in computing.

The teacher goes on to say that it doesn't matter what's in the black box 
because it gets the job done. That's wrong, it matters when you're looking for 
performance and reliability. That's why people read APIs.

Anyway,

Example: Consider a BankAccount data type:

```
// Private ("Encapsulated")
- balance    : double
- numDeposit : int
- numWithdraw: int

// Public
+ getBalance()
+ withdraw(double amount)
+ deposit(double amount)
+ transferFrom(BankAccount from, double amount)
```

The private fields are called "Information hiding" ew.

#### Inheritance

- Code Reuse
- Relating data in a **conceptual way** with _"is a"_

We define these relations in Java with the `extends` keyword.

```
public class Mammal {
	private int eyeColour;
	
	public int getEyeColour() {
		// Stuff here
	}
}

public Dog extends Mammal {
	public unrelatedMethod() {
		// Stuff
	}
}

Dog Rosie = new Dog()
Rosie.getEyeColour() // Works fine.
```

Terms subclass and superclass are self explanatory

##### Overiding

This is used to overwrite a method already defined in a parent (extended).
Overiding happens without any special work. Just define a method that has the 
same name as a parent.

If the parent doesn't want the children to see the `private` variables (which 
__are__ accesible by __all__ children) then they need to be switched to 
`protected`.

#### Polymorphism

Greek roots:

- poly: many
- morph: shapes

This is tightly linked this the concept of object oriented programming. It 
allows all children of a type to be used where that type is valid. Best 
described by an example...

```
BankAccount[] array = new BankAccount[5];

array[0] = new ChildOfBankAccount();
array[1] = new AnotherChildOfBankAccount();
array[2] = new BankAccount();
```

All works fine, however, __limitation/exception__:

```
ChildOfBankAccount name = new ChildOfBankAccount("Info", 200);
array[3] = name;

name.method()     // Is okay
array[3].method() // Illegal
```

The last line is illegal because it's a BankAccount type so it only has the 
methods from that type - not its children.

#### Composition

Different from inheritance and can be thought of as _"has some/one"_ rather 
than __"is a"__. Think databases again.

Creates objects that are made up of other objects.

---

#### Summary:

Encapsulation:
- Objects have attributes and behaviours
- Classes help seperate interfaces and implementation

Inheritence:
- "is a" relationships
- Enables re use of code

Polymor
- Objects may be used according to the interfaces of the classes and super 
  classes

Composition:
- Objects made of other objetcs
- "has a" relationship indicates meaning for included objects

---

## Lecture 4

#### Abstract Data Types

##### Aside: Computer counting

Binary, 0's and 1's:
- You can make a number as large as 2^(length of binary) - 1
- Example: 111 in binary has length 3, so 2^3 - 1 = 7 is the largest number

#### Introduction to abstraction:
Summary: _Don't worry about things we don't need to worry about_

Modularity makes things more manageable by isolating errors in code and 
eliminates redundancy in code. The textbook uses the metaphor of "walls" 
seperating tasks/code.

I prefer the business model...

The communication between modules is described through documentation: In Java 
it's called "the specification document". The API.

#### Data abstraction
- Begins from the premise that the effective problem solutions __depends upon 
  access data__
- Therefore we build the interface off of what we need to do
TODO

#### Abstract Data Types (ADT)
- An ADT is:
	- An __aggregation of data__ (homogeneous or heterogeneous)
	- A __set of operations__ that others may use on that data
- A specification for an ADT:
	- A possibly executable clear description of what must be true about ADT 
	  operations
TODO

In summary:

An abstract data type can be visualized as a wall isolating data structures 
from the non-ADT code utilizing the data structures. Encapsulation helps this.

Difference between:
- ADT (Abstract data types):
  collection of data and a set of operations on that data
- Data structure:
  construct in a programming language to store an aggregation of data

Let's take a look at an ADT: __The List__

We can use terms like _head_ and _tail_ to describe positions in a list. We can 
say the list has a number of items. We're speaking in abstractions because 
we're not saying "the array has four elements".

Here might be some operations in ADT:
- __Create__ an empty list
- Determine if it's __empty__
- Determine __number of items__
- __Insert__ (add) at a given position
- __Remove__ an item at a position
- __Clear__ the list (remove all)
- __Retrieve__ (get) the item at a given position

In a UML it might look like this:

```
// Private
- items

// Public
+ createList()
+ isEmpty()
+ getLength()
+ etc...
```

#### `implements`

Used with an `interface` such as:

```
public interface Bike {
	/** Docstrings and such... */
	public boolean method(int index);
	
	/** Another docstring/explanation of the code */
	public int anotherMethod() throws anException;
}

public class Motorbike implements Bike {
	int speed = 0,
		gear  = 0;
	
	public method(int index) {
		return x > 0 ? true : false; // Or something...
	}
	
	public anotherMethod() {
		// Stuff
	}
}
```

#### An array based implementation

Let's start with something we know. A one dimensional array of integers for 
example.

`[ k ] = [ 12, 14, 100, 2, 4 ]` has indexes 0 to 4 but items 1 - 5

Maybe there's a `MAX_ITEMS` variable somewhere in the code too.

#### Exceptions
- We've seen two categories so far
	- computational constructs: basic data & control flow
	- program-organization constructs: TODO

Exceptions represent specific errors that can occur at runtime. They are said 
to be `thrown` and `caught` by Java.

---

## Lecture 5

#### ADT Redux

TODO: Last class we apparently talked about __contracts__? Probably gonna be on 
a midterm so add that to the last lecture.

Difference between ADT and another data structure. ADT contains a data 
structure which is like a primitive type. It's not abstract.

Apparently you must use __all__ the APIs (methods of an interface) provided. It 
won't compile otherwise: Fixed the `implements` method from last lecture.

Linked lists... yay. Dealing with references.

## Lecture 6

Reference based linked list. Finally something new. A linked list and its nodes 
are not accessed or declared in the same way as an array. They are defined by a 
reference to a head node. This is how we'd create a base linked list:

```
package linkedlist;

class IntegerNode {
	Integer			item;
	IntergerNode	next;
	
	IntegerNode(Integer item) {
		this.item = item;
		this.next = null;
	}
	
	IntegerNode(Integer item, IntegerNode next) {
		// You get the point.
	}
}
```

How to use this class:

```
public class Tester {
	public static void main(String[] args) {
		IntegerNode n = new IntegerNode(new Integer(30));
		IntegerNode head = new IntegerNode(new Integer(40), n);
	}
}
```

This works with a tail first approach (for now...)

By convention, we use `next`, `prev`, and `curr` for talking about positions in 
a linked list. 

## Lecture ? (maybe 8)

#### Generics

Used as a placeholder when developing classes and also for postponing details. 
This is defined with `<E>` where `E` represents the data type which will be 
specificed by whatever implementation uses it.

```
public class Node<E> {
	E data;
	Node<E> link;
	
	public Node(E data, Node<E> link) {
		this.data = data;
		this.link = link;
	}
	
	public E getData() {
		return data; // Will be of type E
	}
}
```

The code would be used by defining what `E` will be, in this case, `String`:

```
// Pretend class, static main, etc is here... {
	Node<String> node = new Node<>("2", null);
	Node<String> head = new Node<>("5", node);
}
```

The output of `println(head.getData() + head.link.getData())` is `"45"`

---

## Lecture 9

#### Midterm next Thursday.

#### Recursion

Recursion is stopped at the _base case_, which is the turning point to start 
winding back up.

__BigInt__ It its own class and it's more awkward to do arithmetic with than 
integers. `BigInteger num = new BigInteger("17839187382");`

#### The Fibonacci sequence: As an example of recursion

It is an example of how recursion can become slow, as more and more 
calculations and necessary. There's code posted for both the recursive and 
iterative implementations of the sequence.

## Lecture 10

Backtracking in recursion. An example would be a maze solving algorithm. Terms 
such as `pp` (partial path) are used.

| Definitions
| - - - - - -
| "Top of tree" | Decision tree root
| "Leaf node"   | Endpoint

#### The midterm is mostly multiple choice, read ch4.

## Lecture involving stacks and heaps

Stacks include methods like `top` and `peek` to access or look at the top of 
the stack. We could use an array or a linked list to implement a stack.

## Lecture

#### Postfix and Prefix expressions:

Post: 2 3 4 + * -> Infix: 2 * (3 + 4)

This is done using stacks. Add to the stack when there's a number and pop the 
last two numbers off the stack once an operand is encountered.

Conversion from infix to postfix can be done using a linked list and a stack.
Now ontop an unrelated topic...

#### Trees :D

- Binary trees and Binary search trees
- Treversal
- Implementation approaches

What is a tree:

It's a set of nodes such that the tree is partitioned into disjoint subsets. 
Trees are partitioned into:
	- A single node __r__, also called a root
	- (Possibly empty) subsets of __r__

Nodes have a _child_/_parent_ relationship. They know their children but not 
their children's children.

Binary trees: They're just trees with a max of two children 
per tree (node).

Binary search trees: Is a binary tree with a set order of its node. For each 
node in a binary search tree:
	- The value of the left child of a node is smaller than the node itself.
	- The right child is larger.

These two rules _must_ apply to every node and subset thereof or else it's 
not a binary tree.

__Height__ of any tree is defined as the number of nodes on the longest path 
from the root to a leaf. Number of the nodes in a __full (no holes)__ tree of 
a given height is: ` 2^{height} - 1 `

Full trees vs Complete trees vs Balanced trees:
- Full: All nodes above the last (leaf) level have 2 children.
- Complete: It is filled from left to right with no holes. It does not need to 
be full.
- Balanced: The height of each node's sibling trees differ from no more than 1.

```
Balanced:   Unbalanced:
    o           o
   / \         / \
  o   o       o   o
 /           / \   \
o           o   x   o
           /
          o
```
Would be balanced if `x` was there. Pretend it isn't.

Therefore, full trees are complete and balanced; complete trees are balanced.

In order to traverse the tree you must start in the bottom left node, go up to 
the parent, descend to each sibling, and then move to the parent's parent. If 
this is done in a binary search tree the output of the traversal will be 
ordered from the smallest value to the largest value.

...Unless it's a __Preorder traversal__ which starts from the root node and 
works down the left side until the leaf is reached. Then it goes down the 
right end.

Next lecture will talk about array based implementations.

## Lesson: Algorithm Efficiency

-	Sequential search, look at each element
- 	Binary search, needs to be sorted, looks half way in the list and tests a 
	value. Calls itself recursively on smaller sections.
	
Runtime efficiency, O(n), makes a lot of assumptions tbh

Radix sort is awesome