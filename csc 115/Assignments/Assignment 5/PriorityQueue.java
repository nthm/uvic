import java.util.NoSuchElementException;

/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Sunday, April 3rd, 2016
	Filename: PriorityQueue.java
	Details:  CSC115 Assignment 5: Emergency Room
*/

public class PriorityQueue {
	
	private Heap heap;

	/**	Creates a new empty priority queue
	*/
	public PriorityQueue() {
		heap = new Heap();
	}

	/**	Inserts an item into the queue
	
		@param item		The item to insert
	*/
	public void enqueue(Comparable item) {
		heap.insert(item);
	}

	/**	Removes the highest priority item from the queue
	
		@retruns		The item
		@throws			NoSuchElementException if the queue is empty
	*/
	public Comparable dequeue() {
		try {
			return heap.removeRootItem();
		} catch (NoSuchElementException err) {
			throw err;
		}
	}

	/**	Retrieves the highest priority item in the queue (without removing it)
	
		@retruns		The item
		@throws			NoSuchElementException if the queue is empty
	*/
	public Comparable peek() {
		try {
			return heap.getRootItem();
		} catch (NoSuchElementException err) {
			throw err;
		}
	}

	/**	@returns		True if the heap is empty, false otherwise
	*/
	public boolean isEmpty() {
		return heap.isEmpty();
	}
	
	/**	Internal testing
	
		@param args		Not used
	*/
	public static void main(String[] args) {
		PriorityQueue queue = new PriorityQueue();
		System.out.println("Empty? (should be yes)..." + (queue.isEmpty() ? "Yes" : "No"));
		
		for (int i = 0; i < 5; i++) {
			System.out.println("Queuing: " + i);
			queue.enqueue(i);
		}
		
		System.out.println("Peek: " + queue.peek());
		
		for (int i = 0; i < 5; i++)
			System.out.println("Dequeuing: " + queue.dequeue());
		
		System.out.print("Testing for exception throwing...(should say PASSED) ");
		try {
			queue.dequeue();
		} catch (NoSuchElementException err) {
			System.out.println("PASSED");
		}
		
		System.out.println("Empty? (should be yes)..." + (queue.isEmpty() ? "Yes" : "No"));
	}
}
