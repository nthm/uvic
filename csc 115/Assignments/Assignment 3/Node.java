/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Tuesday, Feb 28th, 2016
	Filename: ArithExpression.java
	Details:  CSC115 Assignment 3: Calculator
*/

/**
	This node class is used to build a one way string based linked list.
	In this assignment it is used in the StringStack class.
*/

class Node {
	protected String item;
	protected Node next;
	
	/**	Creates a new node containing a given item and link it to another node
	
		@param item		The string to hold inside the node
		@param next		THe node to come after this created one
	*/
	protected Node(String item, Node next) {
		this.item = item;
		this.next = next;
	}
	
	/** Creates a new node that is not attached to another (therefore not part 
		of a list or stack)
		
		@param item		The string to hold inside the node
	*/
	protected Node(String item) {
		this(item, null);
	}
}