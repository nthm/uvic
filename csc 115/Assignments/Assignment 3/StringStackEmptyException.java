/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Tuesday, Feb 28th, 2016
	Filename: ArithExpression.java
	Details:  CSC115 Assignment 3: Calculator
*/

/*
	An exception that is thrown when a StringStack operation requiring items 
	to exist in the list is called when the list is empty.
*/

public class StringStackEmptyException extends RuntimeException {
	
	/**	Creates an exception with a message passed to RuntimeException
	
		@param msg		The error message
	*/
	public StringStackEmptyException(String msg) {
		super(msg);
	}
	
	/**	Creates an empty exception
	*/
	public StringStackEmptyException() {
		super();
	}
}