/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Tuesday, Feb 28th, 2016
	Filename: ArithExpression.java
	Details:  CSC115 Assignment 3: Calculator
*/

/*
	Uses the node class to build a one way linked list (as a stack) to hold 
	string items.
*/

public class StringStack {

	private Node head;

	// No constructor: http://stackoverflow.com/questions/3641114/
	
	/**	Determines if the stack is empty.
		
		@return		True if empty and false otherwise
	*/
	public boolean isEmpty() {
		return head == null;
	}

	/**	Returns the topmost item off of the stack and removes it.
	
		@return		The string on the top of the stack
		@throws		StringStackEmptyException if the stack is empty
	*/
	public String pop() {
		if (head == null)
			throw new StringStackEmptyException();
		
		String item = head.item;
		head = head.next;
		
		return item;
	}

	/**	Returns the topmost item on the stack but does not remove it.
	
		@return		The string on the top of the stack
		@throws		StringStackEmptyException if the stack is empty
	*/
	public String peek() {
		if (head == null)
			throw new StringStackEmptyException();
		return head.item;
	}

	/**	Adds a new item to the top of the stack
	
		@param item		The item to add
	*/
	public void push(String item) {
		Node toPush = new Node(item);
		
		toPush.next = head;
		head = toPush;
	}

	/**	Remove all items in the stack
	*/
	public void popAll() {
		head = null;
	}
	
	/**	Used for testing the class
	
		@param args		Not used
	*/
	public static void main(String[] args) {
		StringStack test = new StringStack();
		
		// Build a stack and view each item
		System.out.print("Peek: ");
		for (int i = 0; i < 5; i++) {
			test.push((i * i) + "");
			System.out.print(test.peek() + " ");
		}
		
		// Pop one
		System.out.println("\nPopped: " + test.pop());
		
		System.out.println("Empty? (should be false)..." + test.isEmpty());
		test.popAll(); // Now it will be empty
		System.out.println("Empty? (should be true)...." + test.isEmpty());
		
		System.out.print("Peeking when empty: ");
		try {
			test.peek();
		} catch (StringStackEmptyException err) {
			System.out.println("Passed");
			return;
		}
		System.out.println("Failed");
	}
}
