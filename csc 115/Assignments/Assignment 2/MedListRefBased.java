/*
	Name:	  Grant Hames-Morgan
	ID:		  V00857826
	Date:	  Tuesday, Feb 11th, 2016
	Filename: MedListRefBased.java
	Details:  CSC115 Assignment 2
*/

/**
	Reference (linked list) based implementation of List&lt;E&gt; to store 
	Medication items. Uses doubly linked lists with `.prev` and `.next`.
*/

public class MedListRefBased implements List<Medication> {
	private int count;
	private int currentIndex;
	private MedicationNode head, node; // `node` is the current node
	
	/** A helper method used to avoid repeating code in methods that require 
		traversing the list to approach a certain index. It modifies the 
		global variables `node` and `currentIndex` which are accessible by 
		other methods.
		
		@param index	Index to approach
	*/
	private void approachIndex(int index) {
		if (head == null || index < 0 || index > count)
			throw new ListIndexOutOfBoundsException("Index out of bounds: " + index);
		
		node = head;
		for (currentIndex = 0; currentIndex != index; currentIndex++)
			node = node.next;
	}
	
	public MedListRefBased() {
		head = node = null;
		count = 0;
	}
	
	public void add(Medication item, int index) {
		if (index < 0 || index > count)
			throw new ListIndexOutOfBoundsException("Index out of bounds: " + index);
		
		MedicationNode newNode = new MedicationNode(item);
		
		if (head == null) {
			head = newNode;
			
		} else if (index == 0) {	
			newNode.next = head;
			head.prev = newNode;
			
		} else {
			approachIndex(index - 1);
			
			// Link the new node to the list
			newNode.prev = node;
			newNode.next = node.next; // May be null

			// Reassign the surrounding items to the new node
			if (node.next != null)
				node.next.prev = newNode;
			node.next = newNode;
		}
		
		count++;
	}
	
	public void remove(int index) {
		if (head == null || index < 0 || index >= count)
			throw new ListIndexOutOfBoundsException("Index out of bounds: " + index);
		
		if (index == 0) {
			head = head.next; // May be null
			if (head != null) head.prev = null;
		} else {
			approachIndex(index);
			
			// There will always be node.prev and node.next may be null
			node.prev.next = node.next;
			
			if (node.next != null)
				node.next.prev = node.prev;
		}
		
		count--;
	}
	
	public void remove(Medication item) {
		while (true) {
			int index = find(item);
			if (index == -1) break;
			
			remove(index);
		}
	}
	
	public Medication get(int index) {
		approachIndex(index);
		return node.item;
	}
	
	public boolean isEmpty() {
		return count == 0;
	}
	
	public int size() {
		return count;
	}
	
	public int find(Medication item) {
		node = head;
		for (currentIndex = 0; currentIndex < count; currentIndex++) {
			if (node.item.equals(item)) return currentIndex;
			node = node.next;
		}
		
		return -1; // Not found
	}
	
	public void removeAll() {
		head  = null;
		count = 0;
	}
	
	@Override
	public String toString() {
		String lines = "List: {";
		
		for (node = head; node != null; node = node.next)
			lines += "\n\t" + node.item + (node.next == null ? "\n" : ", ");
		
		return lines + "}";
	}
}