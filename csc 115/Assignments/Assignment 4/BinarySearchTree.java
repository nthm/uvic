/*
	Name:     Grant Hames-Morgan
	ID:       V00857826
	Date:     Tuesday, Mar 12th, 2016
	Filename: BinarySearchTree.java
	Details:  CSC115 Assignment 4: Patient Location
*/

/*
	The shell of the class, to be completed as part of CSC115 Assignment 4: 
	Patient Location.
*/

import java.util.ArrayList;

public class BinarySearchTree<E extends Comparable<E>> extends BinaryTree<E> {
	private TreeNode<E> node = root,   // Current node
	                    parent = null; // Parent of node
	
	/**	Attempts to move the global node variable to the node of the given 
		item. Exits when the node is either found of it becomes impossible to 
		continue towards the item (therefore it doesn't exist).
		
		@param item		The item to try and approach
	*/
	private void approachNode(E item) {
		node   = root; // Reset to root
		parent = null;
		
		while (true) {
			int res = item.compareTo(node.item);
			if (res == 0)
				break; // Found
			TreeNode<E> test = res > 0 ? node.right : node.left;
			if (test == null)
				break; // Can't go further
			parent = node;
			node   = test;
		}
	}

	/**
	 * Create an empty BinarySearchTree.
	 */
	public BinarySearchTree() {
		super();
	}

	/**	Inserts a given item into the tree, maintaining tree order. If the 
		item already exists then nothing happens.
	
		@param item		The item to insert
	*/
	public void insert(E item) {
		TreeNode<E> newNode = new TreeNode<>(item);
		if (root == null) {
			root = newNode;
		} else {
			approachNode(item);
		/*
			Should return a leaf where node.item != item, however if 
			a node where node.item == item (res == 0) is returned do nothing.
		*/
			int res = item.compareTo(node.item);
			if      (res > 0) node.right = newNode;
			else if (res < 0) node.left  = newNode;
		}
	}
	
	/**	Retrieve (if possible) the item of the given key from the tree.
		
		@param key		The key of the item to retrieve
		@returns		The item (if it exists)
	*/
	public E retrieve(E key) {
		approachNode(key);
		return node.item.equals(key) ? node.item : null;
	}
	
	/**	Finds the item (by a given key) and, if found, removes it.
		
		@param key		The key that is equal to the item. Equality is decided 
		          		by the `equals()` method of the item
	*/
	public void delete(E key) {
		if (root == null) return;
		
		approachNode(key);
		if (!node.item.equals(key)) return;
		
		boolean hasR = node.right != null,
		        hasL = node.left  != null;
		
		if (node == root) {
			// Root is not null, handled above
			
			// No children, destory the tree
			if (!hasR && !hasL) makeEmpty();
			
			// Two children
			else if (hasR && hasL) {
				// Find largest item on left side
				node = root.left;
				while (node.right != null)
					node = node.right;
				
				// Delete found node
				delete(node.item);
				
				// Swap items
				root.item = node.item;
			}
			// One child
			else root = hasL ? root.left : root.right;
		}
		// Now the parent can't be null
		else {
			boolean onParentsLeft = parent.left == node;
			
			// No children (leaf node)
			if (!hasR && !hasL) {
				if (onParentsLeft) parent.left  = null;
				else               parent.right = null;
			}
			// Two children
			else if (hasR && hasL) {
				if (onParentsLeft) {
					parent.left = node.right;
					parent.left.left = node.left;
				} else {
					parent.right = node.right;
					parent.right.left = node.left;
				}
			}
			// Has one child
			else {
				if (hasL) {
					if (onParentsLeft) parent.left  = node.left;
					else               parent.right = node.left;
				} else {
					if (onParentsLeft) parent.left  = node.right;
					else               parent.right = node.right;
				}
			}
		}
	}
	
	/**
	 * Places all the items in the tree into a sorted list.
	 * @return the sorted list.
	 */
	public ArrayList<E> inOrder() {
		ArrayList<E> list = new ArrayList<E>();
		collectInOrder(list, root);
		return list;
	}

	/**	Recursively visit each node from left, to self, to right, and add it 
		to a given array.
		
		@param list		The list to add items to
		@param node		The node to visit
	*/
	private void collectInOrder(ArrayList<E> list, TreeNode<E> node) {
		if (node != null) {
			collectInOrder(list, node.left);
			list.add(node.item);
			collectInOrder(list, node.right);
		}
	}

	/**
	 * Internal test harness.
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		BinarySearchTree<PatientLocation> tree = new BinarySearchTree<PatientLocation>();
	/*
		Commented out some additional names I used for testing in case the 
		tester would break once seeing them.
	*/
		String[][] names = {
			{ "Duck", "Donald" }, { "Mouse" , "Minnie" },
			{ "Dog" , "Goofie" }, { "Newman", "Alfred" }
			// { "Zed" , "Dunno"  }, { "ABC"   , "CBA"    }
		};
		int[] numbers = { 338, 116, 422, 607 }; // 827, 260
		PatientLocation[] patients = new PatientLocation[names.length];
		
		System.out.print("Inserting...");
		for (int i = 0; i < names.length; i++) {
			patients[i] = new PatientLocation(names[i][0], names[i][1], numbers[i]);
			tree.insert(patients[i]);
		}
		System.out.println("OK");
		
		for (int i = 0; i < names.length; i++)
			System.out.printf("Retrieving: %-30.30s Found: %-30.30s%n",
			                  patients[i], tree.retrieve(patients[i]));
		
		System.out.println("\nList: " + tree.inOrder() +
		                   "\nHeight: " + tree.height());
	/*
		I've commented out most lines that print the tree so the testing 
		computer doesn't have 4 windows suddenly open.
	*/
		new DrawableBTree<PatientLocation>(tree).showFrame();
		
		System.out.println("Deleting nodes");
		
		System.out.println("Leaf node");
		tree.delete(patients[3]); // 5
		//new DrawableBTree<PatientLocation>(tree).showFrame();
		
		System.out.println("Middle node, with one child");
		tree.delete(patients[1]);
		//new DrawableBTree<PatientLocation>(tree).showFrame();
		
		System.out.println("Root node");
		tree.delete(patients[0]);
		//new DrawableBTree<PatientLocation>(tree).showFrame();
		
		tree.makeEmpty();
		
		if (tree.isEmpty())
			System.out.println("Passed all tests :)");
	}
}
