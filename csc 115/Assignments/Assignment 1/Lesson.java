/*
	Name:	Grant Hames-Morgan
	ID:		V00857826
	Date:	Frday, January 8th, 2016
	Filename:	Lesson.java
	Details: CSC115 Assignment 1
*/

/**
	Class Lesson holds a list of skiers (through SkierList, see SkierList.java) 
	of a certain level. The level of the lesson is assigned a name and the 
	level of each enrolled student is set to that of the lesson. There is no 
	limit to the number of students in a lesson - it may be empty.
 */

public class Lesson {
	private static final String[] levelNames = 
		{"Beginner", "Novice", "Snowplower", "Intermediate", "Advanced"};
	private int lessonLevel;
	private String lessonName;
	private SkierList students;

	/** A helper method used to avoid repeating code in methods that require a 
		valid level for the lesson.
		
		@param level	Level to be validated
		@returns		The level if it's between 0 and 4 (inclusive), and 0 if 
						otherwise.
	*/
	private void initLevel(int level) {
		if (level > 4 || level < 0) level = 0;
		this.lessonLevel = level;
		this.lessonName  = this.levelNames[level];
	}
	
	
	/**	Creates a new lesson with the given level and an 
		empty list of students.
		
		@param level	The level of the lesson
	*/
	public Lesson(int level) {
		initLevel(level);
		this.students = new SkierList();
	}

	/**	Creates a new lesson with the given level and 
		populates it with the given list of students.
		
		@param level	The level of the lesson
		@param students	List of students to add to the lesson
	*/
	public Lesson(int level, SkierList students) {;
		initLevel(level);
		this.students = new SkierList();
												  
		for (int i = 0; i < students.size(); i++)
			this.students.add(students.get(i));
	}

	/**	Set the level of the lesson.
	
		@param level	Lesson level. Must be between 0 and 4 (inclusive)
	*/
	public void setLessonLevel(int level) {
		initLevel(level);
	}

	/**	Retrieves the lesson name.
		
		@return		The name of the lesson
	*/
	public String getName() {
		return this.lessonName;
	}

	/**	Retrieves the number of students in the lesson.
		
		@return		Number of students
	*/
	public int numStudents() {
		return this.students.size();
	}

	/**	Adds a skier to the lesson. The student's level is set to match that of 
		the lesson's.
	
		@param skier	The student to add
	*/
	public void addSkier(Skier skier) {
		skier.setLevel(this.lessonLevel);
		this.students.add(skier);
	}

	/**	Removes a skier from the lesson.
	
		@param skier	The student to remove
	*/
	public void removeSkier(Skier skier) {
		int index = this.students.findSkier(skier);
		if (index > -1)
			this.students.remove(index);
	}

	/**	Determines if the skier is registered in the lesson.
		
		@param skier	The student to search for
		@return			True if the student is registered, false otherwise
	*/
	public boolean isRegistered(Skier skier) {
		if (this.students.findSkier(skier) > -1) return true;
		return false;
	}

	/**	Overrides the toString() method from java.lang.Object and returns 
		the lesson name on one line with all of the students on following 
		lines. The syntax of each student line is determined in {@link 
		Skier#toString} Syntax:
		
		<pre>
[lesson name] group:
[student.toString]
[student.toString]
...
		</pre>
		
		@return		String following the above format.
	*/
	@Override
	public String toString() {
		String lines = this.lessonName + " group:\n";
		
		for (int i = 0; i < this.students.size(); i++)
			lines += this.students.get(i).toString() + "\n";
		
		return lines;
	}

	/**
	 * Used as a test harness for the class.
	 * @param args Not used.
	 */
	public static void main(String[] args) {
		System.out.println("Testing the Lesson class.");
		Lesson lesson = null;
		String[] group = {"Daffy Duck", "Bugs Bunny", "Betty Boop",
			"Roger Rabbit", "Han Solo", "Chewbacca"};
		try {
			lesson = new Lesson(2);
		} catch (Exception e) {
			System.out.println("Failed to construct a Lesson object.");
			e.printStackTrace();
			return;
		}
		if (!lesson.getName().equals("Snowplower")) {
			System.out.println("Failed at test one.");
			return;
		}
		if (lesson.numStudents() != 0) {
			System.out.println("Failed at test two.");
			return;
		}
		lesson.setLessonLevel(3);
		if (!lesson.getName().equals("Intermediate")) {
			System.out.println("Failed at test three.");
			return;
		}
		for (int i=0; i<group.length; i++) {
			lesson.addSkier(new Skier(group[i]));
		}
		if (lesson.numStudents() != 6) {
			System.out.println("Failed at test four.");
			return;
		}
		System.out.print("Checking the toString: Should see a list of ");
		System.out.println("6 skiers, all with level 3");
		System.out.println(lesson);
		
		System.out.println("Checking constructor that takes a list.");
		int[] levels = {0,3,2};
		int levelIndex = 0;
		SkierList list = new SkierList();
		for (int i=0; i<group.length; i++) {
			list.add(new Skier(group[i],levels[levelIndex]));
			levelIndex = (levelIndex+1)%levels.length;
		}
		try {
			lesson = new Lesson(4,list);
		} catch (Exception e) {
			System.out.println("Constructor not working.");
			e.printStackTrace();
			return;
		}
		if (lesson.numStudents() != 6) {
			System.out.println("Failed at test five.");
			return;
		}
		for (int i=0; i<list.size(); i++) {
			if (!lesson.isRegistered(list.get(i))) {
				System.out.println("Failed at test six.");
				return;
			}
		}
		Skier removed = list.get(3);
		lesson.removeSkier(removed);
		if (lesson.isRegistered(removed)) {
			System.out.println("Failed at test seven.");
			return;
		}
		if (lesson.numStudents() != 5) {
			System.out.println("Failed at test eight.");
			return;
		}
		System.out.print("The following printout should consist of 5 ");
		System.out.println("skiers with varying levels:");
		System.out.println(lesson);
		System.out.println("Testing completed.");
	}
}
