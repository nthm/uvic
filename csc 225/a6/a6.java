import java.io.*;
import java.util.*;

public class Solution {

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws Exception {
        FastReader in = new FastReader();
        int n = in.nextInt();
        // could use n / 2 for the size but I'm not sure if it'll resize at 75%
        // capacity like HashMaps do
        PriorityQueue<Integer> minHeap = new PriorityQueue<Integer>(n, Collections.reverseOrder());
        PriorityQueue<Integer> maxHeap = new PriorityQueue<Integer>(n);

        int[] numbers = new int[n];
        for (int i = 0; i < n; i++) {
            numbers[i] = in.nextInt();
        }
        int mod = 1000000007;
        int median;
        if (numbers[0] < numbers[1]) {
            median = numbers[0];
            maxHeap.add(numbers[1]);
        } else {
            median = numbers[1];
            maxHeap.add(numbers[0]);
        }
        // why this is necessary to not overflow?
        long result = 1L * (((long)numbers[0] * (long)median) % mod);
        System.err.println("Median:" + numbers[0]);
        System.err.println("Median:" + median);
        System.err.println("Result:" + result);

        for (int i = 2, current; i < n; i++) {
            System.err.println("\n\nSTART");
            current = numbers[i];
            if (current < median) {
                System.err.println("Adding to MIN: " + current);
                minHeap.add(current);
            } else if (current > median) {
                System.err.println("Adding to MAX: " + current);
                maxHeap.add(current);
            } else {
                System.err.println("This shouldn't happen" + current);
            }

            System.err.println("Start:\n" + minHeap.toString() + " < " + median + " < " + maxHeap.toString());
            int difference = minHeap.size() - maxHeap.size();
            if (difference == 1) { // min is larger by one
                System.err.println("Min too large! Moving");
                maxHeap.add(median);
                median = minHeap.poll();
            } else if (difference < -1) { // max is larger by more than one
                System.err.println("Max larger by more than one! Moving");
                minHeap.add(median);
                median = maxHeap.poll();
            }
            System.err.println("Final:\n" + minHeap.toString() + " < " + median + " < " + maxHeap.toString());
            System.err.println("New median: " + median);
            result = (result * median) % mod;
            System.err.println("Result: " + result);
        }
        System.out.println(result);
    }
}
