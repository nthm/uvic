import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Queue;

public class Solution {
  static class Cell {
    int x;
    int y;
    Cell parent;
    Cell(int x, int y) {
      this.x = x;
      this.y = y;
      this.parent = null;
    }
    public String toString() {
      return "(" + this.x + ", " + this.y + ") Parent: " + this.parent;
    }
  }

  // Largely based on the posted lab code but I keep parents in the Cell class
  static class BFS {
    private int size;
    private boolean[][] visited;
    private Queue<Cell> queue;
  
    public BFS(int n){
      size = n;
      visited = new boolean[n][n];
      for (boolean[] row: visited) {
        Arrays.fill(row, false);
      }
    }
  
    public int pathLink(char[][] map, Cell start, Cell target) {
      visited[start.y][start.x] = true;
      queue = new ArrayDeque<>();
      queue.add(start);
  
      while(!queue.isEmpty()) {
        Cell c = queue.poll();
        if (c.x == target.x && c.y == target.y) {
          // Done
          int distance = 0;
          while (c.parent != null) {
            map[c.y][c.x] = '%';
            c = c.parent;
            distance++;
          }

          System.err.println();
          for (char[] row : map) {
            System.err.println(row);
          }
          return distance;
        }

        // XXX: This is bad because it initalizes a lot more objects than needed
        // Up Down Right Left
        Cell[] neighbourCells = {
          new Cell(c.x, c.y + 1),
          new Cell(c.x, c.y - 1),
          new Cell(c.x + 1, c.y),
          new Cell(c.x - 1, c.y)
        };
        for (Cell next : neighbourCells) {
          if (
            // Out of bounds
            next.x >= size || next.x < 0 ||
            next.y >= size || next.y < 0 ||
            // Been there before
            visited[next.y][next.x] ||
            // Wall
            map[next.y][next.x] == '#') {
            continue;
          }
          visited[next.y][next.x] = true;
          next.parent = c;
          queue.add(next);
        }
      }
      return -1;
    }
  }

  public static void main(String[] args) throws Exception {
    Scanner in = new Scanner(System.in);
    int n = Integer.parseInt(in.nextLine());

    Cell start = null;
    Cell target = null;
    char[][] map = new char[n][n];
    for (int row = 0; row < n; row++) {
      // Assume the line has n characters as it should
      String line = in.nextLine();
      map[row] = line.toCharArray();
      for (int col = 0; col < n; col++) {
        char type = line.charAt(col);
        if (type == 'A') {
          start = new Cell(col, row);
        } else if (type == 'B') {
          target = new Cell(col, row);
        }
      }
    }
    in.close();
    System.err.println("Found A: " + start);
    System.err.println("Found B: " + target);

    BFS search = new BFS(n);
    int distance = search.pathLink(map, start, target);
    System.out.println(distance > 0
      ? distance
      : "IMPOSSIBLE");
  }
}
