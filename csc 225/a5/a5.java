import java.io.*;
import java.util.*;

public class Solution {

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws Exception {
        FastReader in = new FastReader();
        int n = in.nextInt();
        // List<String> numbers = new ArrayList<>(n);
        HashMap<String, Integer> map = new HashMap<>(n * 2);

        String tempKey = null;
        Integer tempValue = null;

        for (int i = 0; i < n; i++) {
            tempKey = in.next();
            tempValue = map.get(tempKey);
            // if (tempValue == null) {
            //     tempValue = 1;
            // } else {
            //     tempValue = tempValue + 1;
            // }
            // if (tempValue > largestValue) {
            //     secondLargest = largest;
            //     secondLargestValue = largestValue;
            //     largest = tempKey;
            //     largestValue = tempValue;
            // }
            map.put(tempKey, tempValue == null ? 1 : tempValue + 1);
        }

        String largest = null;
        int largestValue = Integer.MIN_VALUE;

        String current = null;
        int currentValue = Integer.MIN_VALUE;

        for(Map.Entry<String, Integer> pair : map.entrySet()) {
            current = pair.getKey();
            currentValue = pair.getValue();
            if (currentValue > largestValue
            || (currentValue == largestValue && current.compareTo(largest) < 0)) {
                largest = current;
                largestValue = currentValue;
            }
        }
        System.out.println(largest + ' ' + largestValue);
    }
}
