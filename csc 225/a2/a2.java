import java.io.*;
import java.util.*;

public class Solution {

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }

    public static void main(String[] args) {

        FastReader in = new FastReader();
        int n = in.nextInt();

        List<Integer> A = new ArrayList<Integer>(n);
        List<Integer> B = new ArrayList<Integer>(n);
        for (int i = 0; i < n; i++){
            A.add(in.nextInt());
        }
        for (int i = 0; i < n; i++){
            B.add(in.nextInt());
        }
        System.out.println(weirdEqual(A, B) ? "YES" : "NO");
    }

    public static boolean weirdEqual(List<Integer> A, List<Integer> B) {
        // Base case
        if (A.equals(B)) {
            return true;
        }
        int n = A.size();
        if (n != B.size() || n % 2 != 0) {
            return false;
        }
        List<Integer> A1 = A.subList(0, n/2);
        List<Integer> A2 = A.subList(n/2, n);
        List<Integer> B1 = B.subList(0, n/2);
        List<Integer> B2 = B.subList(n/2, n);
        
        // Memoize these because they're duplicated below
        boolean A1B1 = weirdEqual(A1, B1);
        boolean A2B2 = weirdEqual(A2, B2);

        return (
            A1B1 && A2B2 ||
            A1B1 && weirdEqual(A1, B2) ||
            A2B2 && weirdEqual(A2, B1)
        );
    }
}
