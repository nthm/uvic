import java.io.*;
import java.util.*;
import java.math.BigInteger;

public class Solution {

    public static void main(String[] args) throws Exception {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        int power = Integer.parseInt(line.substring(2, 3));
        BigInteger n = new BigInteger(line.substring(4));

        BigInteger two = new BigInteger("2");
        BigInteger start = BigInteger.ZERO;
        BigInteger end = n.add(BigInteger.ONE).divide(two); // 1/1 = 1
        BigInteger guess, result;
        int compare;

        while (start.compareTo(end) != 1) {

            System.out.print("[" + start + ", " + end + "]");
            guess = start.add(end).divide(two);
            result = guess.pow(power);
            System.out.println(" = " + guess + "^" + power + " = " + result);

            compare = result.compareTo(n);
            if (compare == 0) {
                System.out.println(guess);
                return;
            }
            if (compare == 1) {
                System.out.println("High");
                end = guess.subtract(BigInteger.ONE);
            } else {
                System.out.println("Low");
                start = guess.add(BigInteger.ONE);
            }
        }
        System.out.println(line + " has no answer");
    }
}
