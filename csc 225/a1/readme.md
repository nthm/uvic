# Over engineering?

Turns out my algorithm is slow. I did a lot of work to make sure I don't need to
run any extra loops, and I think it's as fast as it could be - it's doing as few
operations as possible. However, speed is more than just the number of
instructions for a program:

I considered my algorithm fast because it only loops though the data once.
That's ideal, no? Compared to an algorithm that sorted the array first, before
any work was done, it should be faster. It's not.

The default sorting is surprisingly fast. As I wrote in the comments of my file:
```java
// Somehow, this is super fast. It's almost like a thousand engineers
// have reviewed and optimized it over many decades hmmm
Arrays.sort(arr);
```

When I apply the rest of my algorithm, which is now much easier to implement
because the array is sorted (so I can work backwards), it's more than _3x_
faster to run.

My misconception is likely that I assume `Arrays.sort()` will internally have at
least two for loops in it, maybe three. Therefore, using it and then having my
own for loop would be far worse for my O() time than just implementing it all
from scratch.

The below tests were run with a file of 1000000 numbers (all numbers fit in as a
standard 32bit integer).

```
11.13.52 ~/Desktop
cat numbers.txt | time java LoopOnlyOnce
99989097 99989097
1.91user 0.06system 0:01.72elapsed 115%CPU
```

Took 1.72s

```
11.13.53 ~/Desktop
cat numbers.txt | time java SortThenLoopBackwards
99989097 99989097
0.85user 0.06system 0:00.55elapsed 166%CPU
```

Took 0.55s!!

The old version of my code is a1.java on 33d8b42f

---

From Oracle's site on Java `Arrays.sort()`:
https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html#sort(int[])

```java
public static void sort(int[] a)
```

Implementation note: The sorting algorithm is a Dual-Pivot Quicksort by Vladimir
Yaroslavskiy, Jon Bentley, and Joshua Bloch. This algorithm offers O(n log(n))
performance on many data sets that cause other quicksorts to degrade to
quadratic performance, and is typically faster than traditional (one-pivot)
Quicksort implementations.
