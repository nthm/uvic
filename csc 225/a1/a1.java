import java.io.*;
import java.util.*;

public class Solution {

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }

        int nextInt() {
            return Integer.parseInt(next());
        }

        String nextLine() {
            String str = "";
            try {
                str = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return str;
        }
    }

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        FastReader in = new FastReader();
        int n = in.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++){
            arr[i] = in.nextInt();
        }

        // Somehow, this is super fast. It's almost like a thousand engineers
        // have reviewed and optimized it over many decades hmmm
        Arrays.sort(arr);

        // Last two elements
        int one = arr[n - 1];
        int two = arr[n - 2];
        int x;
        int y;
        // Initial distance could have likely just as easily been [0] and [1]
        int smallestDistance = Math.abs(one - two);

        // Backwards, from there (no index will be out of bounds)
        for (int i = n - 3; i >= 0; i--) {
            x = arr[i + 1];
            y = arr[i];

            // Trust that the JVM is smart enough that Math.abs() will beat the
            // x > y ? x - y : y - x ternary
            int distance = Math.abs(x - y);
            if (distance > smallestDistance) {
                continue;
            }
            // Don't replace our pair if it's smaller
            if (distance == smallestDistance && x <= one) {
                continue;
            }
            System.out.println("New best match!");
            smallestDistance = distance;
            one = x;
            two = y;
            if (smallestDistance == 0) {
                System.out.println("Match is ideal");
                break;
            }
        }
        System.out.println(
            one < two
                ? one + " " + two
                : two + " " + one);
    }
}
