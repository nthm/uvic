import java.io.*;
import java.util.*;

public class Solution {

    static class FastReader {

        BufferedReader br;
        StringTokenizer st;

        public FastReader() {
            br = new BufferedReader(new InputStreamReader(System.in));
        }

        String next() {
            while (st == null || !st.hasMoreElements()) {
                try {
                    st = new StringTokenizer(br.readLine());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return st.nextToken();
        }
        int nextInt() {
            return Integer.parseInt(next());
        }
    }

    public static void main(String[] args) throws Exception {
        FastReader in = new FastReader();
        int n = in.nextInt();
        int k = in.nextInt();
        List<Integer> numbers = new ArrayList<>(n);
        Comparator<Integer> comparator = Collections.reverseOrder();
        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(System.out));
        
        // the largest element is the head of the queue
        PriorityQueue<Integer> pq = new PriorityQueue<>(k, comparator);

        // load the window first before the main loop
        for (int i = 0; i < n; i++) {
            numbers.add(in.nextInt());
        }
        pq.addAll(numbers.subList(0, k - 1));

        for (int i = k - 1; i < n; i++) {
            pq.add(numbers.get(i));

            int largest = pq.remove();
            int secondLargest = pq.peek();
            out.write(largest + secondLargest + " ");
            pq.add(largest);

            pq.remove(numbers.get(i - k + 1));
        }
        out.flush();
    }
}
