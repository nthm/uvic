# A3

The problem is finding the largest numbers in a moving window of size k that
moves one by one through an array of size n. Therefore k <= n.

It must be solved in O(nlogn) time.

## Hints

We're told to use a priority queue, the standard one from `java.util` as the
window, that moves along the array. However, PQs don't support methods for
accessing beyond their first element, so you need to remove the head, peek the
second, and then re-add the head. Knowing that a PQ will autosort, and sorting
likely taking logn time at best, this seems inefficient.

## Attempts

I tried using an array, but shifting all items to provide a window is a heavy
operation. I then moved to a linked list, but didn't want to try sorting that
so I fed it into a PQ and cleared each on each move of the window.

Clearing and completely refilling a PQ is wasteful. Luckily there's a method
that can remove an element (duplicates will be OK, promise).

This passed nearly all test cases. See 47a8017b for that version.

The one test case it didn't pass was because the array of numbers was huge.
It makes more sense to then load all the input into an array _first_, instead
of loading them one at a time as needed. Context switching is expensive!

With all numbers as an array a linked list is just additional overhead. Used
array indexes instead. This is the latest version.