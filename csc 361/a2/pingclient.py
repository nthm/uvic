from socket import *
import sys
from time import time, sleep

testCount = 10

if len(sys.argv) != 3:
  print("Usage: `pingclient.py address port`")
  sys.exit(1)

program, address, port = sys.argv
client = socket(AF_INET, SOCK_DGRAM)

timeouts = 0

for sequenceNumber in range(1, testCount + 1):
  try:
    message = "ping {} {}".format(sequenceNumber, time())
    print("Sending:", message)

    client.sendto(message.encode(), (address, int(port)))
    sentAt = time()
    client.settimeout(1)

    recv, addr = client.recvfrom(1024)
    recvAt = time()
    recv = recv.strip()

    if not recv:
      continue

    rtt = recvAt - sentAt
    print("Received:", recv)
    print("RTT:", rtt)
  except timeout as e:
    print("Request timed out")
    timeouts += 1
  finally:
    print()
    sleep(1)

print("Packets sent:", testCount)
print("Packets lost:", timeouts)
