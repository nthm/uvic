import socket
import sys
import os

# server.py [optional address] [optional port] [optional content path]
print("Usage: `server.py [address] [port] [content path]`")

def argOr(num, default):
  return sys.argv[num] if len(sys.argv) > num else default

host = argOr(1, "0.0.0.0")
port = int(argOr(2, 8080))
rel_content_directory = argOr(3, ".")
abs_content_directory = os.path.abspath(rel_content_directory)

svrsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
svrsocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

svrsocket.bind((host, port))
try:
  print("Server listening on {host}:{port}".format(host=host, port=port))
except Exception as e:
  print("Error couldn't bind to {port}".format(port=port))
  # TODO: Have a signal handler for socket.shutdown(socket.SHUT_RDWR)
  svrsocket.shutdown()
  sys.exit(1)

# Allow only one connection at a time
svrsocket.listen(1)

print("Content path: {}/*".format(abs_content_directory))

while True:
  client, address = svrsocket.accept()
  client.settimeout(10)
  print("Accept:", address)
  print("Client:", client)

  # Up to 1024 bytes
  message = client.recv(1024).decode()
  print("Message: Length:", len(message))
  for msg in message.split('\n'):
    print("< {}".format(msg))

  if len(message) == 0:
    continue

  def sendError(message):
    print(">", message)
    res  = "HTTP/1.1 {}\r\n".format(message)
    res += "Content-Length: 0\r\n"
    res += "Connection: close\r\n"
    res += "\r\n"
    client.send(res.encode())
    client.shutdown(socket.SHUT_RDWR)
    client.close()

  try:
    request = message.split(" ")
    method = request[0]
    
    if (method is None or method != "GET"):
      sendError("405 Method Not Allowed")
      continue
    
    # OK, the method is GET. Look for the file
    filename = request[1]
    if filename is None or filename is "/":
      filename = "/index.html"

    if filename[0] != "/":
      filename = "/" + filename

    filepath = os.path.abspath(rel_content_directory + filename)

    # Forbid before 404 to not leak information about existing files
    if filepath[:len(abs_content_directory)] != abs_content_directory:
      sendError("403 Forbidden")
      continue

    if not os.path.exists(filepath):
      sendError("404 Not Found")
      continue

    print(filepath[:len(abs_content_directory)])

    with open(filepath, "rb") as f:
      size = os.path.getsize(filepath)
      # If we made it to this line without an IOError, it exists
      res  = "HTTP/1.1 200 OK\r\n"
      res += "Content-Length: {}\r\n".format(size)
      res += "\r\n"
      client.send(res.encode())
      print("> 200 OK")
      print(">", size, "bytes")
      
      # Send the content of the requested file to the client
      content = f.read()
      client.sendall(content)
      client.send("\r\n".encode())
      client.shutdown(socket.SHUT_RDWR)

  except Exception as e:
    print("Error:")
    print(e)
    sendError("500 Internal Server Error")

svrsocket.shutdown(socket.SHUT_RDWR)
svrsocket.close()
sys.exit(1)
