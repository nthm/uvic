import socket
import sys

# client.py address port resource
if len(sys.argv) != 4:
  print("Usage: `client.py address port resource`")
  sys.exit(1)

program, address, port, resource = sys.argv
port = int(port)

agent = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
agent.connect((address, port))

req  = 'GET {} HTTP/1.1\r\n'.format(resource)
req += 'Host: {}\r\n'.format(address)
req += '\r\n'
agent.send(req.encode())

res = b''
while True:
    recv = agent.recv(1024)
    print(recv.decode())
    if not recv:
      break

agent.shutdown(socket.SHUT_RDWR)
agent.close()
