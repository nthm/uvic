from socket import *
import sys

# Choose a mail server (e.g. Google mail server) and call it mailserver
mailserver = 'smtp.uvic.ca'

# Create socket called clientSocket and establish a TCP connection with mailserver
clientSocket = socket(AF_INET, SOCK_STREAM)
clientSocket.connect((mailserver, 25))

def send(message):
    print('>', message)
    clientSocket.send((message + '\r\n').encode())

def recv(statusCode):
    recv = clientSocket.recv(1024).decode()
    print(recv)
    if recv[:3] != statusCode:
        print(statusCode, 'reply not received from server')
        sys.exit(1)

recv('220')

# Send HELO command and print server response.
send('HELO Alice')
recv('250')
    
# Send MAIL FROM command and print server response.
send('MAIL FROM: hey@uvic.ca')
recv('250')

# Send RCPT TO command and print server response. 
send('RCPT TO: hey@heyhey.ca')
recv('250')

# Send DATA command and print server response. 
send('DATA')
recv('354')

# Send message data.
send('This is a test message from my smtpclient')

# Message ends with a single period.
send('.')
recv('250')

# Send QUIT command and get server response.
send('QUIT')
recv('221')
