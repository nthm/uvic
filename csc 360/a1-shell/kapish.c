#define _POSIX_C_SOURCE 200809L
#include <stdlib.h>   // NULL, EXIT_SUCCESS
#include <string.h>
#include <signal.h>   // ^C
#include <unistd.h>   // chdir, write, fork, pipe
#include <stdio.h>    // printf

#include <sys/wait.h>
#include <sys/types.h>

#include "builtins/history_list.h"

// Read limit for the input line buffer. Allocates MAX_READ bytes
#define MAX_READ 512

int run_builtins(char **args);
HistoryItem* cursor = NULL;

char *read_input_to_buffer(char *buffer) {
  for (int i = 0, c; i < MAX_READ; i++) {
    c = getchar();

    if (c == EOF) {
      printf("kpsh: EOF\n");
      exit(EXIT_SUCCESS);
    }

    if (c == '\n') {
      buffer[i] = '\0';
      return buffer;
    }
    
    buffer[i] = c;
  }

  // Buffer overflow. Tell them and skip
  fprintf(stderr, "kpsh: Input is over %d characters. Skipping.\n", MAX_READ);
  return NULL;
}

char **parse_buffer(char *line) {
  char* delimiter = " \t\r\n\a";
  int MAX_ARGS = 50;
  int i = 0;

  // TODO: I have concerns of this being a memory leak
  char **tokens = malloc(MAX_ARGS * sizeof(char*));
  char *current;

  if (!tokens) {
    fprintf(stderr, "kpsh: Couldn't allocate %d bytes for buffer\n", MAX_ARGS);
    exit(EXIT_FAILURE);
  }

  // Retrieve the first token
  current = strtok(line, delimiter);

  // Expand '!commandprefix' to '! commandprefix'
  if (current != NULL && current[0] == '!') {
    tokens[0] = "!";
    i = 1;
    // https://stackoverflow.com/q/4295754/remove-first-character-from-string
    current++;
  }

  // Walk through the others
  while (current != NULL) {
    tokens[i] = current;
    i++;

    if (i >= MAX_ARGS) {
      fprintf(stderr, "kpsh: Input has over %d tokens. Skipping.\n", MAX_ARGS);
      return NULL;
    }

    current = strtok(NULL, delimiter);
  }
  tokens[i] = NULL;
  return tokens;
}

int launch(char *line) {
  // Make a copy of the buffer. It will be freed with history_clear()
  int length = strlen(line);
  char* line_copy = (char*)malloc(sizeof(char) * length + 1);
  strncpy(line_copy, line, length);

  // Replace the newline with a null terminator
  do {
    line_copy[length] = '\0';
  } while (line_copy[--length] == '\n');

  char **args = parse_buffer(line);
  int status;
  pid_t pid;

  // Entered nothing, so pass
  if (args[0] == NULL) {
    free(line_copy);
    return -1;
  }

  HistoryItem* task = history_begin_add(line_copy);

  // Test for a builtin command
  status = run_builtins(args);
  if (status != -1) {
    goto processStatus;
  }

  pid = fork();
  if (pid == 0) {
    if (execvp(args[0], args) == -1) {
      fprintf(stderr, "kpsh: Error when using execvp\n");
    }
    exit(EXIT_FAILURE);
  } else {
    // This is the parent and waitpid() will write the status to &status
    do {
      waitpid(pid, &status, WUNTRACED);
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));
  }

  processStatus:
  history_end_add(task, status);
  if (status != 0) {
    printf("kpsh: Child exited with status %d\n", status);
  }
  return status;
}

void sig_handler(int sig) {
  printf("\nReceived signal: %d\n", sig);
  printf("? ");
  fflush(stdout);

  // Set the handler again
  signal(SIGINT, sig_handler);
}

int main(int argc, char **argv) {
  char *buffer = malloc(sizeof(char) * MAX_READ);

  // I don't know enough C to know if this is the OK place for this
  history_list = (HistoryList*)malloc(sizeof(HistoryList));
  cursor = history_list->most_recent;

  if (!buffer) {
    fprintf(stderr, "kpsh: Couldn't allocate %d bytes for buffer\n", MAX_READ);
    exit(EXIT_FAILURE);
  }

  // Read the config/init file
  char filepath[256];
  strcat(filepath, getenv("HOME"));
  strcat(filepath, "/.kapishrc");

  FILE *config = fopen(filepath, "r");
  if (config != NULL) {
    printf("Found and running config file ~/.kapishrc\n");
    int read;
    size_t len = 0;
    while ((read = getline(&buffer, &len, config)) != -1) {
      launch(buffer);
    }

    fclose(config);
  }

  while (1) {
    printf("? ");
    fflush(stdout);
    signal(SIGINT, sig_handler);
    read_input_to_buffer(buffer);
    launch(buffer);
  }

  free(buffer);
  return EXIT_SUCCESS;
}
