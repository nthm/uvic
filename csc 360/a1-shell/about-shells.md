# Write a shell

This assignment is about writing a shell for Unix/Linux. This has a lot of
theory to understand how processes, terminals, and sessions work. However,
there's really not a lot of code to a basic shell.

Our shell, _kapish_, is very minimal. Spawning children via `fork`+`exec` and
supporting few builtin commands is the most it is supposed to do. A `history`
command is optional for bonus marks. It doesn't have any form job control,
backgrounding, piping, redirection, or complex features like tab completion.

It does support environment variables, and reads from a `~/.kapishrc` config
file on startup.

# Background

To write a shell it's good to know about the ecosystem that comes before it, and
how to make something useful. To do that properly would be an entire book (that
no one would ever read!) so here's a few points.

I don't end up discussing PTYs or TTYs, nor do I dive into as much detail as I
should have about process and session groups + their leaders.

Read the GNU docs on implementing a shell in the [sessions](##Sessions) section.

## Importance of a shell

I've blogged a lot about shells. They're super interesting; as is their history
and tie ins to so many other OS concepts. Many people don't think they use
shells, because it's 2019 and we have nice GUIs instead. They miss that shells
are everywhere - your browser's address bar; spotlight search; messaging and
slash commands in IRC, Slack, Telegram; command bars like Ctrl+Shift+P in VSCode
or Alt+/ in Google Docs; REPLs for Python or JS. Jupyter notebook or Obversable;
You might not call these shells because they don't spawn processes, sure, but
they're related. Modern browser devtool consoles are the most powerful shells,
and only improving. That's the future to push for.

Unfortunately, decades have went by and we still suffer from issues like the
unreadable syntax of shell script, patchy processing of escape codes (esp. when
using pipes), binary output corrupting terminals, limited unsearchable
scrollbacks, and confusing methods of job control - to name a few. People rave
that the Unix philosophy is so strong because concepts like piping `|`, `cat`,
and `grep` are still used today. Yet, those examples are few and far in between.
If you run a command, see too much output, so re-run it with grep...you're doing
it wrong. That's backwards, not "powerful". Reading man pages to see how a
command should be used is not a selling point. Likewise, using `cat` or `head`
to look at a file - without syntax highlighting - is not powerful, but people
continue to do it because the alternative of launching their editor takes 20
seconds. 2019's a weird time.

The _fish_ shell has done best at being a modern shell, with tab completion as
its strongest feature. I'm not looking to try and do any of that in kapish. Yes,
because it's far too hard, but also because it's important to focus on getting
more foundational shell concepts right.

I've been talking a lot about features of a "shell"/REPL such as tab completion,
hints and dropdowns, or coloured output. That's universal, but not important to
a _Unix_ shell - or any shell spawning processes.

Foundationally, that involves more of `fork(2)`, `exec(3)`, `stdio(3)`, process
sessions, process groups, PTYs, and controlling terminals.

## Sessions

In a normal shell, a spawned process becomes the child of the shell process. The
parent waits for the child to return, and then waits for the next command to be
entered by the user. Shells can be implemented in around as little as 20 LoC in
C because many things are taken care of for you due to this parent-child
relationship: the file descriptors (stdio) are inherited so the child starts
writing to the same terminal; the current working directory is inherited, so
relative paths should work; the user and group of the parent is also inherited
so permissions will check out too.

Things get a little messy when you want to run multiple processes at the same
time. Most shells offer job control. You can background a process and run a
different one in the foreground (or _also_ in the background). How this is done
is described here:

https://www.gnu.org/software/libc/manual/html_node/Implementing-a-Shell.html

It's a great read.

Uses doubly linked lists to maintain a queue of who can read/write to the
terminal. That's an important and often overlooked concept - _only one process
can have control of the terminal at once_. This also has to do with signal
handling, to make sure the right process gets the right signal.

## Letting go of a session

You ssh into a server and run what will be a long running process. You want to
log out. You can't, sorta. If you ^C it kills your job. Exiting your terminal
kills ssh, which kills the session and its jobs. This can be visualized in
something like `htop` on the server. You could ^Z and `bg` to have it run in the
background, but it's still a child of the ssh session/process.

Everyone runs into this issue at some point. For me it was the first sign that
there's more behind the scenes to having a "session".

It also meant that if you laptop dies, your ssh'd server session dies too. If
your local display manager, X or Wayland, dies then all your sessions do too.

An "answer" is to this is to use `disown` on the server processes. Disown will
reparent the process tree to init (PID 1)**. In a process viewer, the tree isn't
related to the ssh session anymore. It's safe to leave.

Far from perfect though. Want to reattach? You can try programs like _reptyr_
but they have limited success. Also, any output from the program in the meantime
is lost into the void. The file descriptors were still pointed to the old ssh
session, so disowning doesn't stop the flow of logs either.

If you're trying to save your sessions from a parent crash but keep them around
to be interacted with, disowning isn't a good solution at all.

There are programs that attempt to help with this and provide a way of reliably
disconnect and reconnect to a session/process. They'll be brought up later.

## Relationship to 2.6-era daemons

Wayyy back, people needed to run a process as a daemon and the now highly
discouraged but then best practice method was to double fork and `setsid()` in
between so the new child is not attached to a terminal.

This is described in 1.7 of http://www.faqs.org/faqs/unix-faq/programmer/faq/:

> \[Use\] `setsid()` to become a process group and session group leader. Since a
> controlling terminal is associated with a session, and this new session has
> not yet acquired a controlling terminal our process now has no controlling
> terminal

The daemon has no way of talking to a terminal, so it likely writes to a log
file. Today this practice is strongly avoided. Long running/non-interactive
processes will be managed by a process supervisor, likely systemd, but there's a
long list of others. Supervisors want them to post logs directly to the
foreground so they can be captured from there. 2.6-era daemons can confuse
supervisors by disappearing via double forks.

How is this related? Well it's not that double forking is bad. It's about who's
doing it. It's not the job of the process, it's that of the supervisor now. It
keeps things far more simple to let the supervisor be the one managing file
descriptors and controlling terminals.

I'd argue that a shell _is_ a supervisor.

## Session managers: _Other_ supervisors

A supervisor is usually init. There are programs though, like `runit`, which are
very well designed to provide nested process and filedescriptor management.
These tools focus on being far from shells, though, and their interface is
centered around setting up a sandbox (possibly for security) and options
controlling whether a process should be restarted is it fails, depend on other
processes, or rotate logs. They're not meant to be interactive.

For semi-interactive session management there's programs like `dtach` (although,
use `abduco`) and `screen` or `tmux`. I'm not talking about the last two because
they're huge - each is a mountain of code, nearly entire operating systems in
and of themselves, that serve entire tilling window managers too; far beyond the
scope of this discussion.

Unfortunately most people use screen or tmux and that's the end of their story.
It works for them and that's great. Let's be suckless though.

I haven't read qdtach, because I found abducoq which is a better implementation
of the concept. I've written a lot on it in notebooks/cubic.

Around 1000 lines of C (can be read as one file, but uses `#include`s), abduco
manages sessions for programs via sockets and double forking. Instead of
assigning the process to the void and parenting it to init, it parents it to a
server process and binds its file descriptors to a Unix socket.

That's hot.

Use a client to talk to the process' server and do work. Attach to a socket and
disconnect whenever. You can list all the sockets/processes abduco is managing.
There's a server for each process - not one server for all processes.

The sockets are _real files!_ In dtach, accidentally deleting the folder of
sockets wouldn't kill the processes, but would destroy any chances of talking to
them ever again. In abduco you can SIGUSR1 the server and it'll recreate the
socket files.

This is a great leap forward, but has some issues:

- Need to intentionally wrap a program in abduco _before launch_. Once a process
  is running it's not super clear how to have abduco manage it. Solutions like
  reptyr may have limited success.

- There's no scrollback at all: Sockets are live, not buffers. Need to wrap a
  program in another logger _before launch_ to store output when you're away.

- Connecting to a session sort-of-rightfully resets your terminal. This is
  jarring and feels like flipping between full screen windows. I'd rather be
  able to peek at output like `systemctl status xyz` does without the shift.

# Kapish

1600 words later and only scratching the surface. Still. Let's go.

Kapish should use abduco for everything it runs. It should be able to do that
without resetting the terminal - just run the command in the foreground, but be
able to disconnect whenever. Or stop via ^Z and then disconnect to start it
later. That's all of job control right there.

Kapish should duplicate the file descriptors as files. The output of everything
should be saved. Disk space is cheap(ish). Have some limitations, sure. You
wouldn't want to log the output of `yes` since that can run at like 10GB/s.

Ideally Kapish would use a web frontend that _doesn't_ have a terminal. It's
important to have that seperation. If you want a full terminal use WeTTY
(xterm.js and node-pty). They're great projects. From there it's trivial to use
abduco to manage saved sessions. The browser has a tunnel to a PTY on the
server, which is _exactly_ like an ssh connection (you get a fresh terminal each
time), then then abduco is operating its client-server packet model entirely
(web)server side. The browser isn't getting abduco packets.

Being a web shell has flexibility like showing the output buffers as they fill.
Showing notifications. Having a sidebar of history, files, environment
variables, and other sockets to connect to. In a 80x25 terminal that's very hard
to do. Other programs, like tmux, do that with some degree of success but it's
very limiting.

The client in abduco describes the one to many relationship of other abduco's
connecting to the server. An abduco process can be both a server and client. The
client can leave, and another one can attach. If the process abduco is managing
exits when no client is connected to see it happen, the abduco server process
sticks around to save the status code until someone tries to connect.

Starting a bunch of abduco-wrapped commands spawns an equal number of abduco
servers as children of init. If you choose to attach, then a client is spawned
too. The client is _separate_ and is a decendant of your shell. Clients can come
and go.

## Scratchspace...

https://github.com/martanne/abduco/blob/master/client.c isn't clear if it could
be implemented (web)client side over the websocket.

The LWS (https://libwebsockets.org) has a page on Unix Domain Sockets Reverse
Proxy, which is sort of exactly what I'm looking for. I need to write directly
to the abduco socket, I believe... Maybe not. Would that mean writing `Packet`
structs as abduco does internally?
