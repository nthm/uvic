#include <stdlib.h>
#include <time.h>
#include "history_list.h"

// Linked list for command history. This is sort-of future proof as it will work
// for background jobs and commands that are running in parrallel. Kapish
// doesn't support that, however.

HistoryItem* history_begin_add(char* command) {
  time_t now = time(NULL);

  HistoryItem* item = (HistoryItem*)malloc(sizeof(HistoryItem));
  item->command = command;
  item->start_time = now;

  // MR=A<>... becomes MR=A>... with I<A<>...
  if (history_list->most_recent != NULL) {
    history_list->most_recent->next = item;
  }
  // MR=A<>... becomes I<>A<>...
  item->prev = history_list->most_recent;
  // MR=I<>A<>...
  history_list->most_recent = item;

  // Leave next, duration, and status as NULL
  return item;
}

void history_end_add(HistoryItem* started, int status) {
  time_t now = time(NULL);
  double seconds = difftime(now, started->start_time);
  started->duration = seconds;
  started->return_status = status;
  history_list->length++;
}

void history_clear() {
  HistoryItem* runner = history_list->most_recent;

  while (history_list->length > 1) {
    runner = runner->prev;
    free(runner->next->command);
    free(runner->next);
    history_list->length--;
  }
  // Can't free the last item in the loop
  free(runner->prev->command);
  free(runner->prev);
  history_list->length--;
  history_list->most_recent = NULL;
}
