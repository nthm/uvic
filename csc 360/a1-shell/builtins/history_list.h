#ifndef HISTORY_LIST_INCLUDED
#define HISTORY_LIST_INCLUDED

typedef struct HistoryItem HistoryItem;
struct HistoryItem {
  char* command;
  time_t start_time;
  double duration;
  int return_status;

  HistoryItem* prev; // Note that `prev` is further in the past
  HistoryItem* next; // Towards the future
};

typedef struct HistoryList HistoryList;
struct HistoryList {
    HistoryItem* most_recent;
    unsigned int length;
};

// History is a global object - there is only one list

HistoryList* history_list;
HistoryItem* history_begin_add  (char* command);
void         history_end_add    (HistoryItem* started, int status);
void         history_clear      ();

#endif
