#include <stdio.h>
#include <stdlib.h>

#include "linked_list.h"

List* ll() {
    List* leader = (List*)malloc(sizeof(List));
    if (!leader)
        exit(EXIT_FAILURE);

    leader->head   = NULL;
    leader->end    = NULL;
    leader->cursor = NULL;
    leader->length = 0;
    return leader;
}

Node* ll_node(unsigned char hex) {
    Node* item = (Node*)malloc(sizeof(Node));
    if (!item)
        exit(EXIT_FAILURE);

    item->hex  = hex;
    item->prev = NULL;
    item->next = NULL;
    return item;
}

void ll_prev(List* leader) {
    if (leader->cursor == NULL)
        return;
    leader->cursor = leader->cursor->prev;
}

void ll_next(List* leader) {
    if (leader->cursor == NULL)
        return;
    leader->cursor = leader->cursor->next;
}

/* ------------------------------------------------------------------
 * list level adding - doesn't care or touch the cursor */

void ll_prepend(List* leader, Node* to_add) {
    to_add->next = leader->head;
    leader->head->prev = to_add;
    leader->head = to_add;
    leader->length++;
}

void ll_append(List* leader, Node* to_add) {
    if (leader->length == 0) {
        leader->head = to_add;
        leader->end = to_add;
    } else {
        leader->end->next = to_add;
        to_add->prev = leader->end;
        leader->end = to_add;
    }
    leader->length++;
}

/* ------------------------------------------------------------------
 * cursor level adding */

void ll_cursor_prepend(List* leader, Node* to_add) {
    if (leader->cursor == NULL) {
        printf("LL: trying to prepend to a null cursor\n");
        free(to_add);
        return;
    }
    to_add->next = leader->cursor;
    if (leader->head == leader->cursor) {
        leader->cursor->prev = to_add;
        leader->head = to_add;
    } else {
        to_add->prev = leader->cursor->prev;
        leader->cursor->prev->next = to_add;
    }
    leader->cursor->prev = to_add;
    leader->length++;
}

void ll_cursor_append(List* leader, Node* to_add) {
    if (leader->cursor == NULL) {
        printf("LL: trying to append to a null cursor\n");
        free(to_add);
        return;
    }
    to_add->prev = leader->cursor;
    to_add->next = leader->cursor->next;
    leader->cursor->next->prev = to_add;
    leader->cursor->next = to_add;
    leader->length++;

    if (leader->end == leader->cursor) {
        leader->end = to_add;
    }
}

/* ------------------------------------------------------------------
 * unlink the list item at the cursor and return it */

Node* ll_unlink(List* leader) {
    if (leader->cursor == NULL) {
        printf("LL: trying to unlink a null cursor\n");
        return NULL;
    }
    if (leader->cursor == leader->head) {
        leader->head = leader->cursor->next;
    } else {
        leader->cursor->prev->next = leader->cursor->next;
    }
    Node* item = leader->cursor;
    leader->cursor = NULL; /* who's to say what we're pointing at? */
    leader->length--;
    item->prev = NULL;
    item->next = NULL;
    return item;
}

/* ------------------------------------------------------------------
 * debug helper */

void ll_print(List* leader) {
    if (leader == NULL) {
        printf("LL: trying to print NULL leader\n");
        return;
    }
    Node* runner = leader->head;
    unsigned char hex;

    printf("LL: list: items: %d\n", leader->length);
    for ( ; runner; runner = runner->next) {
        if ((hex = runner->hex) < 128)
            printf("  char: '%c'", hex);
        else
            printf("   int: '%d'", hex - 128);

        if (runner == leader->cursor)
            printf(" < cursor");
        if (runner == leader->head)
            printf(" < head");
        if (runner == leader->end)
            printf(" < end");
        printf("\n");
    }
    printf("\n");
}

/* ------------------------------------------------------------------
 * free all nodes of a list but leave the leader allocated */

void ll_empty(List* leader) {
    /* no runner because the list is being destroyed */
    leader->cursor = leader->head;
    /* can't free the last item in the loop */
    while (leader->length > 1) {
        ll_next(leader);
        free(leader->cursor->prev);
        leader->length--;
    }
    free(leader->end);
    leader->length--;
    leader->cursor = leader->head = leader->end = NULL;
}

/* Links:
 * leader: stackoverflow.com/q/21070370/idiomatic-linked-list-prepend-in-c
 * unsigned char: stackoverflow.com/q/15736497/printing-unsigned-char-in-c
 * ll prefix: stackoverflow.com/q/389827/namespaces-in-c
 */
