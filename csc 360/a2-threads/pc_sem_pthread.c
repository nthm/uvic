#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <semaphore.h>
#include <pthread.h>

#define MAX_ITEMS 10
const int NUM_ITERATIONS = 200;
const int NUM_CONSUMERS  = 2;
const int NUM_PRODUCERS  = 2;
const int NUM_THREADS = NUM_CONSUMERS + NUM_PRODUCERS;

sem_t mutex;
sem_t spaces_available;
sem_t spaces_taken;

int histogram [MAX_ITEMS+1]; // histogram [i] == # of times list stored i items
int items = 0;

void* producer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    assert(items >= 0 && items <= MAX_ITEMS);
    // wait until the semaphore is not 0. aka there's room
    // also can be read as "decrement spaces_available" 
    sem_wait(&spaces_available);
    sem_wait(&mutex);
    items++;
    histogram[items]++;
    sem_post(&mutex);
    // increment "spaces_taken"
    sem_post(&spaces_taken);
  }
  return NULL;
}

void* consumer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    assert(items >= 0 && items <= MAX_ITEMS);
    // wait until the semaphore is not 0. aka there's at least one space taken
    // also can be read as "decrement spaces_taken" 
    sem_wait(&spaces_taken);
    sem_wait(&mutex);
    items--;
    histogram[items]++;
    sem_post(&mutex);
    // increment "spaces_available"
    sem_post(&spaces_available);
  }
  return NULL;
}

int main (int argc, char** argv) {
  sem_init(&mutex, 0, 1);
  sem_init(&spaces_available, 0, MAX_ITEMS);
  sem_init(&spaces_taken, 0, 0);

  int rc[NUM_THREADS];
  pthread_t t[NUM_THREADS];

  // create producer threads
  for (int i = 0; i < NUM_PRODUCERS; i++) {
    rc[i] = pthread_create( &t[i], NULL, &producer, NULL);
    if (rc[i]) {
      printf("thread creation failed: %d\n", rc[i]);
    }
  }

  // create consumer threads (NUM_THREADS - NUM_PRODUCERS) of them
  for (int i = NUM_PRODUCERS; i < NUM_THREADS; i++) {
    rc[i] = pthread_create( &t[i], NULL, &consumer, NULL);
    if (rc[i]) {
      printf("thread creation failed: %d\n", rc[i]);
    }
  }

  // block the main thread by joining them
  for (int i = 0; i < NUM_THREADS; i++) {
    pthread_join(t[i], NULL);
  }

  printf ("items value histogram:\n");
  int sum=0;
  for (int i = 0; i <= MAX_ITEMS; i++) {
    printf ("  items=%d, %d times\n", i, histogram [i]);
    sum += histogram [i];
  }
  assert (sum == sizeof (t) / sizeof (pthread_t) * NUM_ITERATIONS);
}
