#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <pthread.h>

#define NUM_ITERATIONS 1000

#ifdef VERBOSE
#define VERBOSE_PRINT(S, ...) printf (S, ##__VA_ARGS__); fflush(NULL);
#else
#define VERBOSE_PRINT(S, ...) ;
#endif


// I couldn't get this working so it's global now...

// struct Agent {
//   pthread_mutex_t mutex;
//   pthread_cond_t  match;
//   pthread_cond_t  paper;
//   pthread_cond_t  tobacco;
//   pthread_cond_t  smoke;
// };
// struct Agent* createAgent() {
//   struct Agent* agent = malloc (sizeof (struct Agent));
//   agent->mutex   = pthread_mutex_init(&agent->mutex, NULL);
//   agent->paper   = pthread_cond_init(&agent->paper, NULL);
//   agent->match   = pthread_cond_init(&agent->match, NULL);
//   agent->tobacco = pthread_cond_init(&agent->tobacco, NULL);
//   agent->smoke   = pthread_cond_init(&agent->smoke, NULL);
//   return agent;
// }

// sum of signals that has been sent out this round
int available = 0;

pthread_mutex_t a_mutex   = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  a_match   = PTHREAD_COND_INITIALIZER;
pthread_cond_t  a_paper   = PTHREAD_COND_INITIALIZER;
pthread_cond_t  a_tobacco = PTHREAD_COND_INITIALIZER;
pthread_cond_t  a_smoke   = PTHREAD_COND_INITIALIZER;

pthread_mutex_t mutex          = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t matches_paper   = PTHREAD_COND_INITIALIZER;
pthread_cond_t tobacco_paper   = PTHREAD_COND_INITIALIZER;
pthread_cond_t tobacco_matches = PTHREAD_COND_INITIALIZER;

/**
 * You might find these declarations helpful.
 *   Note that Resource enum had values 1, 2 and 4 so you can combine resources;
 *   e.g., having a MATCH and PAPER is the value MATCH | PAPER == 1 | 2 == 3
 */
enum Resource            {    MATCH = 1, PAPER = 2,   TOBACCO = 4};
char* resource_name [] = {"", "match",   "paper", "", "tobacco"};

int signal_count [5];  // # of times resource signalled
int smoke_count  [5];  // # of times smoker with resource smoked

/**
 * This is the agent procedure.  It is complete and you shouldn't change it in
 * any material way.  You can re-write it if you like, but be sure that all it does
 * is choose 2 random reasources, signal their condition variables, and then wait
 * wait for a smoker to smoke.
 */
void* agent () {
  static const int choices[]         = {MATCH|PAPER, MATCH|TOBACCO, PAPER|TOBACCO};
  static const int matching_smoker[] = {TOBACCO,     PAPER,         MATCH};
  
  pthread_mutex_lock (&a_mutex);
    for (int i = 0; i < NUM_ITERATIONS; i++) {
      int r = random() % 3;
      signal_count [matching_smoker [r]] ++;
      int c = choices [r];
      if (c & MATCH) {
        VERBOSE_PRINT ("match available\n");
        pthread_cond_signal (&a_match);
      }
      if (c & PAPER) {
        VERBOSE_PRINT ("paper available\n");
        pthread_cond_signal (&a_paper);
      }
      if (c & TOBACCO) {
        VERBOSE_PRINT ("tobacco available\n");
        pthread_cond_signal (&a_tobacco);
      }
      VERBOSE_PRINT ("agent is waiting for smoker to smoke\n");
      pthread_cond_wait (&a_smoke, &a_mutex);
    }
  pthread_mutex_unlock (&a_mutex);
  return NULL;
}

void call_smoker (int currently_available) {
  if (currently_available == MATCH+PAPER) {
    pthread_cond_signal (&matches_paper);
    available = 0;
    return;
  }
  if (currently_available == MATCH+TOBACCO) {
    pthread_cond_signal (&tobacco_matches);
    available = 0;
    return;
  }
  if (currently_available == PAPER+TOBACCO) {
    pthread_cond_signal (&tobacco_paper);
    available = 0;
    return;
  }
}

void* proxyA () {
  pthread_mutex_lock (&a_mutex);
  while (1) {
    pthread_cond_wait (&a_match, &a_mutex);
    VERBOSE_PRINT ("Proxy got MATCH\n");
    VERBOSE_PRINT ("Already available: %d\n", available);
    available += MATCH;
    call_smoker(available);
  }
  pthread_mutex_unlock (&a_mutex);
}

void* proxyB () {
  pthread_mutex_lock (&a_mutex);
  while (1) {
    pthread_cond_wait (&a_paper, &a_mutex);
    VERBOSE_PRINT ("Proxy got PAPER\n");
    VERBOSE_PRINT ("Already available: %d\n", available);
    available += PAPER;
    call_smoker(available);
  }
  pthread_mutex_unlock (&a_mutex);
}

void* proxyC () {
  pthread_mutex_lock (&a_mutex);
  while (1) {
    pthread_cond_wait (&a_tobacco, &a_mutex);
    VERBOSE_PRINT ("Proxy got TOBACCO\n");
    VERBOSE_PRINT ("Already available: %d\n", available);
    available += TOBACCO;
    call_smoker(available);
  }
  pthread_mutex_unlock (&a_mutex);
}

void* smoker_with_tobacco () {
  pthread_mutex_lock (&a_mutex);
  while (1) {
    pthread_cond_wait (&matches_paper, &a_mutex);
    VERBOSE_PRINT ("The tobacco smoker smoked\n");
    smoke_count[TOBACCO]++;
    pthread_cond_signal (&a_smoke);
  }
  pthread_mutex_unlock (&a_mutex);
}

void* smoker_with_match () {
  pthread_mutex_lock (&a_mutex);
  while (1) {
    pthread_cond_wait (&tobacco_paper, &a_mutex);
    VERBOSE_PRINT ("The match smoker smoked\n");
    smoke_count[MATCH]++;
    pthread_cond_signal (&a_smoke);
  }
  pthread_mutex_unlock (&a_mutex);
}

void* smoker_with_paper () {
  pthread_mutex_lock (&a_mutex);
  while (1) {
    pthread_cond_wait (&tobacco_matches, &a_mutex);
    VERBOSE_PRINT ("The paper smoker smoked\n");
    smoke_count[PAPER]++;
    pthread_cond_signal (&a_smoke);
  }
  pthread_mutex_unlock (&a_mutex);
}

int main (int argc, char** argv) {
  int rc[7];
  pthread_t t[7];

  if (rc[0] = pthread_create( &t[0], NULL, &proxyA, NULL)) {
    printf("thread creation failed: %d\n", rc[0]);
  }
  if (rc[1] = pthread_create( &t[1], NULL, &proxyB, NULL)) {
    printf("thread creation failed: %d\n", rc[1]);
  }
  if (rc[2] = pthread_create( &t[2], NULL, &proxyC, NULL)) {
    printf("thread creation failed: %d\n", rc[2]);
  }

  if (rc[3] = pthread_create( &t[3], NULL, &smoker_with_tobacco, NULL)) {
    printf("thread creation failed: %d\n", rc[3]);
  }
  if (rc[4] = pthread_create( &t[4], NULL, &smoker_with_match, NULL)) {
    printf("thread creation failed: %d\n", rc[4]);
  }
  if (rc[5] = pthread_create( &t[5], NULL, &smoker_with_paper, NULL)) {
    printf("thread creation failed: %d\n", rc[5]);
  }

  if (rc[6] = pthread_create( &t[6], NULL, &agent, NULL)) {
    printf("thread creation failed: %d\n", rc[6]);
  }

  pthread_join(t[6], NULL);

  assert (signal_count [MATCH]   == smoke_count [MATCH]);
  assert (signal_count [PAPER]   == smoke_count [PAPER]);
  assert (signal_count [TOBACCO] == smoke_count [TOBACCO]);
  assert (smoke_count [MATCH] + smoke_count [PAPER] + smoke_count [TOBACCO] == NUM_ITERATIONS);
  printf ("Smoke counts: %d matches, %d paper, %d tobacco\n",
          smoke_count [MATCH], smoke_count [PAPER], smoke_count [TOBACCO]);
}
