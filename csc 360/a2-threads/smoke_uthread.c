#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include "uthread.h"
#include "uthread_mutex_cond.h"

#define NUM_ITERATIONS 1000

#ifdef VERBOSE
#define VERBOSE_PRINT(S, ...) printf (S, ##__VA_ARGS__); fflush(NULL);
#else
#define VERBOSE_PRINT(S, ...) ;
#endif

struct Agent {
  uthread_mutex_t mutex;
  uthread_cond_t  match;
  uthread_cond_t  paper;
  uthread_cond_t  tobacco;
  uthread_cond_t  smoke;
};

struct Agent* createAgent() {
  struct Agent* agent = malloc (sizeof (struct Agent));
  agent->mutex   = uthread_mutex_create();
  agent->paper   = uthread_cond_create (agent->mutex);
  agent->match   = uthread_cond_create (agent->mutex);
  agent->tobacco = uthread_cond_create (agent->mutex);
  agent->smoke   = uthread_cond_create (agent->mutex);
  return agent;
}

// sum of signals that has been sent out this round
int available = 0;
uthread_cond_t  matches_paper;
uthread_cond_t  tobacco_paper;
uthread_cond_t  tobacco_matches;

/**
 * You might find these declarations helpful.
 *   Note that Resource enum had values 1, 2 and 4 so you can combine resources;
 *   e.g., having a MATCH and PAPER is the value MATCH | PAPER == 1 | 2 == 3
 */
enum Resource            {    MATCH = 1, PAPER = 2,   TOBACCO = 4};
char* resource_name [] = {"", "match",   "paper", "", "tobacco"};

int signal_count [5];  // # of times resource signalled
int smoke_count  [5];  // # of times smoker with resource smoked

/**
 * This is the agent procedure.  It is complete and you shouldn't change it in
 * any material way.  You can re-write it if you like, but be sure that all it does
 * is choose 2 random reasources, signal their condition variables, and then wait
 * wait for a smoker to smoke.
 */
void* agent (void* av) {
  struct Agent* a = av;
  static const int choices[]         = {MATCH|PAPER, MATCH|TOBACCO, PAPER|TOBACCO};
  static const int matching_smoker[] = {TOBACCO,     PAPER,         MATCH};
  
  uthread_mutex_lock (a->mutex);
    for (int i = 0; i < NUM_ITERATIONS; i++) {
      int r = random() % 3;
      signal_count [matching_smoker [r]] ++;
      int c = choices [r];
      if (c & MATCH) {
        VERBOSE_PRINT ("match available\n");
        uthread_cond_signal (a->match);
      }
      if (c & PAPER) {
        VERBOSE_PRINT ("paper available\n");
        uthread_cond_signal (a->paper);
      }
      if (c & TOBACCO) {
        VERBOSE_PRINT ("tobacco available\n");
        uthread_cond_signal (a->tobacco);
      }
      VERBOSE_PRINT ("agent is waiting for smoker to smoke\n");
      uthread_cond_wait (a->smoke);
    }
  uthread_mutex_unlock (a->mutex);
  return NULL;
}

void call_smoker (int currently_available) {
  if (currently_available == MATCH+PAPER) {
    uthread_cond_signal (matches_paper);
    available = 0;
    return;
  }
  if (currently_available == MATCH+TOBACCO) {
    uthread_cond_signal (tobacco_matches);
    available = 0;
    return;
  }
  if (currently_available == PAPER+TOBACCO) {
    uthread_cond_signal (tobacco_paper);
    available = 0;
    return;
  }
}

void* proxyA (void* av) {
  struct Agent* a = av;
  uthread_mutex_lock(a->mutex);
  while (1) {
    uthread_cond_wait (a->match);
    VERBOSE_PRINT ("Proxy got MATCH\n");
    VERBOSE_PRINT ("Already available: %d\n", available);
    available += MATCH;
    call_smoker(available);
  }
  uthread_mutex_unlock (a->mutex);
}

void* proxyB (void* av) {
  struct Agent* a = av;
  uthread_mutex_lock(a->mutex);
  while (1) {
    uthread_cond_wait (a->paper);
    VERBOSE_PRINT ("Proxy got PAPER\n");
    VERBOSE_PRINT ("Already available: %d\n", available);
    available += PAPER;
    call_smoker(available);
  }
  uthread_mutex_unlock (a->mutex);
}

void* proxyC (void* av) {
  struct Agent* a = av;
  uthread_mutex_lock(a->mutex);
  while (1) {
    uthread_cond_wait (a->tobacco);
    VERBOSE_PRINT ("Proxy got TOBACCO\n");
    VERBOSE_PRINT ("Already available: %d\n", available);
    available += TOBACCO;
    call_smoker(available);
  }
  uthread_mutex_unlock (a->mutex);
}

void* smoker_with_tobacco (void* av) {
  struct Agent* a = av;
  uthread_mutex_lock (a->mutex);
  while (1) {
    uthread_cond_wait (matches_paper);
    VERBOSE_PRINT ("The tobacco smoker smoked\n");
    smoke_count[TOBACCO]++;
    uthread_cond_signal (a->smoke);
  }
  uthread_mutex_unlock (a->mutex);
}

void* smoker_with_match (void* av) {
  struct Agent* a = av;
  uthread_mutex_lock (a->mutex);
  while (1) {
    uthread_cond_wait (tobacco_paper);
    VERBOSE_PRINT ("The match smoker smoked\n");
    smoke_count[MATCH]++;
    uthread_cond_signal (a->smoke);
  }
  uthread_mutex_unlock (a->mutex);
}

void* smoker_with_paper (void* av) {
  struct Agent* a = av;
  uthread_mutex_lock (a->mutex);
  while (1) {
    uthread_cond_wait (tobacco_matches);
    VERBOSE_PRINT ("The paper smoker smoked\n");
    smoke_count[PAPER]++;
    uthread_cond_signal (a->smoke);
  }
  uthread_mutex_unlock (a->mutex);
}

int main (int argc, char** argv) {
  uthread_init (7);
  struct Agent* agent_ref = createAgent();

  matches_paper   = uthread_cond_create (agent_ref->mutex);
  tobacco_paper   = uthread_cond_create (agent_ref->mutex);
  tobacco_matches = uthread_cond_create (agent_ref->mutex);

  uthread_create (proxyA, agent_ref);
  uthread_create (proxyB, agent_ref);
  uthread_create (proxyC, agent_ref);

  uthread_create (smoker_with_tobacco, agent_ref);
  uthread_create (smoker_with_match  , agent_ref);
  uthread_create (smoker_with_paper  , agent_ref);

  uthread_join (uthread_create (agent, agent_ref), 0);

  assert (signal_count [MATCH]   == smoke_count [MATCH]);
  assert (signal_count [PAPER]   == smoke_count [PAPER]);
  assert (signal_count [TOBACCO] == smoke_count [TOBACCO]);
  assert (smoke_count [MATCH] + smoke_count [PAPER] + smoke_count [TOBACCO] == NUM_ITERATIONS);
  printf ("Smoke counts: %d matches, %d paper, %d tobacco\n",
          smoke_count [MATCH], smoke_count [PAPER], smoke_count [TOBACCO]);
}
