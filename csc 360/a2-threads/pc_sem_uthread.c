#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "uthread.h"
#include "uthread_sem.h"

#define MAX_ITEMS 10
const int NUM_ITERATIONS = 200;
const int NUM_CONSUMERS  = 2;
const int NUM_PRODUCERS  = 2;
const int NUM_THREADS = NUM_CONSUMERS + NUM_PRODUCERS;

uthread_sem_t mutex;
uthread_sem_t spaces_available;
uthread_sem_t spaces_taken;

int histogram [MAX_ITEMS+1]; // histogram [i] == # of times list stored i items
int items = 0;

void* producer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    assert(items >= 0 && items <= MAX_ITEMS);
    // wait until the semaphore is not 0. aka there's room
    // also can be read as "decrement spaces_available" 
    uthread_sem_wait(spaces_available);
    uthread_sem_wait(mutex);
    items++;
    histogram[items]++;
    uthread_sem_signal(mutex);
    // increment "spaces_taken"
    uthread_sem_signal(spaces_taken);
  }
  return NULL;
}

void* consumer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    assert(items >= 0 && items <= MAX_ITEMS);
    // wait until the semaphore is not 0. aka there's at least one space taken
    // also can be read as "decrement spaces_taken" 
    uthread_sem_wait(spaces_taken);
    uthread_sem_wait(mutex);
    items--;
    histogram[items]++;
    uthread_sem_signal(mutex);
    // increment "spaces_available"
    uthread_sem_signal(spaces_available);
  }
  return NULL;
}

int main (int argc, char** argv) {
  uthread_t t[4];
  uthread_init(4);

  mutex = uthread_sem_create(1);
  spaces_available = uthread_sem_create(MAX_ITEMS);
  spaces_taken = uthread_sem_create(0);

  // create producer threads
  for (int i = 0; i < NUM_PRODUCERS; i++) {
    t[i] = uthread_create(producer, 0);
  }

  // create consumer threads (NUM_THREADS - NUM_PRODUCERS) of them
  for (int i = 0; i < NUM_PRODUCERS; i++) {
    t[i] = uthread_create(consumer, 0);
  }

  // block the main thread by joining them
  for (int i = 0; i < NUM_THREADS; i++) {
    uthread_join(t[i], NULL);
  }

  printf ("items value histogram:\n");
  int sum=0;
  for (int i = 0; i <= MAX_ITEMS; i++) {
    printf ("  items=%d, %d times\n", i, histogram [i]);
    sum += histogram [i];
  }
  assert (sum == sizeof (t) / sizeof (uthread_t) * NUM_ITERATIONS);
}
