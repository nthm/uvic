#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "uthread.h"
#include "uthread_mutex_cond.h"
#include "spinlock.h"

#define MAX_ITEMS 10
const int NUM_ITERATIONS = 200;
const int NUM_CONSUMERS  = 2;
const int NUM_PRODUCERS  = 2;
const int NUM_THREADS = NUM_CONSUMERS + NUM_PRODUCERS;

int producer_wait_count;     // # of times producer had to wait
int consumer_wait_count;     // # of times consumer had to wait
int histogram [MAX_ITEMS+1]; // histogram [i] == # of times list stored i items

int items = 0;

spinlock_t lock;             // global lock used by all threads

void* producer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    spinlock_lock (&lock);
    assert(items >= 0 && items <= MAX_ITEMS);
    if (items == MAX_ITEMS) {
      // return and wait for consumers to make room in the buffer
      producer_wait_count++;
      // consider it a write off and try again later
      i--;
    } else {
      items++;
      histogram[items]++;
    }
    spinlock_unlock (&lock);
  }
  return NULL;
}

void* consumer (void* v) {
  for (int i=0; i<NUM_ITERATIONS; i++) {
    spinlock_lock (&lock);
    assert(items >= 0 && items <= MAX_ITEMS);
    if (items == 0) {
      // return and wait for producers to add to the buffer
      consumer_wait_count++;
      // consider it a write off and try again later
      i--;
    } else {
      items--;
      histogram[items]++;
    }
    spinlock_unlock (&lock);
  }
  return NULL;
}

int main (int argc, char** argv) {
  uthread_t t[NUM_THREADS];

  // initialize 4 processors since this is a quad core system
  uthread_init (4);

  spinlock_create (&lock);

  // create threads
  for (int i = 0; i < NUM_PRODUCERS; i++) {
    t[i] = uthread_create(producer, 0);
  }

  for (int i = NUM_PRODUCERS; i < NUM_THREADS; i++) {
    t[i] = uthread_create(consumer, 0);
  }

  // block the main thread by joining them
  for (int i = 0; i < NUM_THREADS; i++) {
    uthread_join(t[i], NULL);
  }

  printf ("producer_wait_count=%d\nconsumer_wait_count=%d\n", producer_wait_count, consumer_wait_count);
  printf ("items value histogram:\n");
  int sum = 0;
  for (int i = 0; i <= MAX_ITEMS; i++) {
    printf ("  items=%d, %d times\n", i, histogram [i]);
    sum += histogram [i];
  }
  assert (sum == sizeof (t) / sizeof (uthread_t) * NUM_ITERATIONS);
  return EXIT_SUCCESS;
}
